# -*- coding: utf-8 -*-
"""
Created on Wed Mar 16 12:38:40 2016

@author: csteinke
"""

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
matplotlib.use('Agg')
import matplotlib.pyplot as plt
from matplotlib import rc

#Loading Data 
os.system(['clear','cls'][os.name == 'nt'])

myFontSize = 18
nnCellsX = 9
nnCellsY = 6

eps1 = 5
eps2 = 5

Broadening = 0.1#0.1 #Filename: blub_broadenedEnding.dat: Broadening = 0.02, not broadned= 0.005(?)

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/DiracMoS2/'
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps1)+'_' + str(eps2)
tfolder = '/' #'/t_0.5/' #
filename = 'supercell_'+str(nnCellsX)+'x'+str(nnCellsY)+'_'+str(eps1)+'_' + str(eps2)+'_periodicInE2_'
path = rootDir + folder + subfolder + tfolder


print('Loading Data from %s' % path)
#Loading Dipole elements
DipolMatSave = np.loadtxt(path + filename + 'Dipol.dat')


#Loading excitonic eigenvalues and eigenvectors (be careful! Eigs Data has one value less)

#Eigenvals = np.loadtxt(path + 'exc_ham_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_wocoulomb_eigenvals.dat')
Eigenvals = np.loadtxt(path + 'exc_ham_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_eigenvals_eigs.dat')
Eigenvec = np.loadtxt(path + 'exc_ham_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_eigenvec_eigs.dat')
#Eigenvec = np.loadtxt(path + 'exc_ham_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_wocoulomb_eigenvec.dat')


##Careful! Eigenvalues are not sorted yet!
sortind = np.argsort(Eigenvals)
Eigenvals = Eigenvals[sortind]
Eigenvec = Eigenvec[:,sortind]

numberHoles = nnCellsX*nnCellsY*4/2
numberElec = nnCellsX*nnCellsY*4/2
numberAtoms = nnCellsX*nnCellsY*4

dimBasis = DipolMatSave.shape[1] - 2
#Generating Matrix from Input-files (1-based indexing in Data, 0-based in python)
DipolMatrix = np.zeros((numberElec, numberHoles, 3))
Dipol_index = DipolMatSave[:,0:2].astype(int)-1
DipolMatrix[Dipol_index[:,0],Dipol_index[:,1],0:dimBasis] = DipolMatSave[:,2:(dimBasis+2)]


plt.ioff()
 
#Defining electromagnetic field
#for Ex in range(0,2):
#    for Ey in range(0,2):
#        for Ez in range(0,2):
Ez = 1 
Ex = 0
Ey = 0  
#            if (Ex + Ey + Ez) != 0:
                
Amplitude = 1
Polarization = np.array([Ex,Ey,Ez])
ElecField = Amplitude*Polarization
print('Polarisation:' + str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2]))

#Defining Energy-range and broadening
EnergyMin = np.min(Eigenvals)
EnergyMax = np.max(Eigenvals)
Energystep = 0.001
Energy = np.arange(EnergyMin,EnergyMax,Energystep)


#Creating "list" of Dipole-elements in same kind of basis as eigenvectors [11,12,13,...21,22,...]

#Collective index of i and j (i - electron, j hole)
iCollective = 0

print('Generating list of dipole matrix elements')
DipoleList = np.zeros((numberElec*numberHoles), dtype = complex)

for iElectron in range(0,numberElec):
    for jHole in range(0,numberHoles):

        DipoleList[iCollective] = np.dot(ElecField,DipolMatrix[iElectron,jHole,:])   
        
        iCollective = iCollective + 1
        
#Calculating transition rate (sum over iCollective) for every eigenstate of excitonig matrix

print('Calculating transition rates')
TransitionRate_cmplx = np.zeros(numberElec*numberHoles, dtype = complex)
TransitionRate= np.zeros(numberElec*numberHoles)
OpticalSpectrum = np.zeros((Energy.shape[0],1))
Histogram = np.zeros((Energy.shape[0],1))

#because there is one eigenstate less, only sum over number of eigenstates - 1
for jState in range(0, numberElec * numberHoles-1):
    for iCollective in range(0,numberElec*numberHoles):
        TransitionRate_cmplx[jState] = TransitionRate_cmplx[jState] + \
                     DipoleList[iCollective]*Eigenvec[iCollective,jState]
                      
    TransitionRate[jState] = np.abs(TransitionRate_cmplx[jState])**2
    OpticalSpectrum[:,0] = OpticalSpectrum[:,0] + \
                              TransitionRate[jState] * Broadening/((Energy[:] - Eigenvals[jState])**2 + Broadening**2)
    Histogram[:,0] = Histogram[:,0] +  Broadening/((Energy[:] - Eigenvals[jState])**2 + Broadening**2)

 
Energy = np.reshape(Energy,(Energy.shape[0],1))
SpectrumSave = np.append(Energy,OpticalSpectrum, axis=1)
SaveEnding = str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2])

#np.savetxt(path +'OpticalSpectra/'+'OpticalSpectrum_broadened'+SaveEnding+'.dat', SpectrumSave)
np.savetxt(path +'OpticalSpectra/'+'OpticalSpectrum_broadened_'+str(Broadening) + '_' +SaveEnding+'.dat', SpectrumSave)
 
   

plt.figure()
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
plt.title(r'Absorption spectrum', fontsize=myFontSize)

StrElecField = '('+str(Polarization[0]) +',' + str(Polarization[1]) + ',' + str(Polarization[2]) + ')'
plt.plot(Energy, Histogram/np.max(Histogram), label=r'Full excitonic spectrum', color = 'grey')
plt.plot(Energy, OpticalSpectrum/np.max(OpticalSpectrum) , label=r'$\textbf{E} = ' + StrElecField + '$', color='black', linewidth=1.0)

#plt.legend(fontsize = (myFontSize-4), loc='upper right', borderaxespad = 0)
plt.legend(loc='upper right', borderaxespad = 0)                       

#plt.savefig(path +'OpticalSpectra/'+'OpticalSpectrum_broadened'+SaveEnding+'.eps', format='eps')
plt.savefig(path +'OpticalSpectra/'+'OpticalSpectrum_broadened_'+ str(Broadening)+'_'+SaveEnding+'.eps', format='eps')
plt.close()
             
