# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 18


#Defining place of Data 
nnCellsX = 9
nnCellsY = 4

plt.ioff()

filename = 'exc_ham_'+ str(nnCellsX) + 'x' + str(nnCellsY) +'_DOS_eigs.dat'


savename = 'Excitonspectrum_' + str(nnCellsX) + 'x' + str(nnCellsY) + '_'


DOS = np.loadtxt(filename)




plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations $t = 0.3t_\perp$', fontsize=myFontSize)
#ax.set_yticklabels([])

plt.plot(DOS[:,0], DOS[:,1]/np.max(DOS[:,1]) , label=r'9x4', color='red', linewidth=1.0)


plt.legend(loc='upper right')
#plt.savefig(rootDir + folder + subfolder + savename + 'all.eps', format='eps')
plt.savefig(savename + 'hist.eps', format='eps')
#plt.close('all')


