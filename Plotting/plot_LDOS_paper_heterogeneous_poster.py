# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 09:09:37 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.ticker as ticker
import matplotlib as mpl


#Loading Data 
os.system(['clear','cls'][os.name == 'nt'])

#Figure properties
myFontSize = 20  
myFontSizeLabel = myFontSize - 2
cblim = 20
colormap = 'coolwarm'
cmap = plt.get_cmap(colormap)
cmap.set_under('white')
cblim_low = 0.2

#  # Plot with SANS-SERIF FONT.

rc('text', usetex='false') 
rc('font', family='sans-serif') 

#plt.ioff()



#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'

nnCellsXX = 9
nnCellsYY = 6

eps1 = 5
eps2 = 2

tfolder = '/'

subfolder = str(nnCellsXX)+'x'+str(nnCellsYY)+ '/' + str(eps1) + '_' + str(eps2) + tfolder + 'SpectralFunc/DirectCoordinates/de_0.1_broad_0.01/'
whole_path = rootDir + folder + subfolder

relative = 0

#electron_number = 3
f, axarr = plt.subplots(1, 4, sharex='col', sharey='row', figsize=1.05*plt.figaspect(0.25))
plt.subplots_adjust(hspace = .0, wspace = .0)
    
electron_number_array = np.array([1,5,9])
     
for oo in range(0,3):         
        
    electron_number = electron_number_array[oo]
    filename = 'spectral_function_elec' + str(electron_number) + '.dat'
    
    input_file =  whole_path + filename  
        
    filename_wo = 'spectral_function_woCoul_elec' + str(electron_number) + '.dat'
    input_file_wo = whole_path + filename_wo
    
    SpectralFunc = np.loadtxt(input_file)
    SpectralFunc_wo = np.loadtxt(input_file_wo)
    
    HolePos = np.loadtxt(whole_path + 'SpectralFunc_HolePos.dat')
    dim = HolePos.shape[1]-1
    HolePos = HolePos[:,0:dim]
    ElecPos = np.loadtxt(whole_path + 'SpectralFunc_ElecPos.dat')[:,0:dim]
        
    CurrentElec = ElecPos[electron_number,:]
    
    if relative == 1:
        RelativePos = HolePos - CurrentElec
        NormRelative = np.linalg.norm(RelativePos, axis = 1)
        sortind = np.argsort(NormRelative)
        NormRelative = NormRelative[sortind]
#    
        NormRelative_sort = NormRelative[sortind]
        sort_array = np.append(0, sortind + 1)
        SpectralFunc = SpectralFunc[:,sort_array]
        SpectralFunc_wo = SpectralFunc_wo[:, sort_array]


    #rc('axes', labelweight='100')
    
    #Creating same energy range for both data:
    
    Min_Spectral = np.min(SpectralFunc[:,0])
    Min_Spectral_wo = np.min(SpectralFunc_wo[:,0])
    Max_Spectral = np.max(SpectralFunc[:,0])
    Max_Spectral_wo = np.max(SpectralFunc_wo[:,0])
    
    Energy_min = np.min([Min_Spectral,Min_Spectral_wo])
    Energy_max = np.max([Max_Spectral,Max_Spectral_wo ])
    
    eps = SpectralFunc[1,0] - SpectralFunc[0,0]
    #
    if Min_Spectral > Energy_min:
        E_min_append = np.arange(Energy_min, Min_Spectral, eps)
        LDOS_append = np.zeros((E_min_append.shape[0],SpectralFunc.shape[1]))
        LDOS_append[:,0] = E_min_append
        SpectralFunc = np.append(LDOS_append, SpectralFunc,  axis = 0)
    #
    if Min_Spectral_wo > Energy_min:
        E_min_wo_append = np.arange(Energy_min, Min_Spectral_wo, eps)
        LDOS_append = np.zeros((E_min_wo_append.shape[0],SpectralFunc_wo.shape[1]))
        LDOS_append[:,0] = E_min_wo_append
        SpectralFunc_wo = np.append(LDOS_append, SpectralFunc_wo, axis = 0)
    #    
    if Max_Spectral < Energy_max:
        E_max_append = np.arange(Max_Spectral+eps, Energy_max+eps, eps)
        LDOS_append = np.zeros((E_max_append.shape[0],SpectralFunc.shape[1]))
        LDOS_append[:,0] = E_max_append
        SpectralFunc = np.append(SpectralFunc, LDOS_append, axis = 0)
    
    if Max_Spectral_wo < Energy_max:
        E_max_wo_append = np.arange(Max_Spectral_wo+eps, Energy_max+eps, eps)
        LDOS_append = np.zeros((E_max_wo_append.shape[0],SpectralFunc.shape[1]))
        LDOS_append[:,0] = E_max_wo_append
        SpectralFunc_wo = np.append(SpectralFunc_wo, LDOS_append, axis = 0)  
    #    
    

    if relative == 1:
       x = np.arange(0,19,2)
       a = axarr[0,oo].pcolor(x, SpectralFunc[:,0], SpectralFunc[:,1::2], cmap = plt.get_cmap(colormap),  vmax = cblim,linewidth = 0.,rasterized=True)
       b = axarr[1,oo].pcolor(x, SpectralFunc_wo[:,0], SpectralFunc_wo[:,1::2],cmap = plt.get_cmap(colormap),linewidth = 0.,rasterized=True)
       axarr[1,oo].set_xticks(x + 1)
       axarr[0,oo].set_xticks(x + 1)
       axarr[0,oo].set_xticklabels(np.round(NormRelative[0::2],2),fontsize = myFontSizeLabel)
       axarr[1,oo].set_xticklabels(np.round(NormRelative[0::2],2),fontsize = myFontSizeLabel)
       
       f.text(0.5, 0.03, r'Electron-hole distance ($\AA$)', fontsize=myFontSize, ha='center')
        
    else:
        x = np.arange(0,19,2)
        a = axarr[oo+1].pcolor(x, SpectralFunc[:,0], SpectralFunc[:,1::2], cmap = cmap, vmax = cblim, linewidth = 0.,rasterized=True)
        axarr[oo+1].set_xticks(x + 1)
        axarr[oo+1].set_xticklabels(np.arange(1,10))

    axarr[oo+1].axis([x.min(), x.max(), Energy_min, Energy_max])   
    axarr[oo+1].tick_params(axis='both', labelsize = myFontSizeLabel)
    axarr[oo+1].text(0.5,6.7, 'Correlated', va = 'top', ha = 'left', transform = axarr[oo+1].transData, color = 'white', fontsize = myFontSizeLabel)

   
   
b = axarr[0].pcolor(x,SpectralFunc_wo[:,0], SpectralFunc_wo[:,1::2],cmap = cmap, linewidth = 0.,rasterized=True)   
axarr[0].set_xticks(x + 1)
axarr[0].set_xticklabels(np.arange(1,10))
axarr[0].axis([x.min(), x.max(), Energy_min, Energy_max])
axarr[0].tick_params(axis='both', labelsize = myFontSizeLabel)
axarr[0].text(0.5,6.7, 'Quasi-particle limit', va = 'top', ha = 'left', transform = axarr[0].transData, color = 'white', fontsize = myFontSizeLabel)

axarr[0].set_ylabel('Energy (eV)', fontsize = myFontSize)
   
cbar1_ax = f.add_axes([0.965, 0.15, 0.01, 0.835])
cbar1 = f.colorbar(a, cax = cbar1_ax, orientation = 'vertical', ticklocation = 'right')
cbar1.ax.tick_params(labelsize=myFontSizeLabel, labelright = 'off')
cbar1.set_label('Spectral Function (arb. units)', fontsize=myFontSize)
cbar1.solids.set_rasterized(True)  
    
#    axarr[oo,0].yaxis.set_major_locator(ticker.MultipleLocator(1))
#    axarr[oo,0].set_ylabel('Energy (eV)', fontsize = myFontSize)
#    axarr[oo,0].tick_params(axis='both', labelsize = myFontSizeLabel)
#    axarr[oo,1].tick_params(axis='both', labelsize = myFontSizeLabel)
#
#    axarr[oo,0].text(0.5,6.7, 'Correlated', va = 'top', ha = 'left', transform = axarr[oo,0].transData, color = 'black', fontsize = myFontSizeLabel)
#    axarr[oo,1].text(0.5,6.7, 'Uncorrelated', va = 'top', ha = 'left', transform = axarr[oo,1].transData, color = 'black', fontsize = myFontSizeLabel)
#
#  
#    #plt.tight_layout()
#    
f.subplots_adjust(left=0.035, right = 0.96, top = 0.99, bottom = 0.15)
#    
# 
f.text(0.48, 0.02, r'unit cell of hole', fontsize=myFontSize, transform = f.transFigure, ha='center')

if relative == 1:
    f.savefig(whole_path + 'spectralfunc_map_relative' + str(electron_number) + '.eps', format='eps')
else:
    f.savefig(whole_path + 'spectralfunc_map_hetereogen_poster_' + colormap + '.eps', format='eps')
        
    #plt.close()
    

     
        