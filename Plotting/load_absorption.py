# -*- coding: utf-8 -*-
"""
Created on Wed May 18 08:10:52 2016

@author: csteinke
"""

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc

#Loading Data 
os.system(['clear','cls'][os.name == 'nt'])

myFontSize = 18
nnCellsX = 9
nnCellsY = 6

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
#folder = 'Data/Excitons/DiracMoS2/'
folder = 'Data/Excitons/DiracMoS2/'
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_5'
subfolder2 = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_5'
tfolder = '/' #'/t_inplane_2/'
#filename = 'supercell_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_5_15_periodicInE2_'
path = rootDir + folder + subfolder + tfolder + 'OpticalSpectra/'
path2 = rootDir + folder + subfolder2 + tfolder + 'OpticalSpectra/'

Broadening = 0.1
print('Loading Data')
#Loading Dipole elements

for Ex in range(0,2):
    for Ey in range(0,2):
        #for Ez in range(0,2):
        Ez = 0
    
        if (Ex + Ey + Ez) != 0:
            
            Polarization = np.array([Ex,Ey,Ez])
            Ending = str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2])
            
            print('Polarisation:' + str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2]))
            
            #Spectrum = np.loadtxt(path  +'OpticalSpectrum_broadened'+Ending+'.dat')
            Spectrum = np.loadtxt(path + 'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            #Spectrum_dens = np.loadtxt(path  +'OpticalSpectrum_densdens_broadened'+Ending+'.dat')            
            #Spectrum_wo = np.loadtxt(path  +'OpticalSpectrum_wocoulomb_broadened'+Ending+'.dat')
            Spectrum_wo= np.loadtxt(path + 'OpticalSpectrum_wocoulomb_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            #Spectrum5 = np.loadtxt(path2  +'OpticalSpectrum_broadened'+Ending+'.dat' )
            
            plt.ioff()
            
            plt.figure()
            ax = plt.axes()
            rc('text', usetex=True)	
            rc('axes', labelweight='200')
            plt.xlabel('Energy (eV)', fontsize=myFontSize)
            plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
            StrElecField = '('+str(Polarization[0]) +',' + str(Polarization[1]) + ',' + str(Polarization[2]) + ')'
            plt.title(r'Absorption spectrum $\textbf{E} = ' + StrElecField + '$', fontsize=myFontSize)
            
           
            plt.plot(Spectrum[:,0], Spectrum[:,1], label=r'$Full$', color='red', linewidth=1.0)
            #plt.plot(Spectrum_dens[:,0], Spectrum_dens[:,1], label=r'Density Density spectrum', color='black', linewidth=1.0)
            plt.plot(Spectrum_wo[:,0], Spectrum_wo[:,1], label=r'Spectrum without Coulomb', color='green', linewidth=1.0)
            #plt.plot(Spectrum5[:,0], Spectrum5[:,1], label=r'$\varepsilon = 5$', color='blue', linewidth=1.0)
            
            plt.legend(loc='upper right', borderaxespad = 0)                       
            
            plt.savefig(path +'OpticalSpectrum_vgl_broadened_'+str(Broadening) + '_' +Ending+'.eps', format='eps')
            #plt.savefig(path +'OpticalSpectrum_vgl_broadened' +Ending+'.eps', format='eps')
            plt.close('')