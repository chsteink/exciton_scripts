# -*- coding: utf-8 -*-
"""
Created on Tue Jun 21 13:25:56 2016

@author: csteinke
"""

#Calculating and plotting the contribution of different basis vectors (|elec>|hole>)
#from chosen eigenvalue of excitonic spectrum

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 18


#Defining place of Data 
nnCellsX = 9
nnCellsY = 6

plt.ioff()

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_2'
tfolder = '/'
filename = 'exc_ham_'+ str(nnCellsX) + 'x' + str(nnCellsY) 

path = rootDir + folder + subfolder + tfolder + filename

Eigenvec = np.loadtxt(path + '_wocoulomb_eigenvec.dat')

eigenvalue_number = 0
nN_elec = nnCellsX * nnCellsY *4/2
nN_hole = nnCellsX * nnCellsY *4/2

Eigenvec_Mat = np.reshape(Eigenvec[:,eigenvalue_number],(nN_elec, nN_hole))
Eigenvec_Mat = np.abs(Eigenvec_Mat)**2
#resulting matrix: (|11> |12> |13>
#                    |21> 22> |23> 
#                   |31> |32> |33> ) with |eh> 

plt.matshow(Eigenvec_Mat.T)

plt.title('Contributions of states')
plt.xlabel('hole index')
plt.ylabel('electron index')
plt.colorbar()

plt.savefig(path + '_contributing_wo_log.eps', format='eps')
