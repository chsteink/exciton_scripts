# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 18


#Defining place of Data 
nnCellsX = 9
nnCellsY = 4

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'

filename = 'exc_ham_'+ str(nnCellsX) + 'x' + str(nnCellsY) +'_DOS_eigs.dat'


savename = 'Excitonspectrum_' + str(nnCellsX) + 'x' + str(nnCellsY) + '_'
#subfolder = 'Dielectric/5x5/'

eps1 = 1
eps2 = 10

#Loading file 1: eps2/eps1 = 1
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/tperp_1/t_inplane_0.3/'+str(eps1)+'_' + str(eps2) +'/'
input_file = rootDir + folder + subfolder +filename
DOS_03 = np.loadtxt(input_file)

subfolder = str(nnCellsX) + 'x' + str(nnCellsY) +  '/tperp_1/t_inplane_0.3/'+str(eps1)+'_' + str(eps1) +'/'
input_file = rootDir + folder + subfolder +filename
DOS_001_55 = np.loadtxt(input_file)
##
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/tperp_1/t_inplane_0.3/'+str(eps2)+'_' + str(eps2) +'/'
input_file = rootDir + folder + subfolder +filename
DOS_001_1515 = np.loadtxt(input_file)

##
##File 2: eps2/eps1 = 1.5
#subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_15/t_inplane_0.2/'
#input_file = rootDir + folder + subfolder +filename
#DOS_02 = np.loadtxt(input_file)
#
##File 3: eps2/eps1 = 2
#subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_15/t_inplane_0.1/'
#input_file = rootDir + folder + subfolder +filename
#DOS_01 = np.loadtxt(input_file)
#
#subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_15/t_inplane_0.05/'
#input_file = rootDir + folder + subfolder +filename
#DOS_005 = np.loadtxt(input_file)
#
#subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_15/t_inplane_0.01/'
#input_file = rootDir + folder + subfolder +filename
#DOS_001 = np.loadtxt(input_file)
#



plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations $t = 0.3t_\perp$', fontsize=myFontSize)
#ax.set_yticklabels([])

#plt.plot(DOS_02[:,0], DOS_02[:,1] , label=r'$t = 0.2t_\perp$', color='black', linewidth=1.0)
#plt.plot(DOS_01[:,0], DOS_01[:,1] , label=r'$t = 0.1t_\perp$', color='blue', linewidth=1.0)
#plt.plot(DOS_005[:,0], DOS_005[:,1] , label=r'$t = 0.05_\perp$', color='green', linewidth=1.0)
#plt.plot(DOS_001[:,0], DOS_001[:,1] , label=r'$t = 0.01_\perp$', color='magenta', linewidth=1.0)
plt.plot(DOS_001_55[:,0], DOS_001_55[:,1] , label=r'$\varepsilon = $ '+str(eps1), color='black', linewidth=1.0)
plt.plot(DOS_001_1515[:,0], DOS_001_1515[:,1] , label=r'$\varepsilon =$' + str(eps2), color='blue', linewidth=1.0)
plt.plot(DOS_03[:,0], DOS_03[:,1] , label=r'Dielectric', color='red', linewidth=1.0)



plt.legend(loc='upper left')
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/tperp_1/t_inplane_0.3/'+str(eps1)+'_' + str(eps2) +'/'
#plt.savefig(rootDir + folder + subfolder + savename + 'all.eps', format='eps')
plt.savefig(rootDir + folder + subfolder +  savename + 'homogen_vgl.eps', format='eps')
#plt.close('all')


