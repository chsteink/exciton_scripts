# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 18


#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/12x6/'
filename = 'exc_ham_12x6_DOS_eigs.dat'

savename = 'Excitonspectrum_12x6_'
#subfolder = 'Dielectric/5x5/'


#Loading file 1: eps2/eps1 = 1
subfolder = '15_15/'
input_file = rootDir + folder + subfolder +filename
DOS_15 = np.loadtxt(input_file)

#File 2: eps2/eps1 = 1.5
subfolder = '5_5/'
input_file = rootDir + folder + subfolder +filename
DOS_5 = np.loadtxt(input_file)

#File 3: eps2/eps1 = 2
subfolder = '5_15/'
input_file = rootDir + folder + subfolder +filename
DOS_515 = np.loadtxt(input_file)

plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations 12x6', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_515[:,0], DOS_515[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.plot(DOS_5[:,0], DOS_5[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 5$', color='black', linewidth=1.0)
plt.plot(DOS_15[:,0], DOS_15[:,1] , label=r'$\varepsilon_1=15, \varepsilon_2 = 15$', color='blue', linewidth=1.0)

plt.legend()
plt.savefig(rootDir + folder + savename + 'all.eps', format='eps')




