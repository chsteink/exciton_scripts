# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""

from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 20


#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/9x6/'
filename = 'exc_ham_9x6_DOS_eigs.dat'

savename = 'Excitonspectrum_9x6_'
#subfolder = 'Dielectric/5x5/'


plt.close('all')
#Loading homogeneous data
subfolder = '5_5/'
input_file = rootDir + folder + subfolder +filename
DOS_5hom = np.loadtxt(input_file)
Eigenvals_5wocoul = np.loadtxt(rootDir + folder + subfolder + 'exc_ham_9x6_wocoulomb_eigenvals.dat')
Min_DOS = np.min(DOS_5hom[:,0])
Max_DOS=np.max(Eigenvals_5wocoul)
DOS_range = (Min_DOS, Max_DOS)
[DOS_5wocoul,binEdges] = np.histogram(Eigenvals_5wocoul,bins=200, range=DOS_range)
bincenters = 0.5*(binEdges[1:]+binEdges[:-1])


subfolder = '2_2/'
input_file = rootDir + folder + subfolder + filename
DOS_2hom = np.loadtxt(input_file)


subfolder = '10_10/'
input_file = rootDir + folder + subfolder +filename
DOS_10hom = np.loadtxt(input_file)



plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.tick_params(labelsize=myFontSize-2)
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.3,6.5])
plt.ylim([0,1.05])
plt.xticks(np.arange(1.5,7, 0.5))

plt.plot(DOS_2hom[:,0],DOS_2hom[:,1]/np.max(DOS_2hom[:,1]), label = r'$\varepsilon = 2$', color ='black', linewidth = 1.0)
plt.legend(loc='upper center', bbox_to_anchor = (0.6, 1), fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.93, right = 0.97)


plt.savefig(rootDir + folder + savename + 'homogen_vortrag1.eps', format='eps')

plt.figure(2)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.tick_params(labelsize=myFontSize-2)
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.3,6.5])
plt.ylim([0,1.05])
plt.xticks(np.arange(1.5,7, 0.5))


plt.plot(DOS_2hom[:,0],DOS_2hom[:,1]/np.max(DOS_2hom[:,1]), label = r'$\varepsilon = 2$', color ='black', linewidth = 1.0)
plt.plot(DOS_5hom[:,0],DOS_5hom[:,1]/np.max(DOS_5hom[:,1]), label = r'$\varepsilon = 5$', color ='blue', linewidth = 1.0)
plt.legend(loc='upper center', bbox_to_anchor = (0.6, 1), fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.93, right = 0.97)


plt.savefig(rootDir + folder + savename + 'homogen_vortrag2.eps', format='eps')
#
plt.figure(3)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.tick_params(labelsize=myFontSize-2)
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.3,6.5])
plt.ylim([0,1.05])
plt.xticks(np.arange(1.5,7, 0.5))


plt.plot(DOS_2hom[:,0],DOS_2hom[:,1]/np.max(DOS_2hom[:,1]), label = r'$\varepsilon = 2$', color ='black', linewidth = 1.0)
plt.plot(DOS_5hom[:,0],DOS_5hom[:,1]/np.max(DOS_5hom[:,1]), label = r'$\varepsilon = 5$', color ='blue', linewidth = 1.0)
plt.plot(DOS_10hom[:,0],DOS_10hom[:,1]/np.max(DOS_10hom[:,1]), label = r'$\varepsilon = 10$', color ='green', linewidth = 1.0)
plt.legend(loc='upper center', bbox_to_anchor = (0.6, 1), fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.93, right = 0.97)


plt.savefig(rootDir + folder + savename + 'homogen_vortrag3.eps', format='eps')

plt.figure(4)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.tick_params(labelsize=myFontSize-2)
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.3,5])
plt.ylim([0,1.05])
plt.xticks(np.arange(1.5,5, 0.5))


plt.plot(bincenters,DOS_5wocoul/np.max(DOS_5wocoul), label = r'$\varepsilon = 5$, Joint DOS', color ='green', linewidth = 1.0)
#plt.plot(DOS_5hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 5$, Full Coulomb', color ='blue', linewidth = 1.0)

plt.legend(loc='upper left',fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.93, right = 0.97)


plt.savefig(rootDir + folder + savename + 'joint_vortrag1.eps', format='eps')

plt.figure(5)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.tick_params(labelsize=myFontSize-2)
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.3,5])
plt.ylim([0,1.05])
plt.xticks(np.arange(1.5,5, 0.5))


plt.plot(bincenters,DOS_5wocoul/np.max(DOS_5wocoul), label = r'$\varepsilon = 5$, Joint DOS', color ='green', linewidth = 1.0)
plt.plot(DOS_5hom[:,0],DOS_5hom[:,1]/np.max(DOS_5hom[:,1]), label = r'$\varepsilon = 5$, Full Coulomb', color ='blue', linewidth = 1.0)

plt.legend(loc='upper left',fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.93, right = 0.97)


plt.savefig(rootDir + folder + savename + 'joint_vortrag2.eps', format='eps')

