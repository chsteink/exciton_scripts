# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""

from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc


os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 20
binnumber = 100

#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/9x6/'
filename = 'exc_ham_9x6_eigenvals_eigs.dat'
tfolder = '/t_inplane_2/'

savefolder = folder + 'Spectra_tinplane_2/'
savename = 'Excitonspectrum_9x6_'
#subfolder = 'Dielectric/5x5/'

plt.close('all')
#Loading homogeneous data
subfolder = '5_5'
input_file = rootDir + folder + subfolder + tfolder + filename
Eigenvals_5hom = np.loadtxt(input_file)
Min5 = np.min(Eigenvals_5hom)
Max5 = np.max(Eigenvals_5hom)

subfolder = '2_2'
input_file = rootDir + folder + subfolder + tfolder + filename
Eigenvals_2hom = np.loadtxt(input_file)
Min2 = np.min(Eigenvals_2hom)
Max2 = np.max(Eigenvals_2hom)

subfolder = '1000000_1000000'
Eigenvals_infty = np.loadtxt(rootDir + folder + subfolder + tfolder +  filename)
MinInf = np.min(Eigenvals_infty)
MaxInf = np.max(Eigenvals_infty)

subfolder = '5_2/'
input_file = rootDir + folder + subfolder + tfolder + filename
Eigenvals_52 = np.loadtxt(input_file)




Min_DOS = np.min([Min5,Min2,MinInf])
Max_DOS=np.max([Max5,Max2,MaxInf])
DOS_range = (Min_DOS, Max_DOS)

[DOS_5hom,binEdges] = np.histogram(Eigenvals_5hom,bins=binnumber,range=DOS_range)
bincenters5 = 0.5*(binEdges[1:]+binEdges[:-1])
[DOS_2hom, binEdges] = np.histogram(Eigenvals_2hom,bins=binnumber)
bincenters2 = 0.5*(binEdges[1:]+binEdges[:-1])
[DOS_Inf, binEdges] = np.histogram(Eigenvals_infty,bins=binnumber, range=DOS_range)
bincentersInf = 0.5*(binEdges[1:]+binEdges[:-1])

[DOS_52, binEdges] = np.histogram(Eigenvals_52,bins=binnumber, range=DOS_range)
bincenters52 = 0.5*(binEdges[1:]+binEdges[:-1])

DOS52_hom = 1./2*(DOS_5hom + DOS_2hom)


plt.figure(1)

ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.tick_params(labelsize=myFontSize)
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
#plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
#plt.xlim([1.3,6.5])
plt.ylim([0,1.05])
#plt.xticks(np.arange(1.5,7, 0.5))


plt.plot(bincentersInf,DOS_Inf/np.max(DOS_Inf), label = r'$\varepsilon = \infty$', color ='black', linewidth = 1.5)
plt.plot(bincenters5,DOS_5hom/np.max(DOS_5hom), label = r'$\varepsilon = 5$', color ='blue', linewidth = 1.5)
plt.plot(bincenters2,DOS_2hom/np.max(DOS_2hom), label = r'$\varepsilon = 2$', color ='green', linewidth = 1.5)

plt.legend(loc='upper right',fontsize=myFontSize-2)  #bbox_to_anchor = (0.6, 1), fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.96, right = 0.97)


plt.savefig(rootDir + savefolder + savename + 'homogen_paper.eps', format='eps')

plt.figure(2)

ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.tick_params(labelsize=myFontSize)
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
#plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
#plt.xlim([1.3,6.5])
plt.ylim([0,1.05])
#plt.xticks(np.arange(1.5,7, 0.5))


plt.plot(bincenters52,DOS52_hom/np.max(DOS52_hom), label=r'Hom. environment', color ='grey', linewidth = 1.5)
plt.plot(bincenters52,DOS_52/np.max(DOS_52), label=r'Het. environment', color ='red', linewidth = 1.5)

plt.legend(loc='upper right', fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.96, right = 0.97)


plt.savefig(rootDir + savefolder + savename + 'dielectric_paper.eps', format='eps')

#Absorption probabilities homogeneous
plt.figure(3)

subfolder = '/2_2'
subfolder2 = '/5_5'
subfolder3 = '/1000000_1000000'

#filename = 'supercell_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_5_15_periodicInE2_'
path = rootDir + folder + subfolder + tfolder + 'OpticalSpectra/'
path2 = rootDir + folder + subfolder2 + tfolder + 'OpticalSpectra/'
path3 = rootDir + folder + subfolder3 + tfolder + 'OpticalSpectra/'

Polarization = np.array([1,1,0])
Ending = str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2])
Spectrum2 = np.loadtxt(path  +'OpticalSpectrum_broadened'+Ending+'.dat')
Spectrum5 = np.loadtxt(path2  +'OpticalSpectrum_broadened'+Ending+'.dat' )
Spectrum_TB = np.loadtxt(path3  +'OpticalSpectrum_broadened'+Ending+'.dat')


ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
StrElecField = '('+str(Polarization[0]) +',' + str(Polarization[1]) + ',' + str(Polarization[2]) + ')'
plt.title(r'Absorption spectrum $\textbf{E} = ' + StrElecField + '$', fontsize=myFontSize)
ax.set_yticklabels([])
#plt.xlim([1.3,6.5])

#plt.xticks(np.arange(1.5,7, 0.5))
plt.tick_params(labelsize=myFontSize)

#plt.plot(Spectrum_TB[:,0], Spectrum_TB[:,1], label=r'$\varepsilon = \infty$', color='black', linewidth=1.5)
plt.plot(Spectrum5[:,0], Spectrum5[:,1], label=r'$\varepsilon = 5$', color='blue', linewidth=1.5)
plt.plot(Spectrum2[:,0], Spectrum2[:,1], label=r'$\varepsilon = 2$', color='green', linewidth=1.5)



plt.legend(loc='upper right', fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.96, right = 0.97)                      

plt.savefig(rootDir + savefolder +'OpticalSpectrum_homogen_paper'+Ending+'.eps', format='eps')

plt.figure(4)
subfolder = '/5_2/'
path = rootDir + folder + subfolder + tfolder + 'OpticalSpectra/'
Spectrum52 = np.loadtxt(path  +'OpticalSpectrum_broadened'+Ending+'.dat')

ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
StrElecField = '('+str(Polarization[0]) +',' + str(Polarization[1]) + ',' + str(Polarization[2]) + ')'
plt.title(r'Absorption spectrum $\textbf{E} = ' + StrElecField + '$', fontsize=myFontSize)
ax.set_yticklabels([])
#plt.xlim([1.3,6.5])
plt.ylim([0,1.05])
#plt.xticks(np.arange(1.5,7, 0.5))
plt.tick_params(labelsize=myFontSize)

plt.plot(bincenters52, DOS_52/np.max(DOS_52), label=r'Full spectrum', color='grey', linewidth=1.5)
plt.plot(Spectrum52[:,0], Spectrum52[:,1]/np.max(Spectrum52[:,1]), label=r'Absorption spectrum', color='red', linewidth=1.5)

plt.legend(loc='upper right', fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.96, right = 0.97)                      

plt.savefig(rootDir + savefolder +'OpticalSpectrum_dielectric_paper'+Ending+'.eps', format='eps')

plt.figure(5)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
StrElecField = '('+str(Polarization[0]) +',' + str(Polarization[1]) + ',' + str(Polarization[2]) + ')'
plt.title(r'Absorption spectrum $\textbf{E} = ' + StrElecField + '$', fontsize=myFontSize)
ax.set_yticklabels([])
#plt.xlim([1.3,6.5])
plt.ylim([0,1.05])
#plt.xticks(np.arange(1.5,7, 0.5))
plt.tick_params(labelsize=myFontSize)


plt.plot(bincenters5, DOS_52/np.max(DOS_52), label=r'Full spectrum', color='grey', linewidth=1.5)
plt.plot(Spectrum52[:,0], Spectrum52[:,1]/np.max(Spectrum52[:,1]), label=r'Absorption spectrum', color='red', linewidth=1.5)

plt.legend(loc='upper right', fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.96, right = 0.97)                      

plt.savefig(rootDir + savefolder +'OpticalSpectrum_dielectric_paper'+Ending+'.eps', format='eps')



plt.figure(6)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
StrElecField = '('+str(Polarization[0]) +',' + str(Polarization[1]) + ',' + str(Polarization[2]) + ')'
plt.title(r'Absorption spectrum $\textbf{E} = ' + StrElecField + '$', fontsize=myFontSize)
ax.set_yticklabels([])
#plt.xlim([1.3,6.5])

#plt.xticks(np.arange(1.5,7, 0.5))
plt.tick_params(labelsize=myFontSize)


plt.plot(Spectrum5[:,0], Spectrum5[:,1], label=r'$\varepsilon = 5$', color='blue', linewidth=1.5)
plt.plot(Spectrum2[:,0], Spectrum2[:,1], label=r'$\varepsilon = 2$', color='green', linewidth=1.5)
plt.plot(Spectrum52[:,0], Spectrum52[:,1], label=r'$\varepsilon_1 / \varepsilon_2 = 5/2$', color='red', linewidth=1.5)

plt.legend(loc='upper right', fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.96, right = 0.97)             

plt.savefig(rootDir + savefolder +'OpticalSpectrum_vgl_homogen_dielectric'+Ending+'.eps', format='eps')



path = rootDir + folder 
Spectrum52_sum = np.loadtxt(path + 'OpticalSpectrum_broadened_sum2_5_'+Ending+'.dat')

plt.figure(7)

ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')

plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
StrElecField = '('+str(Polarization[0]) +',' + str(Polarization[1]) + ',' + str(Polarization[2]) + ')'
plt.title(r'Absorption spectrum $\textbf{E} = ' + StrElecField + '$', fontsize=myFontSize)
ax.set_yticklabels([])
#plt.xlim([1.3,6.5])

#plt.xticks(np.arange(1.5,7, 0.5))
plt.tick_params(labelsize=myFontSize)


plt.plot(Spectrum52_sum[:,0], Spectrum52_sum[:,1], label=r'Hom', color='grey', linewidth=1.5)
plt.plot(Spectrum52[:,0], Spectrum52[:,1], label=r'Het.', color='red', linewidth=1.5)

plt.legend(loc='upper right', fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.96, right = 0.97)             

plt.savefig(rootDir + savefolder +'OpticalSpectrum_vgl_homogen_dielectric_sum'+Ending+'.eps', format='eps')