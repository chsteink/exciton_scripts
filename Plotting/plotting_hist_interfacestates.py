# -*- coding: utf-8 -*-
"""
Created on Mon Feb  8 13:54:28 2016

@author: csteinke
"""

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms 
myFontSize = 18

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/Hamiltonian/'
save_folder = 'Excitons/Plots/WF/9x6/'

print('Loading eigenvalues, indices and DOS')
Eigenvals = np.diag(np.loadtxt(rootDir + folder + 'exc_ham_9x6_eigenvals.dat'))
DOS = np.loadtxt(rootDir + folder + 'exc_ham_9x6_DOS.dat')

Electron_indices = np.loadtxt(rootDir + save_folder + 'electron_interfacestates.dat') -1
Electron_indices = Electron_indices.astype(int)
Hole_indices = np.loadtxt(rootDir + save_folder + 'hole_interfacestates.dat') - 1 
Hole_indices = Hole_indices.astype(int)

#Getting Energies to corresponding indices of interface states

print('Getting corresponding energies')
Interface_electron_energy = Eigenvals[Electron_indices]
Interface_hole_energy = Eigenvals[Hole_indices]


#For range minimum and maximum: binEdges needed, not centers
Centers = DOS[:,0]
Diff = np.abs(Centers[0]-Centers[1])
binEdges =  Centers - Diff/2
binEdges = np.append(binEdges, Centers[-1] + Diff)


print('Plotting')
rc('text', usetex=True)	
[Hist_holes, binEdges] = np.histogram(Interface_hole_energy,bins = DOS.shape[0], range=(binEdges[0],binEdges[-1]))
[Hist_elec, binEdges] = np.histogram(Interface_electron_energy,bins = DOS.shape[0], range=(binEdges[0],binEdges[-1]))
bincenters =  0.5 * (binEdges[1:] + binEdges[:-1])

fig1 = plt.figure()
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)

hole_x = bincenters[np.where(Hist_holes !=0)]
hole_y = np.transpose(DOS[np.where(Hist_holes !=0), 1])

elec_x=bincenters[np.where(Hist_elec !=0)]
elec_y = np.transpose(DOS[np.where(Hist_elec != 0),1])

plt.plot(bincenters, DOS[:,1] , label=r'Excitonic gap $(TB+HF+BSE)$', color='black', linewidth=1.0)
plt.plot(hole_x, hole_y, 'ro', label=r'Holes at interface' )
plt.plot(elec_x, elec_y, 'bo', markersize = 4, label=r'Electrons at interface')
#plt.savefig(rootDir + save_folder + 'Interface.eps', format='eps')