# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 08:28:49 2016

@author: csteinke
"""
from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc


rc('text', usetex=True)

myFontSize = 18    
myFontSizeLabel = 14

nnCellsX = 9
nnCellsY = 6


rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folderDirac = 'Data/Excitons/DiracMoS2/'
folderHex = 'Data/Excitons/HexLadder/'
tfolderH1 = '/OpticalSpectra/' #'/t_0.5/' #
tfolderH2 = '/t_inplane_2/OpticalSpectra/'
tfolderD1 = '/t_0.5/OpticalSpectra/'
tfolderD2 = '/OpticalSpectra/' 

savefolder = 'Data/Excitons/'

eps1 = 5
eps2 = 2

subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps1)+'_' + str(eps2)


pathH1 = rootDir + folderHex + subfolder + tfolderH1
pathH2 = rootDir + folderHex + subfolder + tfolderH2
pathD1 = rootDir + folderDirac + subfolder + tfolderD1
pathD2 = rootDir + folderDirac + subfolder + tfolderD2

Broadening = 0.1 #0.005

for Ex in range(0,2):
    for Ey in range(0,2):
        #for Ez in range(0,2):
        Ez = 0
    
        if (Ex + Ey + Ez) != 0:
            
            Polarization = np.array([Ex,Ey,Ez])
            Ending = str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2])
            
            print('Polarisation:' + str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2]))
            

            SpectrumH1 = np.loadtxt(pathH1 + 'OpticalSpectrum_broadened' +Ending+'.dat')
            SpectrumH1_WO= np.loadtxt(pathH1 + 'OpticalSpectrum_wocoulomb_broadened' +Ending+'.dat')
            
            SpectrumH2 = np.loadtxt(pathH2 + 'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            SpectrumH2_WO= np.loadtxt(pathH2 + 'OpticalSpectrum_wocoulomb_broadened_'+str(Broadening) + '_' +Ending+'.dat')

            SpectrumD1 = np.loadtxt(pathD1 + 'OpticalSpectrum_broadened' +Ending+'.dat')
            SpectrumD1_WO= np.loadtxt(pathD1 + 'OpticalSpectrum_wocoulomb_broadened' +Ending+'.dat')
            
            SpectrumD2 = np.loadtxt(pathD2 + 'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            SpectrumD2_WO= np.loadtxt(pathD2 + 'OpticalSpectrum_wocoulomb_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            
            
            
            
            ############global stuff################################
            f, axarr = plt.subplots(2, 2, sharex='col', sharey='row', figsize=plt.figaspect(0.5))
            
            plt.subplots_adjust(bottom = 0.12, left = 0.05, top = 0.96, right = 0.96)       
            
            plt.subplots_adjust(hspace = .0, wspace = .0)
            
            
            axarr[1,0].tick_params(labelsize=myFontSizeLabel)
            axarr[1,1].tick_params(labelsize=myFontSizeLabel)
            
            axarr[0,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
            axarr[1,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
            
            axarr[0,1].yaxis.set_label_position("right")
            axarr[0,1].set_ylabel('Hexagonal ladder', fontsize = myFontSize-2)
            
            axarr[1,1].yaxis.set_label_position("right")
            axarr[1,1].set_ylabel('Dirac Model', fontsize = myFontSize-2)
            
            
            axarr[0,0].text(0.04, 0.95, r'$t = 0.255$', transform=axarr[0,0].transAxes,
                  fontsize=myFontSizeLabel,va='top')
            axarr[0,1].text(0.04, 0.95, r'$t = 1.7$', transform=axarr[0,1].transAxes,
                  fontsize=myFontSizeLabel,va='top')
            axarr[1,0].text(0.04, 0.95, r'$t = 0.5$', transform=axarr[1,0].transAxes,
                  fontsize=myFontSizeLabel,va='top')
            axarr[1,1].text(0.04, 0.95, r'$t = 1.1$', transform=axarr[1,1].transAxes,
                  fontsize=myFontSizeLabel,va='top')
                  
            f.text(0.5, 0.03, 'Energy (eV)', fontsize=myFontSize, ha='center')
            f.text(0.03,0.5, 'Absorption (arb. units)', fontsize = myFontSize, ha = 'center', va='center', rotation = 90)
#
            ################plotting stuff############################      
            axarr[0,0].plot(SpectrumH1[:,0], SpectrumH1[:,1], label='Full Coulomb')
            axarr[0,0].plot(SpectrumH1_WO[:,0], SpectrumH1_WO[:,1], label='Joint DOS')
            
            
            axarr[0,1].plot(SpectrumH2[:,0], SpectrumH2[:,1], label='Full Coulomb')
            axarr[0,1].plot(SpectrumH2_WO[:,0], SpectrumH2_WO[:,1], label='Joint DOS')

            axarr[1,0].plot(SpectrumD1[:,0], SpectrumD1[:,1], label='Full Coulomb')
            axarr[1,0].plot(SpectrumD1_WO[:,0], SpectrumD1_WO[:,1], label='Joint DOS')

            axarr[1,1].plot(SpectrumD2[:,0], SpectrumD2[:,1], label='Full Coulomb')
            axarr[1,1].plot(SpectrumD2_WO[:,0], SpectrumD2_WO[:,1], label='Joint DOS')
            
            axarr[1,0].get_xticklabels()[-1].set_visible(False)
            axarr[0,1].set_xlim([2,20])
            
            axarr[1,1].legend()
#
            savename = rootDir + savefolder + 'OpticalSpectrum_'+str(eps1)+'_'+str(eps2)+'_' +Ending+'.eps'
            plt.savefig(savename, format = 'eps')
#           
            plt.close()
#
