# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 08:28:49 2016

@author: csteinke
"""
from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc


rc('text', usetex=True)

myFontSize = 18    
myFontSizeLabel = 14

nnCellsX = 9
nnCellsY = 6


rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folderDirac = 'Data/Excitons/DiracMoS2/'
folderHex = 'Data/Excitons/HexLadder/'
tfolderH1 = '/' #'/t_0.5/' #
tfolderH2 = '/t_inplane_2/'
tfolderD1 = '/t_0.5/'
tfolderD2 = '/' 

subfolderOpt = 'OpticalSpectra/'

savefolder = 'Data/Excitons/'

eps1 = 2
eps2 = 2

eps12 = 5
eps22 = 5

subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps1)+'_' + str(eps2)
subfolder2 = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps12)+'_' + str(eps22)

pathH1 = rootDir + folderHex + subfolder + tfolderH1
pathH1_2 = rootDir + folderHex + subfolder2 + tfolderH1

pathH2 = rootDir + folderHex + subfolder + tfolderH2
pathH2_2 = rootDir + folderHex + subfolder2 + tfolderH2

pathD1 = rootDir + folderDirac + subfolder + tfolderD1
pathD1_2 = rootDir + folderDirac + subfolder2 + tfolderD1

pathD2 = rootDir + folderDirac + subfolder + tfolderD2
pathD2_2 = rootDir + folderDirac + subfolder2 + tfolderD2


filenameFull = 'exc_ham_'+str(nnCellsX)+'x'+str(nnCellsY)+'_DOS_eigs.dat'

DOSH1 = np.loadtxt(pathH1 + filenameFull)
DOSH1_2 = np.loadtxt(pathH1_2 + filenameFull)

DOSH2 = np.loadtxt(pathH2 + filenameFull)
DOSH2_2 = np.loadtxt(pathH2_2 + filenameFull)

DOSD1 = np.loadtxt(pathD1 + filenameFull)
DOSD1_2 = np.loadtxt(pathD1_2 + filenameFull)

DOSD2 = np.loadtxt(pathD2 + filenameFull)
DOSD2_2 = np.loadtxt(pathD2_2 + filenameFull)

Broadening = 0.1 #0.005

for Ex in range(0,2):
    for Ey in range(0,2):
        #for Ez in range(0,2):
        Ez = 0
    
        if (Ex + Ey + Ez) != 0:
            
            Polarization = np.array([Ex,Ey,Ez])
            Ending = str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2])
            
            print('Polarisation:' + str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2]))
            

            SpectrumH1 = np.loadtxt(pathH1 + subfolderOpt + 'OpticalSpectrum_broadened' +Ending+'.dat')
            SpectrumH1_2= np.loadtxt(pathH1_2 + subfolderOpt + 'OpticalSpectrum_broadened' +Ending+'.dat')
            
            SpectrumH2 = np.loadtxt(pathH2 + subfolderOpt+'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            SpectrumH2_2= np.loadtxt(pathH2_2 + subfolderOpt+'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')

            SpectrumD1 = np.loadtxt(pathD1 + subfolderOpt + 'OpticalSpectrum_broadened' +Ending+'.dat')
            SpectrumD1_2= np.loadtxt(pathD1_2 + subfolderOpt + 'OpticalSpectrum_broadened' +Ending+'.dat')
            
            SpectrumD2 = np.loadtxt(pathD2 + subfolderOpt + 'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            SpectrumD2_2= np.loadtxt(pathD2_2 +  subfolderOpt + 'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')
            
            
            
            
            ############global stuff################################
            f, axarr = plt.subplots(2, 2, sharex='col', sharey='row', figsize=plt.figaspect(0.5))
            
                    
            plt.subplots_adjust(bottom = 0.12, left = 0.05, top = 0.96, right = 0.96)       
            
            plt.subplots_adjust(hspace = .0, wspace = .0)
            
            
            axarr[1,0].tick_params(labelsize=myFontSizeLabel)
            axarr[1,1].tick_params(labelsize=myFontSizeLabel)
            
            axarr[0,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
            axarr[1,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
            
            axarr[0,1].yaxis.set_label_position("right")
            axarr[0,1].set_ylabel('Hexagonal ladder', fontsize = myFontSize-2)
            
            axarr[1,1].yaxis.set_label_position("right")
            axarr[1,1].set_ylabel('Dirac Model', fontsize = myFontSize-2)
            
            
            axarr[0,0].text(0.04, 0.95, r'$t = 0.255$', transform=axarr[0,0].transAxes,
                  fontsize=myFontSizeLabel,va='top')
            axarr[0,1].text(0.04, 0.95, r'$t = 1.7$', transform=axarr[0,1].transAxes,
                  fontsize=myFontSizeLabel,va='top')
            axarr[1,0].text(0.04, 0.95, r'$t = 0.5$', transform=axarr[1,0].transAxes,
                  fontsize=myFontSizeLabel,va='top')
            axarr[1,1].text(0.04, 0.95, r'$t = 1.1$', transform=axarr[1,1].transAxes,
                  fontsize=myFontSizeLabel,va='top')
                  
            f.text(0.5, 0.03, 'Energy (eV)', fontsize=myFontSize, ha='center')
            f.text(0.03,0.5, 'Absorption (arb. units)', fontsize = myFontSize, ha = 'center', va='center', rotation = 90)
#
            ################plotting stuff############################     
           # ax2  = axarr[0,0].twinx()   
            #ax2.plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = 2$, Full', color = '0.75') 
            #ax2.plot(DOSH1_2[:,0], DOSH1_2[:,1], label=r'$\varepsilon = 5$,Full', color='0.5')
            axarr[0,0].plot(SpectrumH1[:,0], SpectrumH1[:,1], label=r'$\varepsilon = 2$', color='blue')
            axarr[0,0].plot(SpectrumH1_2[:,0], SpectrumH1_2[:,1], label=r'$\varepsilon = 5$', color='red')
            
            #factor = 700/0.0000007
           # axarr[0,0].plot(DOSH1[:,0], DOSH1[:,1]/factor, label=r'$\varepsilon = 2$, Full', color = '0.75')      
           # axarr[0,0].plot(DOSH1_2[:,0], DOSH1_2[:,1]/factor, label=r'$\varepsilon = 5$,Full', color='0.5')
            
            axarr[0,1].plot(SpectrumH2[:,0], SpectrumH2[:,1], label=r'$\varepsilon = 2$',  color='blue')
            axarr[0,1].plot(SpectrumH2_2[:,0], SpectrumH2_2[:,1], label=r'$\varepsilon = 5$', color='red')

            axarr[1,0].plot(SpectrumD1[:,0], SpectrumD1[:,1], label=r'$\varepsilon = 2$',  color='blue')
            axarr[1,0].plot(SpectrumD1_2[:,0], SpectrumD1_2[:,1], label=r'$\varepsilon = 5$', color='red')

            axarr[1,1].plot(SpectrumD2[:,0], SpectrumD2[:,1], label=r'$\varepsilon = 2$', color='blue')
            axarr[1,1].plot(SpectrumD2_2[:,0], SpectrumD2_2[:,1], label=r'$\varepsilon = 5$', color='red')
            
            axarr[1,0].get_xticklabels()[-1].set_visible(False)
            axarr[0,1].set_xlim([2,20])
            
            axarr[1,1].legend()
#
            savename = rootDir + savefolder + 'OpticalSpectrum_vgl_' +Ending+'.eps'
            plt.savefig(savename, format = 'eps')
#           
            plt.close()
#
print('Unpolarized')

SpectrumH1 = np.loadtxt(pathH1 + subfolderOpt + 'OpticalSpectrum_broadened_unpol.dat')
SpectrumH1_2= np.loadtxt(pathH1_2 + subfolderOpt + 'OpticalSpectrum_broadened_unpol.dat')

SpectrumH2 = np.loadtxt(pathH2 + subfolderOpt+'OpticalSpectrum_broadened_'+str(Broadening) + '_unpol.dat')
SpectrumH2_2= np.loadtxt(pathH2_2 + subfolderOpt+'OpticalSpectrum_broadened_'+str(Broadening) + '_unpol.dat')

SpectrumD1 = np.loadtxt(pathD1 + subfolderOpt + 'OpticalSpectrum_broadened_unpol.dat')
SpectrumD1_2= np.loadtxt(pathD1_2 + subfolderOpt + 'OpticalSpectrum_broadened_unpol.dat')

SpectrumD2 = np.loadtxt(pathD2 + subfolderOpt + 'OpticalSpectrum_broadened_'+str(Broadening) + '_unpol.dat')
SpectrumD2_2= np.loadtxt(pathD2_2 +  subfolderOpt + 'OpticalSpectrum_broadened_'+str(Broadening) + '_unpol.dat')

f, axarr = plt.subplots(2, 2, sharex='col', sharey='row', figsize=plt.figaspect(0.5))

        
plt.subplots_adjust(bottom = 0.12, left = 0.05, top = 0.96, right = 0.96)       

plt.subplots_adjust(hspace = .0, wspace = .0)


axarr[1,0].tick_params(labelsize=myFontSizeLabel)
axarr[1,1].tick_params(labelsize=myFontSizeLabel)

axarr[0,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
axarr[1,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')

axarr[0,1].yaxis.set_label_position("right")
axarr[0,1].set_ylabel('Hexagonal ladder', fontsize = myFontSize-2)

axarr[1,1].yaxis.set_label_position("right")
axarr[1,1].set_ylabel('Dirac Model', fontsize = myFontSize-2)


axarr[0,0].text(0.04, 0.95, r'$t = 0.255$', transform=axarr[0,0].transAxes,
      fontsize=myFontSizeLabel,va='top')
axarr[0,1].text(0.04, 0.95, r'$t = 1.7$', transform=axarr[0,1].transAxes,
      fontsize=myFontSizeLabel,va='top')
axarr[1,0].text(0.04, 0.95, r'$t = 0.5$', transform=axarr[1,0].transAxes,
      fontsize=myFontSizeLabel,va='top')
axarr[1,1].text(0.04, 0.95, r'$t = 1.1$', transform=axarr[1,1].transAxes,
      fontsize=myFontSizeLabel,va='top')
      
f.text(0.5, 0.03, 'Energy (eV)', fontsize=myFontSize, ha='center')
f.text(0.03,0.5, 'Absorption (arb. units)', fontsize = myFontSize, ha = 'center', va='center', rotation = 90)
#
################plotting stuff############################     
   # ax2  = axarr[0,0].twinx()   
#ax2.plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = 2$, Full', color = '0.75') 
#ax2.plot(DOSH1_2[:,0], DOSH1_2[:,1], label=r'$\varepsilon = 5$,Full', color='0.5')
axarr[0,0].plot(SpectrumH1[:,0], SpectrumH1[:,1], label=r'$\varepsilon = 2$', color='blue')
axarr[0,0].plot(SpectrumH1_2[:,0], SpectrumH1_2[:,1], label=r'$\varepsilon = 5$', color='red')

#factor = 700/0.0000007
   # axarr[0,0].plot(DOSH1[:,0], DOSH1[:,1]/factor, label=r'$\varepsilon = 2$, Full', color = '0.75')      
   # axarr[0,0].plot(DOSH1_2[:,0], DOSH1_2[:,1]/factor, label=r'$\varepsilon = 5$,Full', color='0.5')

axarr[0,1].plot(SpectrumH2[:,0], SpectrumH2[:,1], label=r'$\varepsilon = 2$',  color='blue')
axarr[0,1].plot(SpectrumH2_2[:,0], SpectrumH2_2[:,1], label=r'$\varepsilon = 5$', color='red')

axarr[1,0].plot(SpectrumD1[:,0], SpectrumD1[:,1], label=r'$\varepsilon = 2$',  color='blue')
axarr[1,0].plot(SpectrumD1_2[:,0], SpectrumD1_2[:,1], label=r'$\varepsilon = 5$', color='red')

axarr[1,1].plot(SpectrumD2[:,0], SpectrumD2[:,1], label=r'$\varepsilon = 2$', color='blue')
axarr[1,1].plot(SpectrumD2_2[:,0], SpectrumD2_2[:,1], label=r'$\varepsilon = 5$', color='red')

axarr[1,0].get_xticklabels()[-1].set_visible(False)
axarr[0,1].set_xlim([2,20])

axarr[1,1].legend()
#
savename = rootDir + savefolder + 'OpticalSpectrum_vgl_unpol.eps'
plt.savefig(savename, format = 'eps')
#           
plt.close()