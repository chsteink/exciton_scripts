# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 08:28:49 2016

@author: csteinke
"""
from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc


rc('text', usetex=True)

myFontSize = 18    
myFontSizeLabel = 14

nnCellsX = 9
nnCellsY = 6


rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folderDirac = 'Data/Excitons/DiracMoS2/'
folderHex = 'Data/Excitons/HexLadder/'
tfolderH1 = '/' #'/t_0.5/' #
tfolderH2 = '/t_inplane_2/'
tfolderD1 = '/t_0.5/'
tfolderD2 = '/' 

savefolder = 'Data/Excitons/'

eps1 = 5
eps2 = 2

subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps1)+'_' + str(eps2)


pathH1 = rootDir + folderHex + subfolder + tfolderH1
pathH2 = rootDir + folderHex + subfolder + tfolderH2
pathD1 = rootDir + folderDirac + subfolder + tfolderD1
pathD2 = rootDir + folderDirac + subfolder + tfolderD2

Broadening = 0.1 #0.005


###-------Vgl. DOS/Joint_DOS
#filename = 'supercell_'+str(nnCellsX)+'x'+str(nnCellsY)+'_'+str(eps1)+'_' + str(eps2)+'_periodicInE2_'
filenameWO = 'exc_ham_'+str(nnCellsX)+'x'+str(nnCellsY)+'_wocoulomb_DOS.dat'
filenameFull = 'exc_ham_'+str(nnCellsX)+'x'+str(nnCellsY)+'_DOS_eigs.dat'

DOSH1 = np.loadtxt(pathH1 + filenameFull)
DOSH1_WO = np.loadtxt(pathH1 + filenameWO)

DOSH2 = np.loadtxt(pathH2 + filenameFull)
DOSH2_WO = np.loadtxt(pathH2 + filenameWO)

DOSD1 = np.loadtxt(pathD1 + filenameFull)
DOSD1_WO = np.loadtxt(pathD1 + filenameWO)

DOSD2 = np.loadtxt(pathD2 + filenameFull)
DOSD2_WO = np.loadtxt(pathD2 + filenameWO)

############global stuff################################
f, axarr = plt.subplots(2, 2, sharex='col', sharey='row', figsize=plt.figaspect(0.5))

plt.subplots_adjust(bottom = 0.12, left = 0.05, top = 0.96, right = 0.96)       

plt.subplots_adjust(hspace = .0, wspace = .0)


axarr[1,0].tick_params(labelsize=myFontSizeLabel)
axarr[1,1].tick_params(labelsize=myFontSizeLabel)

axarr[0,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
axarr[1,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')

axarr[0,1].yaxis.set_label_position("right")
axarr[0,1].set_ylabel('Hexagonal ladder', fontsize = myFontSize-2)

axarr[1,1].yaxis.set_label_position("right")
axarr[1,1].set_ylabel('Dirac Model', fontsize = myFontSize-2)


axarr[0,0].text(0.04, 0.95, r'$t = 0.255$', transform=axarr[0,0].transAxes,
      fontsize=myFontSizeLabel,va='top')
axarr[0,1].text(0.04, 0.95, r'$t = 1.7$', transform=axarr[0,1].transAxes,
      fontsize=myFontSizeLabel,va='top')
axarr[1,0].text(0.04, 0.95, r'$t = 0.5$', transform=axarr[1,0].transAxes,
      fontsize=myFontSizeLabel,va='top')
axarr[1,1].text(0.04, 0.95, r'$t = 1.1$', transform=axarr[1,1].transAxes,
      fontsize=myFontSizeLabel,va='top')

################plotting stuff############################      
axarr[0,0].plot(DOSH1[:,0], DOSH1[:,1], label='Full Coulomb')
axarr[0,0].plot(DOSH1_WO[:,0], DOSH1_WO[:,1], label='Joint DOS')
axarr[0,1].plot(DOSH2[:,0], DOSH2[:,1], label='Full Coulomb')
axarr[0,1].plot(DOSH2_WO[:,0], DOSH2_WO[:,1], label='Joint DOS')
axarr[1,0].plot(DOSD1[:,0], DOSD1[:,1], label='Full Coulomb')
axarr[1,0].plot(DOSD1_WO[:,0], DOSD1_WO[:,1], label='Joint DOS')
axarr[1,1].plot(DOSD2[:,0], DOSD2[:,1], label='Full Coulomb')
axarr[1,1].plot(DOSD2_WO[:,0], DOSD2_WO[:,1], label='Joint DOS')

axarr[1,0].get_xticklabels()[-1].set_visible(False)
axarr[0,1].set_xlim([2,20])

axarr[1,1].legend()

f.text(0.5, 0.03, 'Energy (eV)', fontsize=myFontSize, ha='center')
f.text(0.03,0.5, 'Frequency (arb. units)', fontsize = myFontSize, ha = 'center', va='center', rotation = 90)


savename = rootDir + savefolder + 'vgl_Joint_'+str(eps1)+'_'+str(eps2) + '.eps'
plt.savefig(savename, format = 'eps')