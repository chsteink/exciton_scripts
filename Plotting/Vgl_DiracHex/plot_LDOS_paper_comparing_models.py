# -*- coding: utf-8 -*-
"""
Created on Tue Sep  6 09:09:37 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc
import matplotlib.ticker as ticker
import matplotlib as mpl
import matplotlib.gridspec as gridspec



#Loading Data 
os.system(['clear','cls'][os.name == 'nt'])

#Subfunction to align energy values for graphs along x axis for colormaps where zero is not white

def AppendDataMin(Energy_min, Min_Spectral, SpectralFunc, eps):
    E_min_append = np.arange(Energy_min, Min_Spectral, eps)
    LDOS_append = np.zeros((E_min_append.shape[0],SpectralFunc.shape[1]))
    LDOS_append[:,0] = E_min_append
    SpectralFunc = np.append(LDOS_append, SpectralFunc,  axis = 0)
    
    return SpectralFunc
    
def AppendDataMax(Energy_max, Max_Spectral, SpectralFunc, eps):
    E_max_append = np.arange(Max_Spectral+eps, Energy_max+eps, eps)
    LDOS_append = np.zeros((E_max_append.shape[0],SpectralFunc.shape[1]))
    LDOS_append[:,0] = E_max_append
    SpectralFunc = np.append(SpectralFunc, LDOS_append, axis = 0)

    return SpectralFunc

def AlignData(SpectralFunc1, SpectralFunc2, SpectralFunc3, SpectralFunc4):
        
    Min_Spectral1 = np.min(SpectralFunc1[:,0])
    Min_Spectral2 = np.min(SpectralFunc2[:,0])
    Min_Spectral3 = np.min(SpectralFunc3[:,0])
    Min_Spectral4 =np.min(SpectralFunc4[:,0])
    
    Max_Spectral1 = np.max(SpectralFunc1[:,0])
    Max_Spectral2 = np.max(SpectralFunc2[:,0])
    Max_Spectral3 = np.max(SpectralFunc3[:,0])
    Max_Spectral4 = np.max(SpectralFunc4[:,0])
    
    Energy_min = np.min([Min_Spectral1,Min_Spectral2, Min_Spectral3, Min_Spectral4])
    Energy_max = np.max([Max_Spectral1,Max_Spectral2,Max_Spectral3,Max_Spectral4])
    
    eps = SpectralFunc1[1,0] - SpectralFunc1[0,0]

    if Min_Spectral1 > Energy_min:
        SpectralFunc1 = AppendDataMin(Energy_min, Min_Spectral1, SpectralFunc1, eps)
    #
    if Min_Spectral2 > Energy_min:
        SpectralFunc2 = AppendDataMin(Energy_min, Min_Spectral2, SpectralFunc2, eps)
        
    if Min_Spectral3 > Energy_min:
        SpectralFunc3= AppendDataMin(Energy_min, Min_Spectral3, SpectralFunc3, eps)
        
    if Min_Spectral4 > Energy_min:
        SpectralFunc4 = AppendDataMin(Energy_min, Min_Spectral4, SpectralFunc4, eps)
    #    
    if Max_Spectral1 < Energy_max:
        SpectralFunc1 = AppendDataMax(Energy_max, Max_Spectral1, SpectralFunc1, eps)
        
    if Max_Spectral2 < Energy_max:
        SpectralFunc2 = AppendDataMax(Energy_max, Max_Spectral2, SpectralFunc2, eps)
        
    if Max_Spectral3 < Energy_max:
        SpectralFunc3 = AppendDataMax(Energy_max, Max_Spectral3, SpectralFunc3, eps)
        
    if Max_Spectral4 < Energy_max:
        SpectralFunc4 = AppendDataMax(Energy_max, Max_Spectral4, SpectralFunc4, eps)
        
    return SpectralFunc1, SpectralFunc2, SpectralFunc3, SpectralFunc4, Energy_min, Energy_max


#Figure properties
myFontSize = 20
myFontSizeLabel = myFontSize - 2

#Paper Hexlagger t normal:
cblimH1 = None#65
cblim_woH1 = None #12

cblimH2 = None #1.2
cblim_woH2 = None #0.8

cblimD1 = 4.5
cblim_woD1 = None #1

cblimD2 = None #3
cblim_woD2 = None #1


colormap = 'coolwarm'

#  # Plot with SANS-SERIF FONT.

rc('text', usetex='false') 
rc('font', family='sans-serif') 



plt.ioff()


#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folderDirac = 'Data/Excitons/DiracMoS2/'
folderHex = 'Data/Excitons/HexLadder/'
tfolderH1 = '/' #'/t_0.5/' #
tfolderH2 = '/t_inplane_2/'
tfolderD1 = '/t_0.5/'
tfolderD2 = '/' 


nnCellsXX = 9
nnCellsYY = 6

eps1 = 2
eps2 = 2


subfolder = str(nnCellsXX)+'x'+str(nnCellsYY)+ '/' + str(eps1) + '_' + str(eps2) 


pathH1 = rootDir + folderHex + subfolder + tfolderH1
pathH2 = rootDir + folderHex + subfolder + tfolderH2
pathD1 = rootDir + folderDirac + subfolder + tfolderD1
pathD2 = rootDir + folderDirac + subfolder + tfolderD2
    
SpectralFolder = 'SpectralFunc/DirectCoordinates/de_0.1_broad_0.01/'
SpectralFolderH2 = 'SpectralFunc/DirectCoordinates/de_0.2_broad_0.1/'


whole_pathH1 = rootDir + folderHex + subfolder + tfolderH1 + SpectralFolder
whole_pathH2 = rootDir + folderHex + subfolder + tfolderH2 + SpectralFolderH2
whole_pathD1 = rootDir + folderDirac + subfolder + tfolderD1 + SpectralFolder
whole_pathD2 = rootDir + folderDirac + subfolder + tfolderD2 + SpectralFolderH2



f, axarr = plt.subplots(2, 4, sharex='col', sharey='row', figsize=2*0.7*plt.figaspect(0.5))

plt.subplots_adjust(hspace = 0.05, wspace = 0.05)   
plt.subplots_adjust(bottom = 0.09, left = 0.045, top = 0.965, right = 0.955)


relative = 1

#electron_number = 3
   
     
for electron_number in range(0,2):         
        
    filename = 'spectral_function_elec' + str(electron_number) + '.dat'
    filename_wo = 'spectral_function_woCoul_elec' + str(electron_number) + '.dat'

    SpectralFuncH1 = np.loadtxt(whole_pathH1 + filename )
    SpectralFunc_woH1 = np.loadtxt(whole_pathH1 + filename_wo)
    
    SpectralFuncH2 = np.loadtxt(whole_pathH2 + filename)
    SpectralFunc_woH2 = np.loadtxt(whole_pathH2 + filename_wo)
    
    SpectralFuncD1 = np.loadtxt(whole_pathD1 + filename)
    SpectralFunc_woD1 = np.loadtxt(whole_pathD1 + filename_wo)
    
    SpectralFuncD2 = np.loadtxt(whole_pathD2 + filename)
    SpectralFunc_woD2= np.loadtxt(whole_pathD2 + filename_wo)
    
    HolePosHex = np.loadtxt(whole_pathH1 + 'SpectralFunc_HolePos.dat')
    dim = HolePosHex.shape[1]-1
    HolePosHex = HolePosHex[:,0:dim]
    ElecPosHex = np.loadtxt(whole_pathH1 + 'SpectralFunc_ElecPos.dat')[:,0:dim]
    
    HolePosDirac= np.loadtxt(whole_pathD1 + 'SpectralFunc_HolePos.dat')
    dim = HolePosHex.shape[1]-1
    HolePosDirac = HolePosDirac[:,0:dim]  
    ElecPosDirac= np.loadtxt(whole_pathD1 + 'SpectralFunc_ElecPos.dat')[:,0:dim]
        
    
    CurrentElecHex = ElecPosHex[electron_number,:]
    CurrentElecDirac = ElecPosDirac[electron_number,:]
    
    if relative == 1:
        RelativePosHex = HolePosHex - CurrentElecHex
        NormRelativeHex = np.linalg.norm(RelativePosHex, axis = 1)
        sortind = np.argsort(NormRelativeHex)
        NormRelativeHex = NormRelativeHex[sortind]
        sort_array = np.append(0, sortind + 1)
        
        SpectralFuncH1 = SpectralFuncH1[:,sort_array]
        SpectralFunc_woH1 = SpectralFunc_woH1[:, sort_array]
        
        SpectralFuncH2 = SpectralFuncH2[:,sort_array]
        SpectralFunc_woH2 = SpectralFunc_woH2[:, sort_array]
        
        RelativePosDirac = HolePosDirac - CurrentElecDirac
        NormRelativeDirac = np.linalg.norm(RelativePosDirac, axis = 1)
        sortind = np.argsort(NormRelativeDirac)
        NormRelativeDirac = NormRelativeDirac[sortind]
        sort_array = np.append(0, sortind + 1)
        
        SpectralFuncD1 = SpectralFuncD1[:,sort_array]
        SpectralFunc_woD1 = SpectralFunc_woD1[:, sort_array]
        
        SpectralFuncD2 = SpectralFuncD2[:,sort_array]
        SpectralFunc_woD2 = SpectralFunc_woD2[:, sort_array]
#
#    
#
#    #rc('axes', labelweight='100')
#    
#    #Creating same energy range for both data:
    
    SpectralFuncH1, SpectralFunc_woH1, SpectralFuncD1,SpectralFunc_woD1, Energy_min_up, Energy_max_up = AlignData(SpectralFuncH1, 
                                                                                    SpectralFunc_woH1, 
                                                                                    SpectralFuncD1,
                                                                                    SpectralFunc_woD1)

    SpectralFuncH2, SpectralFunc_woH2, SpectralFuncD2,SpectralFunc_woD2,Energy_min_low, Energy_max_low  = AlignData(SpectralFuncH2, 
                                                                                    SpectralFunc_woH2, 
                                                                                    SpectralFuncD2,
                                                                                    SpectralFunc_woD2)


#    
#
    if relative == 1:
       xHex = NormRelativeHex[0::4]
       xHex = np.append(xHex,xHex[-1]+xHex[1])
      
       
       xDirac = NormRelativeDirac[0::4]
       xDirac = np.append(xDirac, xDirac[-1]+xDirac[1])
     
       #x = np.arange(0,19,2)
       upperLeft = axarr[0,0].pcolor(xHex, SpectralFuncH1[:,0], SpectralFuncH1[:,1::4], cmap = plt.get_cmap(colormap), vmax = cblimH1, linewidth = 0.,rasterized=True)
       upperLeft_wo = axarr[0,1].pcolor(xHex, SpectralFunc_woH1[:,0], SpectralFunc_woH1[:,1::4],cmap = plt.get_cmap(colormap),vmax = cblim_woH1, linewidth = 0.,  rasterized=True)
       
       upperRight = axarr[0,2].pcolor(xDirac, SpectralFuncD1[:,0], SpectralFuncD1[:,1::4], cmap = plt.get_cmap(colormap), vmax = cblimD1, linewidth = 0.,rasterized=True)
       upperRight_wo = axarr[0,3].pcolor(xDirac, SpectralFunc_woD1[:,0], SpectralFunc_woD1[:,1::4],cmap = plt.get_cmap(colormap),vmax = cblim_woD1, linewidth = 0.,  rasterized=True)
       
       lowerLeft = axarr[1,0].pcolor(xHex, SpectralFuncH2[:,0], SpectralFuncH2[:,1::4], cmap = plt.get_cmap(colormap), vmax = cblimH2, linewidth = 0.,rasterized=True)
       lowerLeft_wo = axarr[1,1].pcolor(xHex, SpectralFunc_woH2[:,0], SpectralFunc_woH2[:,1::4],cmap = plt.get_cmap(colormap),vmax = cblim_woH2, linewidth = 0.,  rasterized=True)
       
       lowerRight= axarr[1,2].pcolor(xDirac, SpectralFuncD2[:,0], SpectralFuncD2[:,1::4], cmap = plt.get_cmap(colormap), vmax = cblimD2, linewidth = 0.,rasterized=True)
       lowerRight_wo= axarr[1,3].pcolor(xDirac, SpectralFunc_woD2[:,0], SpectralFunc_woD2[:,1::4],cmap = plt.get_cmap(colormap),vmax = cblim_woD2, linewidth = 0.,  rasterized=True)
       
       #axarr[1].set_xticks(x + 1)
       #axarr[0].set_xticks(x + 1)
       #axarr[0].set_xticklabels(np.round(NormRelative[0::2],2),fontsize = myFontSizeLabel)
       #axarr[1].set_xticklabels(np.round(NormRelative[0::2],2),fontsize = myFontSizeLabel)


       f.text(0.01, 0.53, r'Energy (eV)', fontsize = myFontSize, transform = f.transFigure, ha = 'center', va = 'center', rotation = 90)
       f.text(0.5, 0.01, r'Electron-hole distance ($\AA$)', fontsize=myFontSize, transform = f.transFigure, ha='center')
       f.text(0.28,0.975, r'Hexagonal Ladder', fontsize = myFontSize , fontweight = 'bold', transform = f.transFigure, ha='center')
       f.text(0.73, 0.975, r'Dirac Model', fontsize = myFontSize , fontweight = 'bold', transform = f.transFigure, ha='center')
#   
    for axisNumberCol in range(0,4):
        for axisNumberRow in range(0,2):
            
            axarr[axisNumberRow, axisNumberCol].tick_params(axis='both', labelsize = myFontSizeLabel)
            
            if axisNumberCol == 0 or axisNumberCol == 1:
                if axisNumberRow == 0: 
                    axarr[axisNumberRow,axisNumberCol].axis([xHex.min(), xHex.max(), Energy_min_up, Energy_max_up]) 
                    
                else:
                    axarr[axisNumberRow,axisNumberCol].axis([xHex.min(), xHex.max(), Energy_min_low, Energy_max_low]) 
            
            else:
                if axisNumberRow == 0:
                    axarr[axisNumberRow,axisNumberCol].axis([xDirac.min(), xDirac.max(), Energy_min_up, Energy_max_up])   
                else:
                    axarr[axisNumberRow,axisNumberCol].axis([xDirac.min(), xDirac.max(), Energy_min_low, Energy_max_low])   

#   
#   #vertical colorbar
    cbar1_ax = f.add_axes([0.96,0.09, 0.015, 0.875])
    cbar1 = f.colorbar(upperLeft, cax = cbar1_ax, orientation = 'vertical')
    cbar1.ax.tick_params(labelsize=myFontSizeLabel, labelright = 'off')
    cbar1.set_label('Spectral Function (arb. units)', fontsize=myFontSize-2)
    cbar1.solids.set_rasterized(True)     
    
    for oo in range(0,4):
        
        if oo == 0 or oo == 2:
            axarr[0,oo].text(0.95,0.02, 'Correlated', va = 'bottom', ha = 'right', transform = axarr[0,oo].transAxes, color = 'white', fontsize = myFontSizeLabel)
            axarr[1,oo].text(0.95,0.97, 'Correlated', va = 'top', ha='right', transform =axarr[1,oo].transAxes, color = 'white', fontsize = myFontSizeLabel )
        else:   
            axarr[0,oo].text(0.95,0.02, 'Quasi-particle limit', va = 'bottom', ha = 'right', transform = axarr[0,oo].transAxes, color = 'white', fontsize = myFontSizeLabel)
            axarr[1,oo].text(0.95,0.97, 'Quasi-particle limit', va = 'top', ha = 'right', transform = axarr[1,oo].transAxes, color = 'white', fontsize = myFontSizeLabel)
    
    axarr[0,0].text(0.02, 0.9,r'a) Localized', va = 'bottom', ha = 'left', transform = axarr[0,0].transAxes, color = 'white', fontsize = myFontSizeLabel)
    axarr[1,0].text(0.02, 0.9,r'b) Delocalized', va = 'bottom', ha = 'left', transform = axarr[1,0].transAxes, color = 'white', fontsize = myFontSizeLabel)
    axarr[0,2].text(0.02, 0.9,r'c) Localized', va = 'bottom', ha = 'left', transform = axarr[0,2].transAxes, color = 'white', fontsize = myFontSizeLabel)
    axarr[1,2].text(0.02, 0.9,r'd) Delocalized', va = 'bottom', ha = 'left', transform = axarr[1,2].transAxes, color = 'white', fontsize = myFontSizeLabel)
#
    
#    axarr[0].yaxis.set_major_locator(ticker.MultipleLocator(1))
#    axarr[0].set_ylabel('Energy (eV)', fontsize = myFontSize)
    


    axarr[1,0].get_xticklabels()[-2].set_visible(False)
    axarr[1,1].get_xticklabels()[-2].set_visible(False)
     
#    
#    #cbar2_ax = f.add_axes([0.52, 0.9, 0.44, 0.03])
#    #cbar2 = f.colorbar(b, cax = cbar2_ax, orientation = 'horizontal', ticklocation = 'top')
#    #cbar2.ax.tick_params(labelsize=myFontSizeLabel, labeltop = 'off')
#    #cbar2.set_label('Spectral Function (arb. units)', fontsize=myFontSize-2)
#    

    # axarr[0].text(0.95,0.02, 'a) Correlated', va = 'bottom', ha = 'right', transform = axarr[0].transAxes, color = 'white', fontsize = myFontSizeLabel)
    # axarr[1].text(0.95,0.02, 'b) Uncorrelated', va = 'bottom', ha = 'right', transform = axarr[1].transAxes, color = 'white', fontsize = myFontSizeLabel)
#
#    
#    
    if relative == 1:
        savefolder = 'Data/Excitons/'
        
        f.savefig(rootDir + savefolder + 'spectralfunc_map_relative' + str(electron_number) + '.pdf', format='pdf')
#    else:
#        f.savefig(whole_path + 'spectralfunc_map' + str(electron_number) + '_' + colormap + '.eps', format='eps')
#        
    plt.close()
#    

     
        