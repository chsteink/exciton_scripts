# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 08:28:49 2016

@author: csteinke
"""
from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc


rc('text', usetex='false') 
rc('font', family='sans-serif')


myFontSize = 20   
myFontSizeLabel = myFontSize - 2
linewidthPlot = 2

nnCellsX = 9
nnCellsY = 6


rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folderDirac = 'Data/Excitons/DiracMoS2/'
folderHex = 'Data/Excitons/HexLadder/'
tfolderH1 = '/' #'/t_0.5/' #
tfolderH2 = '/t_inplane_2/'
tfolderD1 = '/t_0.5/'
tfolderD2 = '/' 

savefolder = 'Data/Excitons/'

eps_vec = np.array([2,5,1000000])

f, axarr = plt.subplots(2, 2, sharex='col', sharey='row', figsize=0.7*plt.figaspect(0.5))

plt.subplots_adjust(hspace = .0, wspace = .0)   
plt.subplots_adjust(bottom = 0.16, left = 0.05, top = 0.98, right = 0.96)
   
for i in range(0,int(eps_vec.shape[0])):
        
    eps1 = eps_vec[i]
    eps2 = eps_vec[i]
    
    subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps1)+'_' + str(eps2)
    
    
    pathH1 = rootDir + folderHex + subfolder + tfolderH1
    pathH2 = rootDir + folderHex + subfolder + tfolderH2
    pathD1 = rootDir + folderDirac + subfolder + tfolderD1
    pathD2 = rootDir + folderDirac + subfolder + tfolderD2

    ###-------Vgl. DOS/Joint_DOS
    #filename = 'supercell_'+str(nnCellsX)+'x'+str(nnCellsY)+'_'+str(eps1)+'_' + str(eps2)+'_periodicInE2_'
    #filenameFull = 'exc_ham_'+str(nnCellsX)+'x'+str(nnCellsY)+'_DOS_eigs.dat'
    filenameFull =  'exc_ham_'+str(nnCellsX)+'x'+str(nnCellsY)+'_eigenvals_eigs.dat'
    
    
    EigenvalsH1 = np.loadtxt(pathH1 + filenameFull)
    EigenvalsH2= np.loadtxt(pathH2 + filenameFull)
    EigenvalsD1= np.loadtxt(pathD1 + filenameFull)
    EigenvalsD2= np.loadtxt(pathD2 + filenameFull)
    
    
    
    
    
    ##################Calculating DOS#########################
    
    
    DeltaE = 0.1  
    Energy_min = np.min(EigenvalsH1) - DeltaE
    Energy_max = np.max(EigenvalsH1) + DeltaE
    
    numberOfBins = int((Energy_max - Energy_min)/DeltaE)
        
        
    DOSH1 = np.zeros((numberOfBins,2))

    
    [DOSH1[:,1], binEdges] = np.histogram(EigenvalsH1, bins=numberOfBins, range=(Energy_min, Energy_max))
    DOSH1[:,0] =  0.5*(binEdges[1:]+binEdges[:-1])
    
    DeltaE = 0.6
    Energy_min = np.min(EigenvalsH2) - 2*DeltaE
    Energy_max = np.max(EigenvalsH2) + 2*DeltaE
    numberOfBins = int((Energy_max - Energy_min)/DeltaE)
    
    DOSH2 = np.zeros((numberOfBins,2))

    
    [DOSH2[:,1], binEdges] = np.histogram(EigenvalsH2, bins=numberOfBins, range=(Energy_min, Energy_max))
    DOSH2[:,0] =  0.5*(binEdges[1:]+binEdges[:-1])
    
    DeltaE = 0.1
    Energy_min = np.min(EigenvalsD1) - DeltaE
    Energy_max = np.max(EigenvalsD1) + DeltaE
    numberOfBins = int((Energy_max - Energy_min)/DeltaE)
    DOSD1  = np.zeros((numberOfBins,2)) 

    [DOSD1[:,1], binEdges] = np.histogram(EigenvalsD1, bins=numberOfBins, range=(Energy_min, Energy_max))
    DOSD1[:,0] =  0.5*(binEdges[1:]+binEdges[:-1])
    
    DeltaE = 0.5
    Energy_min = np.min(EigenvalsD2) -  DeltaE
    Energy_max = np.max(EigenvalsD2) +  DeltaE
    numberOfBins = int((Energy_max - Energy_min)/DeltaE)
    DOSD2 = np.zeros((numberOfBins,2))
    [DOSD2[:,1], binEdges] = np.histogram(EigenvalsD2, bins=numberOfBins, range=(Energy_min, Energy_max))
    DOSD2[:,0] =  0.5*(binEdges[1:]+binEdges[:-1])
    
    ############global stuff################################

    
           
    
    
    
    
    axarr[1,0].tick_params(labelsize=myFontSizeLabel)
    axarr[1,1].tick_params(labelsize=myFontSizeLabel)
    
    axarr[0,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
    axarr[1,0].tick_params(labelsize=myFontSizeLabel,labelleft='off')
    

    
    

    
    ################plotting stuff############################  
    
    if i == 0:
        axarr[0,0].plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = 2$', linewidth = linewidthPlot, color = 'blue')
    
        axarr[0,1].plot(DOSH2[:,0], DOSH2[:,1], label=r'$\varepsilon = 2$', linewidth = linewidthPlot, color = 'blue')
    
        axarr[1,0].plot(DOSD1[:,0], DOSD1[:,1], label=r'$\varepsilon = 2$', linewidth = linewidthPlot, color = 'blue')
    
        axarr[1,1].plot(DOSD2[:,0], DOSD2[:,1], label=r'$\varepsilon = 2$',linewidth = linewidthPlot, color = 'blue')
    

    elif i == 1:
        axarr[0,0].plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = 5$', linewidth = linewidthPlot, color = 'red')
    
        axarr[0,1].plot(DOSH2[:,0], DOSH2[:,1], label=r'$\varepsilon = 5$', linewidth = linewidthPlot, color = 'red')
    
        axarr[1,0].plot(DOSD1[:,0], DOSD1[:,1], label=r'$\varepsilon = 5$', linewidth = linewidthPlot, color = 'red')
    
        axarr[1,1].plot(DOSD2[:,0], DOSD2[:,1], label=r'$\varepsilon = 5$', linewidth = linewidthPlot, color = 'red')
        
        
    elif i == 2:
        axarr[0,0].plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = \infty$', linewidth = linewidthPlot, color = 'black')
    
        axarr[0,1].plot(DOSH2[:,0], DOSH2[:,1], label=r'$\varepsilon = \infty$',linewidth = linewidthPlot,  color = 'black')
    
        axarr[1,0].plot(DOSD1[:,0], DOSD1[:,1], label=r'$\varepsilon = \infty$', linewidth = linewidthPlot, color = 'black')
    
        axarr[1,1].plot(DOSD2[:,0], DOSD2[:,1], label=r'$\varepsilon = \infty$', linewidth = linewidthPlot, color = 'black')

    
    axarr[1,0].get_xticklabels()[-1].set_visible(False)
    axarr[0,1].set_xlim([0,20])
    axarr[0,1].set_xticks(np.arange(0,20+1,2))    
    
    axarr[0,0].set_ylim([0,3500])
    #axarr[0,1].set_ylim([0,1200])
    axarr[1,0].set_ylim([0,3000])
    
    axarr[1,1].legend()
    

f.text(0.5, 0.025, 'Energy (eV)', fontsize=myFontSize, transform = f.transFigure, ha='center')
f.text(0.03,0.56, 'Frequency (arb. units)', fontsize = myFontSize, ha = 'center', va='center', rotation = 90)

axarr[0,0].text(0.04, 0.95, r'$t = 0.255\,\mathrm{eV}$', transform=axarr[0,0].transAxes,
          fontsize=myFontSizeLabel,va='top')
axarr[0,1].text(0.04, 0.95, r'$t = 1.7\,\mathrm{eV}$', transform=axarr[0,1].transAxes,
          fontsize=myFontSizeLabel,va='top')
axarr[1,0].text(0.04, 0.95, r'$t = 0.5\,\mathrm{eV}$', transform=axarr[1,0].transAxes,
          fontsize=myFontSizeLabel,va='top')
axarr[1,1].text(0.04, 0.95, r'$t = 1.1\,\mathrm{eV}$', transform=axarr[1,1].transAxes,
          fontsize=myFontSizeLabel,va='top')

axarr[0,1].yaxis.set_label_position("right")
axarr[0,1].set_ylabel('Hexagonal ladder', fontsize = myFontSize-4)
    
axarr[1,1].yaxis.set_label_position("right")
axarr[1,1].set_ylabel('Dirac Model', fontsize = myFontSize-4)

savename = rootDir + savefolder + 'vgl_homogen_DOS_all.eps'
plt.savefig(savename, format = 'eps')


