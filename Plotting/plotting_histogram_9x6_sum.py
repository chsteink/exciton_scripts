# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""
from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])


def getDOS(Eig_eps1, Eig_eps2):
    
    Maxeps1 = np.max(Eig_eps1)
    Maxeps2 = np.max(Eig_eps2)

    Mineps1 = np.min(Eig_eps1)
    Mineps2 = np.min(Eig_eps2)

    DOS_range = (min(Mineps1,Mineps2), max(Maxeps1, Maxeps2))

    [DOS_eps1,binEdges] = np.histogram(Eig_eps1,bins=200, range=DOS_range)
    bincenters = 0.5*(binEdges[1:]+binEdges[:-1])
    DOS_eps2, binEdges = np.histogram(Eig_eps2, bins = 200, range = DOS_range)

    DOS = 1./2 * (DOS_eps1 + DOS_eps2)
    
    DOS_save = np.array([bincenters, DOS]).T
    return DOS_save
    
#Plotting of histograms excitonic spectrum with added homogeneosu curves
myFontSize = 20


#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/9x6/'
filename_DOS = 'exc_ham_9x6_DOS_eigs.dat'
filename = 'exc_ham_9x6_eigenvals_eigs.dat'

savename = 'Excitonspectrum_9x6_'
#subfolder = 'Dielectric/5x5/'

numberofbins = 200

#Loading file 1: eps2/eps1 = 1
subfolder = '5_1/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_51 = np.loadtxt(input_file)

#File 2: eps2/eps1 = 1.5
subfolder = '5_2/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_52 = np.loadtxt(input_file)

#File 3: eps2/eps1 = 2
subfolder = '5_4/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_54 = np.loadtxt(input_file)

#File4: eps2/eps1 = 2.5
subfolder = '5_6/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_56 = np.loadtxt(input_file)

#File5: eps2/eps1 = 3
subfolder='5_10/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_510 = np.loadtxt(input_file)

subfolder = '5_15/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_515 = np.loadtxt(input_file)

subfolder = '6_15/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_615 = np.loadtxt(input_file)

subfolder = '7.5_15/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_715 = np.loadtxt(input_file)

subfolder = '10_15/'
input_file = rootDir + folder + subfolder +filename_DOS
DOS_1015 = np.loadtxt(input_file)

#Loading homogeneous data
subfolder = '5_5/'
input_file = rootDir + folder + subfolder +filename
Eig_5hom = np.loadtxt(input_file)

subfolder = '6_6/'
input_file = rootDir + folder + subfolder +filename
Eig_6hom = np.loadtxt(input_file)

subfolder = '4_4/'
input_file = rootDir + folder + subfolder +filename
Eig_4hom = np.loadtxt(input_file)

subfolder = '2_2/'
input_file = rootDir + folder + subfolder +filename
Eig_2hom = np.loadtxt(input_file)

subfolder = '1_1/'
input_file = rootDir + folder + subfolder +filename
Eig_1hom = np.loadtxt(input_file)

subfolder = '10_10/'
input_file = rootDir + folder + subfolder +filename
Eig_10hom = np.loadtxt(input_file)

subfolder = '6_6/'
input_file = rootDir + folder + subfolder +filename
Eig_6hom = np.loadtxt(input_file)

subfolder = '7.5_7.5/'
input_file = rootDir + folder + subfolder +filename
Eig_7hom = np.loadtxt(input_file)

subfolder = '15_15/'
input_file = rootDir + folder + subfolder +filename
Eig_15hom = np.loadtxt(input_file)

#Calculating DOS in given area (for homogeneous data: must be same) and add both homogeneous 
DOS_51hom = getDOS(Eig_5hom, Eig_1hom)
DOS_52hom = getDOS(Eig_5hom, Eig_2hom)
DOS_54hom = getDOS(Eig_5hom, Eig_4hom)
DOS_56hom = getDOS(Eig_5hom, Eig_6hom)
DOS_510hom = getDOS(Eig_5hom, Eig_10hom)
DOS_515hom = getDOS(Eig_5hom, Eig_15hom)
DOS_615hom = getDOS(Eig_6hom, Eig_15hom)
DOS_715hom = getDOS(Eig_7hom, Eig_15hom)
DOS_1015hom = getDOS(Eig_10hom, Eig_15hom)




plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/1)', fontsize=myFontSize)
ax.set_yticklabels([])


plt.plot(DOS_51hom[:,0],DOS_51hom[:,1], label = r'Added homogeneous ', color ='black', linewidth = 1.0)
plt.plot(DOS_51[:,0], DOS_51[:,1]  , label=r'$(varepsilon_1=5, \varepsilon_2 = 1$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_1_add.eps', format='eps')
#
plt.figure(2)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='400')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.3,6.8])
plt.ylim([0,1.05])
plt.xticks(np.arange(1.5,7, 0.5))

plt.plot(DOS_52hom[:,0],DOS_52hom[:,1]/np.max(DOS_52hom[:,1]), label = r'$(\varepsilon = 5)$ + $(\varepsilon = 2)$', color ='black', linewidth = 1.0)
plt.plot(DOS_52[:,0], DOS_52[:,1]/np.max(DOS_52[:,1]) , label=r'Dielectric ($\varepsilon_1 / \varepsilon_2 = 5 / 2$)', color='red', linewidth=1.0)

plt.legend(loc='upper right',fontsize=myFontSize-2)
plt.subplots_adjust(bottom = 0.12, left = 0.065, top = 0.93, right = 0.97)

plt.savefig(rootDir + folder + savename + '5_2_add_vortrag.eps', format='eps')

plt.figure(3)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/4)', fontsize=myFontSize)
ax.set_yticklabels([])


plt.plot(DOS_54hom[:,0],DOS_54hom[:,1], label = r'Added homogeneous', color ='black', linewidth = 1.0)

plt.plot(DOS_54[:,0], DOS_54[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 4$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_4_add.eps', format='eps')

plt.figure(4)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/6)', fontsize=myFontSize)
ax.set_yticklabels([])


plt.plot(DOS_56hom[:,0],DOS_56hom[:,1], label = r'Added homogeneous', color ='black', linewidth = 1.0)

plt.plot(DOS_56[:,0], DOS_56[:,1] ,label=r'$\varepsilon_1=5, \varepsilon_2 = 6$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_6_add.eps', format='eps')

plt.figure(5)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/10)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_510hom[:,0],DOS_510hom[:,1], label = r'Added homogeneous ', color ='black', linewidth = 1.0)

plt.plot(DOS_510[:,0], DOS_510[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 10$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_10_add.eps', format='eps')

plt.figure(6)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/15)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_515hom[:,0],DOS_515hom[:,1], label = r'Added homogeneous', color ='black', linewidth = 1.0)

plt.plot(DOS_515[:,0], DOS_515[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_15_add.eps', format='eps')

plt.figure(7)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (6/15)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_615hom[:,0],DOS_615hom[:,1], label = r'Added homogeneous ', color ='black', linewidth = 1.0)

plt.plot(DOS_615[:,0], DOS_615[:,1] , label=r'$\varepsilon_1=6, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '6_15_add.eps', format='eps')

plt.figure(8)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (7.5/15)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_715hom[:,0],DOS_715hom[:,1], label = r'Added homogeneous', color ='black', linewidth = 1.0)

plt.plot(DOS_715[:,0], DOS_715[:,1] , label=r'$\varepsilon_1=7.5 \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '7_15_add.eps', format='eps')

plt.figure(9)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (10/15)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_1015hom[:,0],DOS_1015hom[:,1], label = r'Added homogeneous ', color ='black', linewidth = 1.0)

plt.plot(DOS_1015[:,0], DOS_1015[:,1] , label=r'$\varepsilon_1=10, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '10_15_add.eps', format='eps')



plt.close('all')

