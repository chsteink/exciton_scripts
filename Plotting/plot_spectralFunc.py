#!/usr/bin/env python

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.mlab as ml
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc

#Loading Data 
os.system(['clear','cls'][os.name == 'nt'])


#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'

nnCellsXX = 9
nnCellsYY = 6

eps1 = 2
eps2 = 2

tfolder = '/'

numberElectrons = 6

plot_mode = 1 #Contour = 1, Discrete = 2


#subfolder = str(nnCellsXX)+'x'+str(nnCellsYY)+ '/' + str(eps1) + '_' + str(eps2) + tfolder + 'SpectralFunc/DirectCoordinates/de_0.01_broad_0.005/'
subfolder = str(nnCellsXX)+'x'+str(nnCellsYY)+ '/' + str(eps1) + '_' + str(eps2) + tfolder + 'SpectralFunc/DirectCoordinates/'

for electron_number in range(0,numberElectrons):
    
    filename = 'spectral_function_elec' + str(electron_number) + '.dat'
    whole_path = rootDir + folder + subfolder
    input_file =  whole_path + filename  
    
    
    SpectralFunc = np.loadtxt(input_file)
    HolePos = np.loadtxt(whole_path + 'SpectralFunc_HolePos.dat')
    HolePos = HolePos[:,0:3]
    ElecPos = np.loadtxt(whole_path + 'SpectralFunc_ElecPos.dat')[:,0:3]
    
    CurrentElec = ElecPos[electron_number,:]
    
    plotSize = (np.size(SpectralFunc[0,:])-1) * np.size(SpectralFunc[:,0]);
    myFontSize = 20
    
#    Relative = HolePos - CurrentElec
#    NormRelative = np.linalg.norm(Relative, axis = 1)
#    sortind = np.argsort(NormRelative)
#    
#    NormRelative_sort = NormRelative[sortind]
#    sort_array = np.append(0, sortind + 1)
#    SpectralFunc = SpectralFunc[:,sort_array]
#    
    if plot_mode == 1:  
        plotX = np.empty(plotSize, dtype=np.float)
        plotY = np.empty(plotSize, dtype=np.float)
        plotZ = np.empty(plotSize, dtype=np.float)
    
        pp = 0
        for ii in range(0, np.size(SpectralFunc[:,0])):
            for xx in range(1, np.size(SpectralFunc[0,:])):
                plotX[pp] = xx 
                plotY[pp] = SpectralFunc[ii,0]
                plotZ[pp] = SpectralFunc[ii,xx]
                pp = pp + 1
    		
    		
        xi = np.linspace(min(plotX), max(plotX), 50)
        yi = np.linspace(min(plotY), max(plotY), 100)
        zi = ml.griddata(plotX, plotY, plotZ, xi, yi)
    
        plt.figure()
        rc('text', usetex=True)	
        rc('axes', labelweight='100')
    
    	
        plt.contourf(xi, yi, zi, 50, linewidths = 1.0, cmap = plt.get_cmap('coolwarm'))
    
        cb = plt.colorbar() 
        cb.ax.tick_params(labelsize=myFontSize)
        cb.set_label('Spectral function (arb. units)', fontsize=myFontSize)
        plt.title('Electron at' + str(CurrentElec))

        plt.xticks(np.arange(18)+1, (np.ceil(np.arange(1,18+1)/2.)))    
    
        plt.xlabel('unit cell', fontsize=myFontSize)
    #plt.xlabel('rh-re')
        plt.ylabel('Energy (eV)', fontsize=myFontSize)
    #plt.gcf().subplots_adjust(bottom=0.13)
    #plt.gcf().subplots_adjust(top = 0.99)
    #plt.gcf().subplots_adjust(right = 0.99)
    
    #plt.tick_params(labelsize=myFontSize)
    
    #plt.xticks(np.arange(18)+1,NormRelative_sort)

    
        plt.show()
    
        plt.savefig(whole_path + 'spectralfunc_elec' + str(electron_number) + '.eps', format='eps')
        
    if plot_mode == 2:
        
        plt.figure()
        rc('text', usetex=True)	
        rc('axes', labelweight='100')
        
        x = np.arange(0,19)
        eps = SpectralFunc[1,0] - SpectralFunc[0,0]
        y = np.arange(SpectralFunc[0,0], SpectralFunc[-1,0]+eps, eps)
        plt.pcolor(x, y, SpectralFunc[:,1:], cmap = plt.get_cmap('coolwarm'))
        cb = plt.colorbar() 
        cb.ax.tick_params(labelsize=myFontSize)
        cb.set_label('Spectral function (arb. units)', fontsize=myFontSize)
        
        plt.title('Electron at' + str(CurrentElec))
        plt.xticks(x + 0.5, x+1)
        plt.xlim([0,18])
        #plt.yticks(np.arange(1.6,6.6,0.4))
        plt.ylim([y[0], y[-1]-eps])
        
        plt.xlabel('hole number', fontsize =myFontSize)
        plt.ylabel('Energy (eV)', fontsize=myFontSize)
        
        plt.savefig(whole_path + 'spectralfunc_disc_elec' + str(electron_number) + '.eps', format='eps')

plt.close('all')