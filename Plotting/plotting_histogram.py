# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 18


#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'
save_folder = 'Excitons/Plots/7x7/'
#subfolder = 'Dielectric/5x5/'

numberofbins = 200

#Loading file 1: Heterogen
filename = 'supercell_7x7_5_15_periodicInE2_excitons'
ending = '.npz'
whole_file = rootDir + folder + filename 
data_file = whole_file + ending

Data_Het = np.load(data_file)

Het_EE = Data_Het['EHM_EE']
Het_EE_DOS = np.loadtxt(rootDir + folder + 'Hamiltonian/exc_ham_7x7_DOS.dat')

#Loading file 2: Homogen eps = 5
filename = 'supercell_7x7_5_5_periodicOn_excitons'
ending = '.npz'
whole_file = rootDir + folder + filename 
data_file = whole_file + ending

Data_Hom5 = np.load(data_file)

Hom5_EE = Data_Hom5['EHM_EE']

#Loading file 3: Homogen eps = 15
filename = 'supercell_7x7_15_15_periodicOn_excitons'
ending = '.npz'
whole_file = rootDir + folder + filename 
data_file = whole_file + ending

Data_Hom15 = np.load(data_file)

Hom15_EE = Data_Hom15['EHM_EE']

#Loading file 4: Homogen eps=5 without Coulomb-interaction in exciton matrix
filename = 'supercell_7x7_5_5_periodicOn_excitons_withoutCoulomb'
ending = '.npz'
whole_file = rootDir + folder + filename 
data_file = whole_file + ending

Data_woC_Hom5 = np.load(data_file)

Hom5_woC_EE = Data_woC_Hom5['EHM_EE']


#Loading file 5: Homogen eps=15 without Coulomb-interaction in exciton matrix
filename = 'supercell_7x7_15_15_periodicOn_excitons_withoutCoulomb'
ending = '.npz'
whole_file = rootDir + folder + filename 
data_file = whole_file + ending

Data_Hom15 = np.load(data_file)

Hom15_woC_EE = Data_Hom15['EHM_EE']

#Loading file 6: Heterogen without Coloum-interaction in exciton matrix
filename = 'supercell_7x7_5_15_periodicInE2_excitons_withoutCoulomb'
ending = '.npz'
whole_file = rootDir + folder + filename 
data_file = whole_file + ending

Data_woC_Het = np.load(data_file)

Het_woC_EE = Data_woC_Het['EHM_EE']

#Loading file 7: Heterogen with Coulomb interaction, only density density contributions: DOS
filename = 'Hamiltonian/exc_ham_7x7_DOS_densdens.dat'
Het_densdens_DOS = np.loadtxt(rootDir + folder + filename)

#-------
#Calculating histograms
[Hist_Hom5, binEdges] = np.histogram(Hom5_EE,bins = numberofbins, range=(1.7,4.6))
bincenters =  0.5 * (binEdges[1:] + binEdges[:-1])
[Hist_woC_Hom5, binEdges] = np.histogram(Hom5_woC_EE,bins = numberofbins, range = (1.7, 4.6))

plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (homogeneous system $\varepsilon = 5$)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(bincenters, Hist_Hom5 , label=r'Excitonic gap $(TB+HF+BSE)$', color='black', linewidth=1.0)
plt.plot(bincenters, Hist_woC_Hom5, label=r'$E^{elec}_i - E^{hole}_j$ $(TB + HF)$', color='r', linewidth=1.0)
plt.legend(fontsize = (myFontSize - 4), loc='upper left')

fig1 = plt.gcf()

fig1.savefig(rootDir + save_folder + 'Excitonspectrum_homogen_7x7_eps5.eps', format='eps')
fig1.savefig(rootDir + save_folder + 'Excitonspectrum_homogen_7x7_eps5.pdf', format='pdf')
#homogen eps = 15

#heterogen eps = 5 und eps = 15
[Hist_Het, binEdges] = np.histogram(Het_EE,bins = numberofbins, range=(1.7,4.6))
bincenters =  0.5 * (binEdges[1:] + binEdges[:-1])
[Hist_woC_Het, binEdges] = np.histogram(Het_woC_EE,bins = numberofbins, range=(1.7,4.6))

plt.figure(2)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (heterogeneous system $\varepsilon_1 = 5$, $\varepsilon_2 = 15$)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.ylim([0,430])
plt.plot(bincenters, Hist_Het , label=r'Excitonic gap $(TB+HF+BSE)$', color='black', linewidth=1.0)
plt.plot(bincenters, Hist_woC_Het, label=r'$E^{elec}_i - E^{hole}_j$ $(TB + HF)$', color='r', linewidth=1.0)
plt.legend(fontsize = (myFontSize - 4), loc='upper right', borderaxespad = 0)


plt.savefig(rootDir + save_folder + 'Excitonspectrum_heterogen_7x7_eps5_15.eps', format='eps')
plt.savefig(rootDir + save_folder + 'Excitonspectrum_heterogen_7x7_eps5_15.pdf', format='pdf')
##############################
#homogeneous eps = 15
[Hist_Hom15, binEdges] = np.histogram(Hom15_EE,bins = numberofbins, range=(1.7,4.6))
bincenters =  0.5 * (binEdges[1:] + binEdges[:-1])
[Hist_woC_Hom15, binEdges] = np.histogram(Hom15_woC_EE,bins = numberofbins,range=(1.7,4.6))

plt.figure(3)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (homogeneous system $\varepsilon = 15$)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.7,4])

plt.plot(bincenters, Hist_Hom15 , label=r'Excitonic gap $(TB+HF+BSE)$', color='black', linewidth=1.0)
plt.plot(bincenters, Hist_woC_Hom15, label=r'$E^{elec}_i - E^{hole}_j$ $(TB + HF)$', color='r', linewidth=1.0)
plt.legend(fontsize = (myFontSize - 4), loc='upper right')

fig3 = plt.gcf()

fig3.savefig(rootDir + save_folder + 'Excitonspectrum_homogen_7x7_eps15.eps', format='eps')
fig3.savefig(rootDir + save_folder + 'Excitonspectrum_homogen_7x7_eps15.pdf', format='pdf')

fig4 = plt.figure(4)
ax = plt.axes()
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (TB + HF + BSE)', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(bincenters, Hist_Hom5, label=r'$\varepsilon_{1/2} = 5$', color = 'black', linewidth=1.0)
plt.plot(bincenters, Hist_Hom15, label=r'$\varepsilon_{1/2} = 15$', color = 'blue', linewidth=1.0)
plt.plot(bincenters, Hist_Het, label=r'$\varepsilon_1 = 5 \ \& \ \varepsilon_2 = 15$', color = 'r', linewidth=1.0)

plt.legend(fontsize = (myFontSize - 4))


fig4.savefig(rootDir + save_folder + 'Excitonspectrum_all_7x7.eps', format='eps')
fig4.savefig(rootDir + save_folder + 'Excitonspectrum_all_7x7.pdf', format='pdf')

fig5 = plt.figure(5)
ax = plt.axes()

plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (TB + HF + BSE)', fontsize=myFontSize)
ax.set_yticklabels([])

#plt.plot(bincenters, Hist_Hom5, label=r'$\varepsilon_{1/2} = 5$', color = 'black', linewidth=1.0)
#plt.plot(bincenters, Hist_Hom15, label=r'$\varepsilon_{1/2} = 15$', color = 'blue', linewidth=1.0)
plt.plot(Het_EE_DOS[:,0], Het_EE_DOS[:,1], label=r'Full Coulomb', color = 'r', linewidth=1.0)
plt.plot(Het_densdens_DOS[:,0], Het_densdens_DOS[:,1], label=r'Density-Density', color='green', linewidth = 1.0)

plt.legend(fontsize = (myFontSize - 4))


fig5.savefig(rootDir + save_folder + 'Excitonspectrum_all_7x7_densdens.eps', format='eps')
