# -*- coding: utf-8 -*-
"""
Created on Fri Jul 29 08:28:49 2016

@author: csteinke
"""
from __future__ import division

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc


rc('text', usetex='false') 
rc('font', family='sans-serif')


myFontSize = 20   
myFontSizeLabel = myFontSize - 2

nnCellsX = 9
nnCellsY = 6


rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folderDirac = 'Data/Excitons/DiracMoS2/'
folderHex = 'Data/Excitons/HexLadder/'
tfolderH1 = '/' #'/t_0.5/' #
tfolderH2 = '/t_inplane_2/'
tfolderD1 = '/t_0.5/'
tfolderD2 = '/' 

savefolder = 'Data/Excitons/'

eps_vec = np.array([2,5,1000000])



f2, ax2 = plt.subplots(1, 1, sharex='col', sharey='row', figsize=0.7*plt.figaspect(0.5))
plt.subplots_adjust(hspace = .0, wspace = .0)
plt.subplots_adjust(bottom = 0.16, left = 0.05, top = 0.99, right = 0.96)

   
for i in range(0,int(eps_vec.shape[0])):
        
    eps1 = eps_vec[i]
    eps2 = eps_vec[i]
    
    subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps1)+'_' + str(eps2)
    
    
    pathH1 = rootDir + folderHex + subfolder + tfolderH1

    ###-------Vgl. DOS/Joint_DOS
    #filename = 'supercell_'+str(nnCellsX)+'x'+str(nnCellsY)+'_'+str(eps1)+'_' + str(eps2)+'_periodicInE2_'
    filenameFull = 'exc_ham_'+str(nnCellsX)+'x'+str(nnCellsY)+'_DOS_eigs.dat'
    
    DOSH1 = np.loadtxt(pathH1 + filenameFull)
    

    
    ############global stuff################################


    ax2.tick_params(labelsize=myFontSizeLabel,labelleft='off')
    

    
    

    
    ################plotting stuff############################  
    
    if i == 0:

        
        ax2.plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = 2$', color = 'blue')

    elif i == 1:

        
        ax2.plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = 5$', color = 'red')
        
    elif i == 2:

        
        ax2.plot(DOSH1[:,0], DOSH1[:,1], label=r'$\varepsilon = \infty$', color = 'black')
    
    ax2.legend(fontsize = myFontSize - 2)
    
    ax2.set_xlabel('Energy (eV)',fontsize=myFontSize)
    ax2.set_ylabel('Frequency (arb. units)', fontsize = myFontSize)
    ax2.get_xticklabels()[-1].set_visible(False)

savename = rootDir + savefolder + 'homogen_DOS_all.pdf'
plt.savefig(savename, format = 'pdf')


