# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 18


#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/9x6/'
filename = 'exc_ham_9x6_DOS_eigs.dat'

savename = 'Excitonspectrum_9x6_'
#subfolder = 'Dielectric/5x5/'

numberofbins = 200

#Loading file 1: eps2/eps1 = 1
subfolder = '5_1/'
input_file = rootDir + folder + subfolder +filename
DOS_51 = np.loadtxt(input_file)

#File 2: eps2/eps1 = 1.5
subfolder = '5_2/'
input_file = rootDir + folder + subfolder +filename
DOS_52 = np.loadtxt(input_file)

#File 3: eps2/eps1 = 2
subfolder = '5_4/'
input_file = rootDir + folder + subfolder +filename
DOS_54 = np.loadtxt(input_file)

#File4: eps2/eps1 = 2.5
subfolder = '5_6/'
input_file = rootDir + folder + subfolder +filename
DOS_56 = np.loadtxt(input_file)

#File5: eps2/eps1 = 3
subfolder='5_10/'
input_file = rootDir + folder + subfolder +filename
DOS_510 = np.loadtxt(input_file)

subfolder = '5_15/'
input_file = rootDir + folder + subfolder +filename
DOS_515 = np.loadtxt(input_file)

subfolder = '6_15/'
input_file = rootDir + folder + subfolder +filename
DOS_615 = np.loadtxt(input_file)

subfolder = '7.5_15/'
input_file = rootDir + folder + subfolder +filename
DOS_715 = np.loadtxt(input_file)

subfolder = '10_15/'
input_file = rootDir + folder + subfolder +filename
DOS_1015 = np.loadtxt(input_file)

#Loading homogeneous data
subfolder = '5_5/'
input_file = rootDir + folder + subfolder +filename
DOS_5hom = np.loadtxt(input_file)

subfolder = '6_6/'
input_file = rootDir + folder + subfolder +filename
DOS_6hom = np.loadtxt(input_file)

subfolder = '4_4/'
input_file = rootDir + folder + subfolder +filename
DOS_4hom = np.loadtxt(input_file)

subfolder = '2_2/'
input_file = rootDir + folder + subfolder +filename
DOS_2hom = np.loadtxt(input_file)

subfolder = '1_1/'
input_file = rootDir + folder + subfolder +filename
DOS_1hom = np.loadtxt(input_file)

subfolder = '10_10/'
input_file = rootDir + folder + subfolder +filename
DOS_10hom = np.loadtxt(input_file)

subfolder = '6_6/'
input_file = rootDir + folder + subfolder +filename
DOS_6hom = np.loadtxt(input_file)

subfolder = '7.5_7.5/'
input_file = rootDir + folder + subfolder +filename
DOS_7hom = np.loadtxt(input_file)

subfolder = '15_15/'
input_file = rootDir + folder + subfolder +filename
DOS_15hom = np.loadtxt(input_file)


plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/1)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.1,2.5])
plt.ylim([0,200])

plt.plot(DOS_5hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 5$', color ='black', linewidth = 1.0)
plt.plot(DOS_1hom[:,0], DOS_1hom[:,1], label=r'$\varepsilon=1$', color='blue', linewidth=1.0)
plt.plot(DOS_51[:,0], DOS_51[:,1]  , label=r'$\varepsilon_1=5, \varepsilon_2 = 1$', color='red', linewidth=1.0)
plt.legend(loc='upper left')
    

plt.savefig(rootDir + folder + savename + '5_1_zoom.eps', format='eps')

plt.figure(2)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/2)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.4,2])
plt.ylim([0,200])

plt.plot(DOS_5hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 5$', color ='black', linewidth = 1.0)
plt.plot(DOS_2hom[:,0], DOS_2hom[:,1] , label=r'$\varepsilon=2$', color='blue', linewidth=1.0)
plt.plot(DOS_52[:,0], DOS_52[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 2$', color='red', linewidth=1.0)
plt.legend(loc='upper left')

plt.savefig(rootDir + folder + savename + '5_2_zoom.eps', format='eps')

plt.figure(3)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/4)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.6,2])
plt.ylim([0,200])

plt.plot(DOS_5hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 5$', color ='black', linewidth = 1.0)
plt.plot(DOS_4hom[:,0], DOS_4hom[:,1] , label=r'$\varepsilon=4$', color='blue', linewidth=1.0)
plt.plot(DOS_54[:,0], DOS_54[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 4$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_4_zoom.eps', format='eps')

plt.figure(4)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/6)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.65,1.9])
plt.ylim([0,200])

plt.plot(DOS_5hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 5$', color ='black', linewidth = 1.0)
plt.plot(DOS_6hom[:,0], DOS_6hom[:,1] , label=r'$\varepsilon=6$', color='blue', linewidth=1.0)
plt.plot(DOS_56[:,0], DOS_56[:,1] ,label=r'$\varepsilon_1=5, \varepsilon_2 = 6$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_6_zoom.eps', format='eps')

plt.figure(5)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/10)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.65,2])
plt.ylim([0,200])

plt.plot(DOS_5hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 5$', color ='black', linewidth = 1.0)
plt.plot(DOS_10hom[:,0], DOS_10hom[:,1] , label=r'$\varepsilon=10$', color='blue', linewidth=1.0)
plt.plot(DOS_510[:,0], DOS_510[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 10$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_10_zoom.eps', format='eps')

plt.figure(6)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (5/15)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.65,2])
plt.ylim([0,200])

plt.plot(DOS_5hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 5$', color ='black', linewidth = 1.0)
plt.plot(DOS_15hom[:,0], DOS_15hom[:,1] , label=r'$\varepsilon=15$', color='blue', linewidth=1.0)
plt.plot(DOS_515[:,0], DOS_515[:,1] , label=r'$\varepsilon_1=5, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '5_15_zoom.eps', format='eps')

plt.figure(7)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (6/15)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.65,2])
plt.ylim([0,200])

plt.plot(DOS_6hom[:,0],DOS_5hom[:,1], label = r'$\varepsilon = 6$', color ='black', linewidth = 1.0)
plt.plot(DOS_15hom[:,0], DOS_15hom[:,1] , label=r'$\varepsilon=15$', color='blue', linewidth=1.0)
plt.plot(DOS_615[:,0], DOS_615[:,1] , label=r'$\varepsilon_1=6, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '6_15_zoom.eps', format='eps')

plt.figure(8)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (7.5/15)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.65,2])
plt.ylim([0,200])

plt.plot(DOS_7hom[:,0],DOS_7hom[:,1], label = r'$\varepsilon = 7$', color ='black', linewidth = 1.0)
plt.plot(DOS_15hom[:,0], DOS_15hom[:,1] , label=r'$\varepsilon=15$', color='blue', linewidth=1.0)
plt.plot(DOS_715[:,0], DOS_715[:,1] , label=r'$\varepsilon_1=7, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '7_15_zoom.eps', format='eps')

plt.figure(9)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations (10/15)', fontsize=myFontSize)
ax.set_yticklabels([])
plt.xlim([1.65,2])
plt.ylim([0,200])

plt.plot(DOS_10hom[:,0],DOS_10hom[:,1], label = r'$\varepsilon = 10$', color ='black', linewidth = 1.0)
plt.plot(DOS_15hom[:,0], DOS_15hom[:,1] , label=r'$\varepsilon=15$', color='blue', linewidth=1.0)
plt.plot(DOS_1015[:,0], DOS_1015[:,1] , label=r'$\varepsilon_1=10, \varepsilon_2 = 15$', color='red', linewidth=1.0)
plt.legend()

plt.savefig(rootDir + folder + savename + '6_15_zoom.eps', format='eps')



#plt.figure(6)
#ax = plt.axes()
#rc('text', usetex=True)	
#rc('axes', labelweight='200')
#plt.xlabel('Energy (eV)', fontsize=myFontSize)
#plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
#plt.title(r'Electron - hole excitations)', fontsize=myFontSize)
#ax.set_yticklabels([])
#
#plt.plot(DOS_1[:,0], DOS_1[:,1] , label=r'$\varepsilon_2 / \varepsilon_1 = 1$', linewidth=1.0)
#plt.plot(DOS_15[:,0], DOS_1[:,1] , label=r'$\varepsilon_2 / \varepsilon_1 = 1.5$', linewidth=1.0)
#plt.plot(DOS_2[:,0], DOS_1[:,1] , label=r'$\varepsilon_2 / \varepsilon_1 = 2$', linewidth=1.0)
#plt.plot(DOS_25[:,0], DOS_1[:,1] , label=r'$\varepsilon_2 / \varepsilon_1 = 2.5$', linewidth=1.0)
#plt.plot(DOS_3[:,0], DOS_1[:,1] , label=r'$\varepsilon_2 / \varepsilon_1 = 3$', linewidth=1.0)
#plt.legend()
#
#plt.savefig(rootDir + folder + savename + 'all.eps', format='eps')


