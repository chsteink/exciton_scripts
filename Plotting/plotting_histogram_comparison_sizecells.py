# -*- coding: utf-8 -*-
"""
Created on Fri Jan 15 11:22:12 2016

@author: csteinke
"""


import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

#Plotting pf histograms for Corea
myFontSize = 18
savename = 'Excitonspectrum_comaprison_cellsize_'

#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'

subfolder = '7x7/'
filename = 'exc_ham_7x7_DOS.dat'
input_file = rootDir + folder + subfolder +filename
DOS_7x7 = np.loadtxt(input_file)


subfolder = '7x9/'
filename = 'exc_ham_7x9_DOS_eigs.dat'
input_file = rootDir + folder + subfolder +filename
DOS_7x9 = np.loadtxt(input_file)

subfolder = '9x6/5_15/'
filename = 'exc_ham_9x6_DOS_eigs.dat'
input_file = rootDir + folder + subfolder +filename
DOS_9x6 = np.loadtxt(input_file)

subfolder = '12x6/5_15/'
filename = 'exc_ham_12x6_DOS_eigs.dat'
input_file = rootDir + folder + subfolder +filename
DOS_12x6 = np.loadtxt(input_file)

subfolder = '14x7/'
filename = 'exc_ham_14x7_DOS_eigs.dat'
input_file = rootDir + folder + subfolder +filename
DOS_14x7 = np.loadtxt(input_file)

plt.figure(1)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_7x7[:,0], DOS_7x7[:,1]/np.max(DOS_7x7[:,1]) , label=r'7x7', color='red', linewidth=1.0)
plt.plot(DOS_7x9[:,0], DOS_7x9[:,1]/np.max(DOS_7x9[:,1]) , label=r'7x9', color='black', linewidth=1.0)

plt.plot(DOS_9x6[:,0], DOS_9x6[:,1]/np.max(DOS_9x6[:,1]) , label=r'9x6', color='blue', linewidth=1.0)
plt.plot(DOS_12x6[:,0], DOS_12x6[:,1]/np.max(DOS_12x6[:,1]) , label=r'12x6', color='green', linewidth=1.0)
plt.plot(DOS_14x7[:,0], DOS_14x7[:,1]/np.max(DOS_14x7[:,1]) , label=r'14x7', color='navy', linewidth=1.0)

plt.legend()
plt.savefig(rootDir + folder + savename + 'all.eps', format='eps')


plt.figure(2)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_7x7[:,0], DOS_7x7[:,1]/np.max(DOS_7x7[:,1]) , label=r'7x7', color='red', linewidth=1.0)
plt.plot(DOS_7x9[:,0], DOS_7x9[:,1]/np.max(DOS_7x9[:,1]) , label=r'7x9', color='black', linewidth=1.0)

plt.legend()
plt.savefig(rootDir + folder + savename + 'y_dir.eps', format='eps')



plt.figure(3)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_9x6[:,0], DOS_9x6[:,1]/np.max(DOS_9x6[:,1]) , label=r'9x6', color='red', linewidth=1.0)
plt.plot(DOS_12x6[:,0], DOS_12x6[:,1]/np.max(DOS_12x6[:,1]) , label=r'12x6', color='black', linewidth=1.0)

plt.legend()
plt.savefig(rootDir + folder + savename + 'x_dir.eps', format='eps')

plt.figure(4)
ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('Frequency (arb. units)', fontsize=myFontSize)
plt.title(r'Electron - hole excitations', fontsize=myFontSize)
ax.set_yticklabels([])

plt.plot(DOS_9x6[:,0], DOS_9x6[:,1]/np.max(DOS_9x6[:,1]) , label=r'9x6', color='red', linewidth=1.0)
plt.plot(DOS_14x7[:,0], DOS_14x7[:,1]/np.max(DOS_14x7[:,1]) , label=r'14x7', color='black', linewidth=1.0)


plt.legend()
plt.savefig(rootDir + folder + savename + 'size_cell.eps', format='eps')

