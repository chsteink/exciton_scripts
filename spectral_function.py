# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 15:33:04 2016

@author: csteinke
"""

from __future__ import division
from __future__ import print_function


#Defining directory in which functions can be found
import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
#matplotlib.use('PS')
import matplotlib.pyplot as plt

import matplotlib.mlab as ml


def AtomIndex(grid,vec):
    #only works when vec and grid are in the same basis!

    nnAtoms = grid.shape[0]
    vec = np.tile(vec,(nnAtoms,1))
    
    Diff = np.sum(abs(grid-vec),axis=1)
    
    ind = np.where(Diff<10**(-6))[0]
    
    return ind
    

#Calculating Spectral function from excitonic spectrum

print('Loading Data ')

nnCellsX = 5
nnCellsY = 5

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/'
filename = 'supercell_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_5_15_periodicInE2_'
path = rootDir + folder + subfolder #+ filename 

elecStates = np.loadtxt(path + 'elec_states.dat')
elecStates = np.transpose(elecStates)
holeStates = np.loadtxt(path + 'hole_states.dat')
holeStates = np.transpose(holeStates)

Eigenvals = np.loadtxt(rootDir + folder + subfolder + 'exc_ham_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_eigenvals_eigs.dat')
Eigenvec = np.loadtxt(rootDir + folder + subfolder+ 'exc_ham_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_eigenvec_eigs.dat')

#sorting eigenvalues
sortind = np.argsort(Eigenvals)
Eigenvals = Eigenvals[sortind]
Eigenvec = Eigenvec[:,sortind]
        
#Defining Energy-range and broadening
EnergyMin = np.min(Eigenvals)
EnergyMax = np.max(Eigenvals)
Energystep = 0.01
Energy = np.arange(EnergyMin,EnergyMax,Energystep)
Broadening = 0.005


 #loading grid
grid = np.loadtxt(rootDir + 'Data/Python/HexLadder/supercell'+str(nnCellsX) + 'x' + str(nnCellsY) + '_cart_grid.dat')
gridCart = grid[:,0:3]

#Creating relative and center of mass vectors
NumberAtoms = gridCart.shape[0]
RelativeVector = np.zeros((NumberAtoms,NumberAtoms,3))
MassCenter = np.zeros((NumberAtoms, NumberAtoms, 3))

for riHol in range(0, NumberAtoms):
    for rjEl in range(0, NumberAtoms):
        
        RelativeVector[riHol,rjEl,:] = gridCart[riHol,:] - gridCart[rjEl, :]
        MassCenter[riHol, rjEl,:] = 1/2*(gridCart[riHol,:] + gridCart[rjEl,:])

#Defining interesting relative vector
RelVec0 = np.array([0.,0.,0.])

Indizes_Rel = np.where((RelVec0[0] == RelativeVector[:,:,0])&(RelVec0[1] == RelativeVector[:,:,1])&(RelVec0[2] == RelativeVector[:,:,2]))
MassCenter0 = MassCenter[Indizes_Rel[0],Indizes_Rel[1],:]

#Other idea to get MassVectors
#for riHol in range(0,NumberAtoms):
#    for rjEl in range(0,NumberAtoms):
#        r_tmp = gridCart[riHol,:] - gridCart[rjEl,:]
#        
#        if (r_tmp == RelVec0).all():
#            S = 1/2*(gridCart[riHol,:] + gridCart[rjEl,:])
            
        

NumberMassVectors = MassCenter0.shape[0]

 #generating double indices for sum
N=elecStates.shape[1]
     
indices = np.arange(0,N)
     
for ii in range(0,N):
    
    dummy = np.ones((N,2))
    dummy[:,0] = ii * dummy[:,0]
    dummy[:,1] = indices
    
    if ii == 0: 
        
        ind_mat = dummy
         
    else:
        ind_mat = np.append(ind_mat,dummy, axis = 0)
     
ind_mat = ind_mat.astype(int)
numberEigenval = Eigenvals.shape[0]
numberCombination = ind_mat.shape[0]

numberEnergy = Energy.shape[0]
#sum over all center of mass vectors
SpectralFunc = np.zeros((numberEnergy))


print('Calculating spectral function')
for iMass in range(0, NumberMassVectors):

    Loop = iMass/NumberMassVectors*100
    print('Calculated %.2f Percent of Mass Vectors' % Loop)
    #generating space-index to know which row to choose from elec/hole states
    rElec = MassCenter0[iMass,:]-RelVec0/2
    rHol = MassCenter0[iMass,:] + RelVec0/2    
    ind_elec = AtomIndex(gridCart, rElec)
    ind_hole = AtomIndex(gridCart, rHol)
    
    Sum_nu = np.zeros((numberEnergy))
    for nu in range(0,numberEigenval):
        
    #sum over all electron and hole states
        Coeff_sum = 0
        for ii in range(0,numberCombination):

            Coeff_sum = Coeff_sum + Eigenvec[ii,nu]*elecStates[ind_elec,ind_mat[ii,0]]*holeStates[ind_hole,ind_mat[ii,1]]        
        
        Sum_nu[:] = Sum_nu[:] + np.abs(Coeff_sum)**2 * Broadening/(Broadening**2 + (Energy - Eigenvals[nu])**2)
        
    SpectralFunc[:] = SpectralFunc + Sum_nu

SpectralFunc = 2*np.pi * SpectralFunc

SaveFunc = np.zeros((numberEnergy,2))
SaveFunc[:,0] = Energy
SaveFunc[:,1] = SpectralFunc

np.savetxt(rootDir + folder + subfolder + 'absorption_spectra_'+str(nnCellsX) + 'x' + str(nnCellsY) + '_atRelVec_' + str(RelVec0[0])+'_' + str(RelVec0[1])+'_'+ str(RelVec0[2])+'.dat', SaveFunc)