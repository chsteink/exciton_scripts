# -*- coding: utf-8 -*-
"""
Created on Wed May 18 08:10:52 2016

@author: csteinke
"""

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc

#Loading Data 
os.system(['clear','cls'][os.name == 'nt'])

myFontSize = 18
nnCellsX = 9
nnCellsY = 6

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
#folder = 'Data/Excitons/DiracMoS2/'
folder = 'Data/Excitons/HexLadder/'
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/2_2'
subfolder2 = str(nnCellsX) + 'x' + str(nnCellsY) + '/5_5'
tfolder = '/t_inplane_2/' #'/t_inplane_2/'
#filename = 'supercell_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_5_15_periodicInE2_'
path = rootDir + folder + subfolder + tfolder + 'OpticalSpectra/'
path2 = rootDir + folder + subfolder2 + tfolder + 'OpticalSpectra/'


Broadening = 0.1#0.1

print('Loading Data')
#Loading Dipole elements

E = 1

Polarization = np.zeros(3,dtype = int)


for ii in range(0,3):
    Polarization[ii] = E
    Ending = str(Polarization[0]) +str(Polarization[1]) + str(Polarization[2])
    
    if Broadening == 0:
        Spectrum = np.loadtxt(path  +'OpticalSpectrum_broadened'+Ending+'.dat')
        #Spectrum_wo = np.loadtxt(path  +'OpticalSpectrum_wocoulomb_broadened'+Ending+'.dat')
    
    else:
       # Spectrum_wo= np.loadtxt(path + 'OpticalSpectrum_wocoulomb_broadened_'+str(Broadening) + '_' +Ending+'.dat')
        Spectrum = np.loadtxt(path + 'OpticalSpectrum_broadened_'+str(Broadening) + '_' +Ending+'.dat')
    
    if ii == 0:
        Spectrum_save = Spectrum
        #Spectrum_wo_save = Spectrum_wo
    else:
        Spectrum_save = np.append(Spectrum_save, np.reshape(Spectrum[:,1], (Spectrum[:,1].shape[0],1)),axis=1)
        #Spectrum_wo_save = np.append(Spectrum_wo_save, np.reshape(Spectrum_wo[:,1],(Spectrum_wo[:,1].shape[0],1)),axis=1)
    
    Polarization[ii] = 0
  
Spectrum_unpolarized = np.sum(Spectrum_save[:,1:5], axis = 1)  
#Spectrum_wo_unpolarized = np.sum(Spectrum_wo_save[:,1:5], axis=1)

Spectrum_plot = np.array([Spectrum_save[:,0], Spectrum_unpolarized]).T
#Spectrum_wo_plot = np.array([Spectrum_wo_save[:,0], Spectrum_wo_unpolarized]).T


            
plt.figure()

ax = plt.axes()
rc('text', usetex=True)	
rc('axes', labelweight='200')
plt.xlabel('Energy (eV)', fontsize=myFontSize)
plt.ylabel('I(E) (arb. units)', fontsize=myFontSize)
plt.title(r'Absorption spectrum' , fontsize=myFontSize)

   
plt.plot(Spectrum_plot[:,0], Spectrum_plot[:,1], label=r'$Full$', color='red', linewidth=1.0)
#plt.plot(Spectrum_dens[:,0], Spectrum_dens[:,1], label=r'Density Density spectrum', color='black', linewidth=1.0)
#plt.plot(Spectrum_wo_plot[:,0], Spectrum_wo_plot[:,1], label=r'without Coulomb', color='green', linewidth=1.0)
#plt.plot(Spectrum5[:,0], Spectrum5[:,1], label=r'$\varepsilon = 5$', color='blue', linewidth=1.0)

plt.tick_params(labelleft = 'off')
plt.legend(loc='upper right', borderaxespad = 0)                       

plt.savefig(path +'OpticalSpectrum_vgl_broadened_'+str(Broadening) + '_' +Ending+'.eps', format='eps')

if Broadening == 0:
    
    np.savetxt(path +'OpticalSpectrum_broadened_unpol.dat', Spectrum_plot)
    #np.savetxt(path + 'OpticalSpectrum_wocoulomb_broadened_unpol.dat', Spectrum_wo_plot)
    plt.savefig(path + 'OpticalSpectrum_vgl_broadened_unpol.eps', format = 'eps')

else:
    np.savetxt(path +'OpticalSpectrum_broadened_'+str(Broadening) + '_unpol.dat', Spectrum_plot)
    #np.savetxt(path + 'OpticalSpectrum_wocoulomb_broadened_'+str(Broadening)+'_unpol.dat', Spectrum_wo_plot)
    plt.savefig(path +'OpticalSpectrum_vgl_broadened_'+str(Broadening) + '_unpol.eps', format='eps')
    