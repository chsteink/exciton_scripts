//#############################################################
// spectral_function.cpp
//#############################################################
// Calculates spectral function in dependence of the energy
// and the space vector of the electron and the mass 
// A(w, r_e, r_h)
//#############################################################
// Version 1.0
//
// - initially written by Christina Steinke based on a program of 
//   Daniel Mourad
//
// - Including parallel processing
//
// Last changes: 2016-08-22
//
//Compile with:  g++ spectral_function_direct_woCoul.cpp -o spectral_direct_wo -O3 -march=native -fopenmp

//#############################################################

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <vector>
#include <sstream>
#include <omp.h>
#include "boost/multi_array.hpp"

//##################################################################
// Use the following from the standard namespace
//##################################################################
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

//##################################################################
//                  global variables
//##################################################################

const double Pi = 3.141592654;

//##################################################################

//##################################################################
//                 TYPE DEFINITIONS
//##################################################################
// define complex-valued 3-dimensional array datatype 
// from "boost/multi_array.hpp" 
//typedef boost::multi_array<double, 2> array2double;

//typedef boost::multi_array<double, 4> array4double;

//##################################################################
//                DEFINITIONS
//##################################################################

//###############################################
// define a modulo-Funktion in analogy to MATLAB
// as "%" operator (sign-sensitive!!!)
//###############################################
#define MOD(a,b) ( ((a) < 0) ? (b)- ( (-(a)) % (b)) : (a) % (b) )

//##################################################################
//                     Function prototypes
//##################################################################
// --------------> not needed at the moment


//##################################################################
//##################################################################
//                             MAIN
//##################################################################
//##################################################################

// Main expects command line parameters, of which the number is in 
// argc and the parameters itself are subsequently in argv[], where 
// argv[0] contains the progranm name itself
//
//
// argv[0]: program name itself
// argv[1]: number of unit cells in x-direction
// argv[2]: number of unit cells in y-direction

//
// example: "./spectral_function 9 6"

int main(int argc, char* argv[])
{
  //=====================================================================
  //=====================================================================
  // Parameters to change
  //=====================================================================
  //=====================================================================
  
  
  //Broadening of delta-function
   double broadening = 0.1;
  //Energy steps
  double energyStep = 0.2;
  
  
  // File name of exported Spectral Function: suffix of atom number will be added later
  std::string spectralFuncFilename("./spectral_function_woCoul");
  
  
  //============================================================
  //============================================================
  // End of free parameter part
  //============================================================
  //============================================================
  
  //############################################################
  //Check the number of input parameters
  //############################################################
  if (argc < 3) {
    // Tell the user how to run the code with command line parameters:
    // - as stated above argv[0] contains the progranm name itself
    // - we need three additional parameters:
    //  (1) the number of unit cells in x-direction
    //  (2) the number of unit cells in y-direction
    
    cerr << endl << "USAGE: " << argv[0] \
    << " nnCellsXX nnCellsYY" 
    << endl << endl;
    cerr << "EXAMPLE: " << argv[0] << " 9 6" << endl << endl;
    
    return 1;
  }
  
  //##################################################################
  //   Check and convert the command line input parameters themselves
  //##################################################################
  
  // Define input stringstreams to convert the argv[1], argv[2] to int  
  std::istringstream nnCellsXXStream(argv[1]);
  std::istringstream nnCellsYYStream(argv[2]);
  // 
  int nnCellsXX; 
  int nnCellsYY;
  // 
  nnCellsXXStream >> nnCellsXX;
  nnCellsYYStream >> nnCellsYY;
  
  std::string nnCellsXXstr(nnCellsXXStream.str());
  std::string nncellsYYstr(nnCellsYYStream.str());
  // 
  
  //##################################################################
  //  Defining places where data is stored
  //##################################################################
  
  //Careful: the electron and hole states are transposed! elecState[iiState, iPlace] 
  //instead of elecState[iPlace, iiState]!!
  // 
  const std::string locationElectronStates("./elec_states.dat");
  const std::string locationHoleStates("./hole_states.dat");
  const std::string locationExcitonStates( "./exc_ham_"+ nnCellsXXstr + "x" + nncellsYYstr + "_wocoulomb_eigenvec.dat");
  const std::string locationExcitonEnergies("./exc_ham_"+ nnCellsXXstr + "x" + nncellsYYstr + "_wocoulomb_eigenvals.dat");
  const std::string locationGrid("/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/Data/Python/HexLadder/supercell" \
  +nnCellsXXstr+"x" + nncellsYYstr + "_cart_grid.dat");
  
  //Location of electron vectors for which spectral function is calculated
  const std::string locationElecVec("./SpectralFunc_ElecPos.dat"); 
  
  //Location of hole vectors for spectral function
  const std::string locationHoleVec("./SpectralFunc_HolePos.dat"); 
  
  
  
  //######################################################
  //######################################################
  // Read in data
  //######################################################
  //######################################################
  
  // Initialize the vectors (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > electronStatesMatrix;
  std::vector< std::vector<double> > holeStatesMatrix;
  
  //Create a string to read line into it
  std::string inputline;
  
  // Dummy vaiable to stream the TB expansion coefficients
  double coefficientDummy;
  
  //#####################################################
  //       Sinlge particle Electron States
  //#####################################################
  // Open input filestream to read in the values
  std::ifstream electronStatesStream;
  electronStatesStream.open(locationElectronStates.c_str());
  
  if(electronStatesStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading electron states from " << endl \
    << locationElectronStates << endl << endl;
    
    while(std::getline(electronStatesStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> coefficientPerSite;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	coefficientPerSite.push_back(coefficientDummy);
	
      }
      
      
      electronStatesMatrix.push_back(coefficientPerSite);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationElectronStates << "\" not found..." \
    << endl << endl;
    return 1;
  }
  
  
  //#####################################################
  //     Single Particle Hole States
  //#####################################################
  std::ifstream holeStatesStream;
  holeStatesStream.open(locationHoleStates.c_str());
  
  if(holeStatesStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading hole states from " << endl \
    << locationHoleStates << endl << endl;
    
    while(std::getline(holeStatesStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> coefficientPerSite;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	coefficientPerSite.push_back(coefficientDummy);
	
      }
      
      holeStatesMatrix.push_back(coefficientPerSite);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationHoleStates << "\" not found..." \
    << endl << endl;
    return 1;
  }
  
    //#####################################################
  // Loading electronVector information of the supercell
  //#####################################################
  
  // Initialize the vector (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > elecVectorMat;
  
  std::ifstream elecVecStream;
  elecVecStream.open(locationElecVec.c_str());
  
  if(elecVecStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading electron Vectors from " << endl \
    << locationElecVec << endl << endl;
    
    while(std::getline(elecVecStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> elecVecComponents;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	elecVecComponents.push_back(coefficientDummy);
	
      }
      
      elecVectorMat.push_back(elecVecComponents);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationElecVec << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  }
  
   //Delete last column because it contains unneccessary information for this purpose (counting starts with zero)
  unsigned int columnToDelete;
  columnToDelete = elecVectorMat[0].size() - 1; 
  cout << "Column to Delete:" << columnToDelete << endl;
  
  
  for (unsigned i = 0; i < elecVectorMat.size(); ++i)
  {
    
    if (elecVectorMat[i].size() > columnToDelete)
    {
      
      elecVectorMat[i].erase(elecVectorMat[i].begin() + columnToDelete); 
    }
  }
  
  
    int sizeElecVec = elecVectorMat.size();
    
    cout << "Found " << sizeElecVec << " electron vectors! " << endl; 
    
      //#####################################################
  // Loading hole vector information of the supercell
  //#####################################################
  
  // Initialize the vector (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > holeVectorMat;
  
  std::ifstream holeVecStream;
  holeVecStream.open(locationHoleVec.c_str());
  
  if(holeVecStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading hole Vectors from " << endl \
    << locationHoleVec << endl << endl;
    
    while(std::getline(holeVecStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> holeVecComponents;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	holeVecComponents.push_back(coefficientDummy);
	
      }
      
      holeVectorMat.push_back(holeVecComponents);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationHoleVec << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  }
  
  
  for (unsigned i = 0; i < holeVectorMat.size(); ++i)
  {
    
    if (holeVectorMat[i].size() > columnToDelete)
    {
      
      holeVectorMat[i].erase(holeVectorMat[i].begin() + columnToDelete); 
    }
  }
  
  
    int sizeHoleVec = holeVectorMat.size();
    
    cout << "Found " << sizeHoleVec << " hole vectors! " << endl; 
    
  
  //#####################################################
  // Loading grid information of the supercell
  //#####################################################
  
  // Initialize the vector (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > gridMatrix;
  
  std::ifstream gridStream;
  gridStream.open(locationGrid.c_str());
  
  if(gridStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading grid from " << endl \
    << locationGrid << endl << endl;
    
    while(std::getline(gridStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> gridComponents;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	gridComponents.push_back(coefficientDummy);
	
      }
      
      gridMatrix.push_back(gridComponents);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationGrid << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  }
  
  
  //Delete last column because it contains unneccessary information for this purpose (counting starts with zero)
  
  //gridMatrix[i] ruft Zeile i der Matrix auf, gridMatrix.size() gibt Anzahl der Zeilen
  
  for (unsigned i = 0; i < gridMatrix.size(); ++i)
  {
    
    if (gridMatrix[i].size() > columnToDelete)
    {
      
      gridMatrix[i].erase(gridMatrix[i].begin() + columnToDelete); 
    }
  }
  
  int dimension;
  dimension = gridMatrix[0].size();
  cout << "Dimension of grid:" << dimension << endl;
  
  //#####################################################
  // Loading excitonic eigenvalues
  //#####################################################
  std::vector< double>  excEigenvals;
  
  std::ifstream exEigenvalsStream;
  exEigenvalsStream.open(locationExcitonEnergies.c_str());
  
  if(exEigenvalsStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading excitonic eigenvalues from " << endl \
    << locationExcitonEnergies << endl << endl;
    
    while(exEigenvalsStream >> coefficientDummy)
    {
      
      excEigenvals.push_back(coefficientDummy);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationExcitonEnergies << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  }
  //Check if eigenvalues are sorted in ascending order, if not quit
  if (excEigenvals[0] > excEigenvals[1])
  {
    cerr << "STOP: Excitonic eigenvalues not sorted!" << endl << endl;
    return 1;
  }
  
  //#####################################################
  // Loading excitonic eigenvectors
  //#####################################################
  
  // Initialize the vector (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > excStatesMatrix;
  
  std::ifstream excStatesMatrixStream;
  excStatesMatrixStream.open(locationExcitonStates.c_str());
  
  if(excStatesMatrixStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading excitonic eigenstates from " << endl \
    << locationExcitonStates << endl << endl;
    
    while(std::getline(excStatesMatrixStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> coefficientPerEnergy;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	coefficientPerEnergy.push_back(coefficientDummy);
	
      }
      
      excStatesMatrix.push_back(coefficientPerEnergy);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationExcitonStates << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  } 
  
  // Timer to measure the elapses wall clock and processor time
clock_t startProcTime, stopProcTime;
time_t startWallTime, stopWallTime;
startProcTime = clock();
startWallTime = time(0);



  //######################################################
  //######################################################
  // Calculation part of spectral function
  //######################################################
  //######################################################
  
  //Generating numbers of states, energies and so on to create
  //for loops
  
  int numberExcEnergies = excEigenvals.size();
  int numberHoleStates = nnCellsXX * nnCellsYY * 4 * 0.5;
  int numberElecStates = nnCellsXX * nnCellsYY * 4 * 0.5;
  int numberAtoms = nnCellsXX * nnCellsYY * 4 ;
  int numberCombination  = numberHoleStates*numberElecStates; 
  
  //Transposing eigenstate matrix for faster perfomance 
    cout << "--------------------------------------------------" << endl;
cout << "Transpose eigenstate matrix" << endl; 
std::vector< std::vector<double> > excStatesMatrix_transposed(numberExcEnergies, std::vector<double>(numberCombination)); 

//numberExcEnergies = new number of rows (old number of columns)
//numberCombination = new number of columns (old number of rows) 

for(int colT=0; colT< numberCombination; colT++)
{
      for(int rowT=0; rowT< numberExcEnergies ; rowT++)  
	{
            excStatesMatrix_transposed[rowT][colT]= excStatesMatrix[colT][rowT];
        }
    }
  
  
 //######################################################
  // Generating index matrix for electron and hole states
  // line corresponds to excitonic index
  //#####################################################
  
  cout << "Calculating index matrix" << endl; 
  
  // std::vector< std::vector< int > > indMat(numberCombination,std::vector<int>(2));
  std::vector < int > indMat_el(numberCombination); 
   std::vector < int > indMat_hol(numberCombination); 
  int countRow= 0;
  
  for (unsigned int iEl = 0; iEl < numberElecStates; ++iEl)
  {
    for (unsigned int jHol = 0; jHol < numberHoleStates; ++jHol)
    {
  //    indMat[countRow][0] = iEl;
	indMat_el[countRow] = iEl; //first column
	indMat_hol[countRow] = jHol; //second column
  //    indMat[countRow][1] = jHol;
  
  
      countRow = countRow+ 1 ;
    }
  }
  
  
  //######################################################
  // Generating input energy values
  //#####################################################
  
  double minEnergy = *std::min_element(excEigenvals.begin(), excEigenvals.end());
  double maxEnergy = *std::max_element(excEigenvals.begin(), excEigenvals.end());
  
  std::vector<double> Energy;
  double tmp_value = 0;
  int loopCount = 0;
  
  cout << "Calculating Energy vector for Spectral function" << endl;
 while(tmp_value < (maxEnergy-energyStep))
 { 
    tmp_value = minEnergy + loopCount*energyStep;
    Energy.push_back(tmp_value);
    loopCount = loopCount + 1;

  }
  
  int sizeEnergy = Energy.size();

  int energyCount; 
  
  
  //######################################################
  // Calculating the Spectral function
  //######################################################
      cout << "--------------------------------------------------" << endl;
  cout << "Start calculation of spectral function" << endl;
  
//looping over electron vectors: calculate for every electron place spectral function at hole place

  for (unsigned int iElecVec = 0; iElecVec < sizeElecVec; ++iElecVec)
  {
    std::vector < double> rElec(dimension);
    
    for(unsigned int kk = 0; kk < elecVectorMat[0].size(); ++kk)
    {
    rElec[kk] = elecVectorMat[iElecVec][kk];
    }
cout << "--------------------------------------------------" << endl;     
     cout << "Calculation for electron vector number " << iElecVec + 1 << " from " << sizeElecVec << endl; 
     cout << "--------------------------------------------------" << endl;
  
   std::vector< std::vector<double> > Spectral_func(sizeEnergy, std::vector<double>(sizeHoleVec));

    //Loop through all hole vectors
  for(signed int iHoleVec = 0; iHoleVec < sizeHoleVec; ++iHoleVec)
  {
    //getting position of hole
    std::vector< double> rHole(dimension);
  
    for(unsigned int kk = 0; kk < holeVectorMat[0].size(); ++kk)
    {
      rHole[kk] = holeVectorMat[iHoleVec][kk];
    }

    
   
  #pragma omp parallel for
        //Loop through chosen energy values

   
      
  for(int iEnergy = 0; iEnergy < sizeEnergy; ++iEnergy)
  { 
    //Progress counter
  #pragma omp atomic
   
    energyCount += 1;
    
    if (energyCount % 4 == 0)
    {
        #pragma omp critical    
           cout << energyCount/double(sizeEnergy)*100 << "% of energies calculated" << endl; 
    }

    
    
      

    int ind_elec;
    int ind_hole;
  
    std::vector< double > difference_dummy_elec(dimension);
    std::vector< double > difference_dummy_hole(dimension);
  
    
    //Finding row index of electron/hole space vector to determine column of elec/hole state 
    for (unsigned int row = 0; row < numberAtoms; ++row)
    {
      for (unsigned int component = 0; component < dimension; ++component)
      {
	difference_dummy_elec[component] = std::abs(gridMatrix[row][component] - rElec[component]);
	difference_dummy_hole[component] = std::abs(gridMatrix[row][component] - rHole[component]);     
      } //endfor comparing gridMatrix with electron place
      
      if(dimension == 3)
      {
	if (difference_dummy_elec[0] < 1e-6 && difference_dummy_elec[1] < 1e-6 && difference_dummy_elec[2] < 1e-6)
	{
	  ind_elec = row;
	} //endif ind_elec
      
	if (difference_dummy_hole[0] < 1e-6 && difference_dummy_hole[1] < 1e-6 && difference_dummy_hole[2] < 1e-6)
	{
	  ind_hole = row;
	} //endif ind_hole
      } //endif dimension
      
      if(dimension == 2)
      {
	if (difference_dummy_elec[0] < 1e-6 && difference_dummy_elec[1] < 1e-6)
	{
	  ind_elec = row;
	} //endif ind_elec
      
	if (difference_dummy_hole[0] < 1e-6 && difference_dummy_hole[1] < 1e-6)
	{
	  ind_hole = row;
	} //endif ind_hole
      } //endif dimension
	
    } //endfor row

   //Sum over all excitonic eigenvalues
  
    double Sum_iExc = 0;
    
      for (int iExc = 0; iExc < numberExcEnergies; ++iExc)
    {
    
    //Calculating multiplicaton of coefficient of eigenvectos
      double coeffMult = 0;
    for (int ii = 0; ii < numberCombination; ++ii)    
    {
      int ind_elecState = indMat_el[ii]; 
      int ind_holeState = indMat_hol[ii];
      coeffMult =  coeffMult + excStatesMatrix_transposed[iExc][ii] * electronStatesMatrix[ind_elecState][ind_elec] * holeStatesMatrix[ind_holeState][ind_hole];
    } //endfor multiplication of coefficients
    
    
    Sum_iExc = Sum_iExc + std::abs(coeffMult)*std::abs(coeffMult) * broadening/(std::pow(broadening,2.) + std::pow((Energy[iEnergy] - excEigenvals[iExc]),2.));
    
    } //endfor excitonic eigenvalues
    
    
    Spectral_func[iEnergy][iHoleVec] = Spectral_func[iEnergy][iHoleVec] + 2.*Pi*Sum_iExc;
    
  
  } //endfor iEnergy
 
  } //endfor iHole Loop  
  //######################################################
  // Writing Energy and corresponding Spectral function in file
  //######################################################
  
  std::stringstream iElecVecStream;
  iElecVecStream << iElecVec;
  
  //std::stringstream iHoleVecStream;
  //iHoleVecStream << iHoleVec;
  
  std::string elecVectorStr(iElecVecStream.str());
  //std::string holeVectorStr(iHoleVecStream.str());
  std::string spectralFilenameSave; 
  
  //spectralFilenameSave = spectralFuncFilename + "_elec" + elecVectorStr + "_hole" + holeVectorStr + ".dat";
  spectralFilenameSave = spectralFuncFilename + "_elec" + elecVectorStr + ".dat";

// Open output file stream
std::ofstream spectralOfStream(spectralFilenameSave.c_str());

// Set the numerical precision
spectralOfStream.precision(12);

cout << "--------------------------------------------------" << endl;
cout << "Exporting spectral function to " << endl
     << spectralFilenameSave << endl << endl;
cout << "--------------------------------------------------" << endl;
     
if (dimension == 3)
{
  spectralOfStream << "#electron: (" << rElec[0] << "," << rElec[1] << "," << rElec[2] << ")" << endl; 
}

if (dimension == 2)
{
  spectralOfStream << "#electron: (" << rElec[0] << "," << rElec[1]  << ")" << endl; 
}


 for(unsigned int iiStream = 0; iiStream < sizeEnergy; ++iiStream)
 {
   spectralOfStream << Energy[iiStream];
   
   for (unsigned int kkHoleStream = 0; kkHoleStream < sizeHoleVec; ++kkHoleStream)
   {
      spectralOfStream  << " " << Spectral_func[iiStream][kkHoleStream];
   }
   spectralOfStream << endl; 
 }					 
  
    // Timer to measure the elapsed time 
    
stopProcTime = clock();
stopWallTime = time(0);
double totalProcTime = (stopProcTime - startProcTime) \
                      / (double)CLOCKS_PER_SEC;
double totalWallTime = (stopWallTime - startWallTime);
cout << "##############################################" << endl;
cout << " Elapsed WALL CLOCK TIME: " << totalWallTime << " seconds" << endl;
cout << " Elapsed PROCESSOR TIME:  " << totalProcTime << " seconds" << endl;
cout << "##############################################" << endl;
cout << endl;

 
  
  } //endfor iElecVec
  
//#################################################
// CLOSE MAIN
// #################################################
  
}