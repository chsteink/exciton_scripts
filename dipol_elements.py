# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 15:16:20 2016

@author: csteinke
"""

#Programm calculating dipole matrix elements
#Matrix elements of impulse operator maybe wrong sign!

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

latticeConstant = 3.18
nnCellsX = 9
nnCellsY = 6

eps1 = 5
eps2 = 2

system = 'DiracMoS2' 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/'+system+'/'
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) + '/' + str(eps1)+'_' + str(eps2)
tfolder = '/t_0.5/' #'/t_inplane_2/'
filename = 'supercell_'+str(nnCellsX)+'x'+str(nnCellsY)+'_'+str(eps1)+'_' + str(eps2)+'_periodicInE2_'
path = rootDir + folder + subfolder + tfolder
print('Loading Data from %s' % path)
#path = rootDir + folder + subfolder + filename 

elecStates = np.transpose(np.loadtxt(path + 'elec_states.dat'))
holeStates = np.transpose(np.loadtxt(path + 'hole_states.dat'))
holeEnerg = np.loadtxt(path + 'hole_energies.dat')
elecEnerg = np.loadtxt(path + 'elec_energies.dat')
grid = np.loadtxt(rootDir + 'Data/Python/'+system+'/supercell'+str(nnCellsX) + 'x' + str(nnCellsY) +'_cart_grid.dat')
grid = grid[:,0:-1]

#Loading hamilton-operator
#Hamiltonian_path = rootDir + 'Data/Python/HexLadder/Dielectric/' + str(nnCellsX) + 'x' + str(nnCellsY) + tfolder + filename + '.npz'
#Tmp = np.load(Hamiltonian_path)
#HFTBM = np.real(Tmp['HFTBM'])

#TBM = np.loadtxt(rootDir + 'Data/Python/HexLadder/Dielectric/'+str(nnCellsX) + 'x' + str(nnCellsY) + tfolder + filename +  'TBM.dat')

#defining origin in middle of supercell

#x0 = nnCellsX * latticeConstant/2
#y0 = nnCellsY * np.sqrt(3)*latticeConstant/2
#z0 = np.max(grid[:,2])/2
#R0 = np.array([x0,y0,z0])

#natural units: charge [1] and length [1/eV]
e0 = 8.5424546e-2
me = 510.99906e3
lengthFactor = 1e-10/(1.9732705e-7) #multiply to length 
grid = lengthFactor*grid
#R0 = lengthFactor*R0

dimBasis = grid.shape[1]


numberHoles = holeStates.shape[1]
numberElec = elecStates.shape[1]
numberAtoms = grid.shape[0]


DipolMatSave = np.zeros((numberElec*numberHoles,dimBasis+2))
DipolMatrix = np.zeros((numberElec, numberHoles, dimBasis))

ImpulsMatSave = np.zeros((numberElec*numberHoles, dimBasis+2))
ImpulsMatrix = np.zeros((numberElec, numberHoles, dimBasis))


Surface = 0
plt.ioff()

nn = 0
for iEl in range(0,numberElec):
    for jHol in range(0,numberHoles):
        
        DipolMat = np.zeros((1,dimBasis))
       # ImpulsMat = np.zeros((1,3))        
        
        for R in range(0, numberAtoms):
            DipolMat= DipolMat + elecStates[R,iEl]*holeStates[R,jHol]*grid[R,:]

            #for R1 in range(0,numberAtoms):
            #    ImpulsMat = ImpulsMat + elecStates[R,iEl]*holeStates[R1,jHol] * HFTBM[R,R1] *(-1)*(grid[R,:] - grid[R1,:])
                
        DipolMatSave[nn,0] = iEl + 1
        DipolMatSave[nn,1] = jHol + 1
        DipolMatSave[nn,2:] = -e0*DipolMat
        #ImpulsMatSave[nn,0] = iEl + 1
        #ImpulsMatSave[nn,1] = jHol + 1
        #ImpulsMatSave[nn,2:] = np.abs(1j*e0*1/me*1/(elecEnerg[iEl]-holeEnerg[jHol]) * ImpulsMat)**2
        
        nn = nn + 1 
        DipolMatrix[iEl,jHol,:] = np.abs(-e0*DipolMat)**2
        #ImpulsMatrix[iEl,jHol,:] = np.abs(1j*e0*1/me*1/(elecEnerg[iEl]-holeEnerg[jHol]) * ImpulsMat)**2
        
    stri = str(iEl)
    print(stri)
 #   xaxis = range(1,numberHoles+1)
#    plt.figure()
#    plt.title('i='+stri)
#    plt.plot(xaxis, DipolMatrix[iEl,:,0],label='dx', color='black')
#    plt.plot(xaxis, DipolMatrix[iEl,:,1],label='dy', color='red')
#    plt.plot(xaxis, DipolMatrix[iEl,:,2],label='dz', color='blue')
#    plt.xlabel('j')
#    plt.ylabel('dij')   
#    plt.legend()
#    plt.savefig(rootDir + folder + subfolder + 'Dipol/'+filename + 'Dipol_i'+stri+'.eps', format='eps')
#
#    plt.close()
#    
#    plt.figure()
#    plt.title('i='+stri)
#    plt.plot(xaxis, ImpulsMatrix[iEl,:,0],label='dx', color='black')
#    plt.plot(xaxis, ImpulsMatrix[iEl,:,1],label='dy', color='red')
#    plt.plot(xaxis, ImpulsMatrix[iEl,:,2],label='dz', color='blue')
#    plt.xlabel('j')
#    plt.ylabel('dij')   
#    plt.legend()
#    plt.savefig(rootDir + folder + subfolder + 'Impulsoperator_TBM/'+filename + 'Impuls_i'+stri+'.eps', format='eps')
#    plt.close()
# 
    #plt.figure()
    #plt.title('i='+stri)
    #plt.plot(xaxis, ImpulsMatrix[iEl,:,1],label='p-dy', color='red')
    #plt.plot(xaxis, DipolMatrix[iEl,:,1],label='r-dy', color='black')
    #plt.xlabel('j')
    #plt.ylabel('dij')   
    #plt.legend()
    #plt.savefig(rootDir + folder + subfolder + 'Compare_HFTBM/'+filename + 'Compare_i'+stri+'.eps', format='eps')
    #plt.close()
    
if dimBasis == 3:
    np.savetxt(path + filename + 'Dipol.dat', DipolMatSave, fmt=('%i','%i','%.6e','%.6e', '%.6e'))
elif dimBasis == 2:
    np.savetxt(path + filename + 'Dipol.dat', DipolMatSave, fmt=('%i','%i','%.6e','%.6e'))
#np.savetxt(path +'Impuls_TBM.dat', ImpulsMatSave, fmt=('%i','%i','%.6e','%.6e'))

