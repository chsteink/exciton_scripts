//#############################################################
// spectral_function.cpp
//#############################################################
// Calculates spectral function in dependence of the energy
// and the relative vector A(r,w)
//#############################################################
// Version 1.1
//
// - initially written by Christina Steinke based on a program of 
//   Daniel Mourad
//
// - Including parallel processing
//
// Last changes: 2016-04-07
//
//#############################################################

#include <iostream>
#include <fstream>
#include <string>
#include <ctime>
#include <vector>
#include <sstream>
#include <omp.h>
#include "boost/multi_array.hpp"

//##################################################################
// Use the following from the standard namespace
//##################################################################
using std::cin;
using std::cout;
using std::cerr;
using std::endl;

//##################################################################
//                  global variables
//##################################################################

const double Pi = 3.141592654;

//##################################################################

//##################################################################
//                 TYPE DEFINITIONS
//##################################################################
// define complex-valued 3-dimensional array datatype 
// from "boost/multi_array.hpp" 
//typedef boost::multi_array<double, 2> array2double;

//typedef boost::multi_array<double, 4> array4double;

//##################################################################
//                DEFINITIONS
//##################################################################

//###############################################
// define a modulo-Funktion in analogy to MATLAB
// as "%" operator (sign-sensitive!!!)
//###############################################
#define MOD(a,b) ( ((a) < 0) ? (b)- ( (-(a)) % (b)) : (a) % (b) )

//##################################################################
//                     Function prototypes
//##################################################################
// --------------> not needed at the moment


//##################################################################
//##################################################################
//                             MAIN
//##################################################################
//##################################################################

// Main expects command line parameters, of which the number is in 
// argc and the parameters itself are subsequently in argv[], where 
// argv[0] contains the progranm name itself
//
//
// argv[0]: program name itself
// argv[1]: number of unit cells in x-direction
// argv[2]: number of unit cells in y-direction

//
// example: "./spectral_function 9 6"

int main(int argc, char* argv[])
{
  //=====================================================================
  //=====================================================================
  // Parameters to change
  //=====================================================================
  //=====================================================================
  
  
  //Broadening of delta-function
  double broadening = 0.005;
  //Energy steps
  double energystep = 0.01;
  
  //Relative vector which is looked at
  std::vector< double> relativeVector(3);
  relativeVector = {0., 0., 0.};
  
  // File name of exported Spectral Function
  std::string spectralFuncFilename("./spectral_function_000_parallel.dat");
  
  
  //============================================================
  //============================================================
  // End of free parameter part
  //============================================================
  //============================================================
  
  //############################################################
  //Check the number of input parameters
  //############################################################
  if (argc < 3) {
    // Tell the user how to run the code with command line parameters:
    // - as stated above argv[0] contains the progranm name itself
    // - we need three additional parameters:
    //  (1) the number of unit cells in x-direction
    //  (2) the number of unit cells in y-direction
    
    cerr << endl << "USAGE: " << argv[0] \
    << " nnCellsXX nnCellsYY" 
    << endl << endl;
    cerr << "EXAMPLE: " << argv[0] << " 9 6" << endl << endl;
    
    return 1;
  }
  
  //##################################################################
  //   Check and convert the command line input parameters themselves
  //##################################################################
  
  // Define input stringstreams to convert the argv[1], argv[2] to int  
  std::istringstream nnCellsXXStream(argv[1]);
  std::istringstream nnCellsYYStream(argv[2]);
  // 
  int nnCellsXX; 
  int nnCellsYY;
  // 
  nnCellsXXStream >> nnCellsXX;
  nnCellsYYStream >> nnCellsYY;
  
  std::string nnCellsXXstr(nnCellsXXStream.str());
  std::string nncellsYYstr(nnCellsYYStream.str());
  // 
  
  //##################################################################
  //  Defining places where data is stored
  //##################################################################
  
  //Careful: the electron and hole states are transposed! elecState[iiState, iPlace] 
  //instead of elecState[iPlace, iiState]!!
  // 
  const std::string locationElectronStates("./elec_states.dat");
  const std::string locationHoleStates("./hole_states.dat");
  const std::string locationExcitonStates( "./exc_ham_"+ nnCellsXXstr + "x" + nncellsYYstr + "_eigenvec_eigs.dat");
  const std::string locationExcitonEnergies("./exc_ham_"+ nnCellsXXstr + "x" + nncellsYYstr + "_eigenvals_eigs.dat");
  const std::string locationGrid("/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/Data/Python/HexLadder/supercell" \
  +nnCellsXXstr+"x" + nncellsYYstr + "_cart_grid.dat");
  
  
  
  //######################################################
  //######################################################
  // Read in data
  //######################################################
  //######################################################
  
  // Initialize the vectors (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > electronStatesMatrix;
  std::vector< std::vector<double> > holeStatesMatrix;
  
  //Create a string to read line into it
  std::string inputline;
  
  // Dummy vaiable to stream the TB expansion coefficients
  double coefficientDummy;
  
  //#####################################################
  //       Sinlge particle Electron States
  //#####################################################
  // Open input filestream to read in the values
  std::ifstream electronStatesStream;
  electronStatesStream.open(locationElectronStates.c_str());
  
  if(electronStatesStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading electron states from " << endl \
    << locationElectronStates << endl << endl;
    
    while(std::getline(electronStatesStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> coefficientPerSite;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	coefficientPerSite.push_back(coefficientDummy);
	
      }
      
      
      electronStatesMatrix.push_back(coefficientPerSite);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationElectronStates << "\" not found..." \
    << endl << endl;
    return 1;
  }
  
  
  //#####################################################
  //     Single Particle Hole States
  //#####################################################
  std::ifstream holeStatesStream;
  holeStatesStream.open(locationHoleStates.c_str());
  
  if(holeStatesStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading hole states from " << endl \
    << locationHoleStates << endl << endl;
    
    while(std::getline(holeStatesStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> coefficientPerSite;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	coefficientPerSite.push_back(coefficientDummy);
	
      }
      
      holeStatesMatrix.push_back(coefficientPerSite);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationHoleStates << "\" not found..." \
    << endl << endl;
    return 1;
  }
  
  //#####################################################
  // Loading grid information of the supercell
  //#####################################################
  
  // Initialize the vector (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > gridMatrix;
  
  std::ifstream gridStream;
  gridStream.open(locationGrid.c_str());
  
  if(gridStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading grid from " << endl \
    << locationGrid << endl << endl;
    
    while(std::getline(gridStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> gridComponents;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	gridComponents.push_back(coefficientDummy);
	
      }
      
      gridMatrix.push_back(gridComponents);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationGrid << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  }
  
  
  //Delete last column because it contains unneccessary information for this purpose (counting starts with zero)
  unsigned columnToDelete = 3;
  
  //gridMatrix[i] ruft Zeile i der Matrix auf, gridMatrix.size() gibt Anzahl der Zeilen
  
  for (unsigned i = 0; i < gridMatrix.size(); ++i)
  {
    
    if (gridMatrix[i].size() > columnToDelete)
    {
      
      gridMatrix[i].erase(gridMatrix[i].begin() + columnToDelete); 
    }
  }
  

  
  //#####################################################
  // Loading excitonic eigenvalues
  //#####################################################
  std::vector< double>  excEigenvals;
  
  std::ifstream exEigenvalsStream;
  exEigenvalsStream.open(locationExcitonEnergies.c_str());
  
  if(exEigenvalsStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading excitonic eigenvalues from " << endl \
    << locationExcitonEnergies << endl << endl;
    
    while(exEigenvalsStream >> coefficientDummy)
    {
      
      excEigenvals.push_back(coefficientDummy);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationExcitonEnergies << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  }
  //Check if eigenvalues are sorted in ascending order, if not quit
  if (excEigenvals[0] > excEigenvals[1])
  {
    cerr << "STOP: Excitonic eigenvalues not sorted!" << endl << endl;
    return 1;
  }
  
  //#####################################################
  // Loading excitonic eigenvectors
  //#####################################################
  
  // Initialize the vector (i.e., dynamically declared matrix
  //  as vector of vectors )
  std::vector< std::vector<double> > excStatesMatrix;
  
  std::ifstream excStatesMatrixStream;
  excStatesMatrixStream.open(locationExcitonStates.c_str());
  
  if(excStatesMatrixStream.is_open())
  {
    cout << "--------------------------------------------------" << endl;
    cout << "Reading excitonic eigenstates from " << endl \
    << locationExcitonStates << endl << endl;
    
    while(std::getline(excStatesMatrixStream, inputline))
    {
      //Writes line into a string
      std::stringstream inputlineStream(inputline);
      std::vector<double> coefficientPerEnergy;
      
      while(inputlineStream >> coefficientDummy)
      {
	
	coefficientPerEnergy.push_back(coefficientDummy);
	
      }
      
      excStatesMatrix.push_back(coefficientPerEnergy);
      
    }
  }
  
  // If necessary, warn the user that the stream does not work and quit
  else
  {
    cerr << "STOP: File \"" << locationExcitonStates << "\" not found..." \
    << endl << endl;
    return 1;
    
    
  }
  
  // Timer to measure the elapses wall clock and processor time
clock_t startProcTime, stopProcTime;
time_t startWallTime, stopWallTime;
startProcTime = clock();
startWallTime = time(0);



  //######################################################
  //######################################################
  // Calculation part of spectral function
  //######################################################
  //######################################################
  
  //Generating numbers of states, energies and so on to create
  //for loops
  
  int numberExcEnergies = excEigenvals.size();
  int numberHoleStates = nnCellsXX * nnCellsYY * 4 * 0.5;
  int numberElecStates = nnCellsXX * nnCellsYY * 4 * 0.5;
  int numberAtoms = nnCellsXX * nnCellsYY * 4 ;
  int numberCombination  = numberHoleStates*numberElecStates; 
  
  //Transposing eigenstate matrix for faster perfomance 
    cout << "--------------------------------------------------" << endl;
cout << "Transpose eigenstate matrix" << endl; 
std::vector< std::vector<double> > excStatesMatrix_transposed(numberExcEnergies, std::vector<double>(numberCombination)); 

//numberExcEnergies = new number of rows (old number of columns)
//numberCombination = new number of columns (old number of rows) 

for(int rowT=0; rowT< numberExcEnergies ; rowT++)
{
        for(int colT=0; colT< numberCombination; colT++)
	{
            excStatesMatrix_transposed[rowT][colT]= excStatesMatrix[colT][rowT];
        }
    }
  
  
  
  
  //######################################################
  // Calculating mass vectors corresponding to relative vector 
  //######################################################
  
  std::vector< std::vector < double > > massVector;
  std::vector< double > relativeVector_tmp(3) ;
  std::vector< double > massVector_tmp(3) ;
  
  for (unsigned int riEl = 0; riEl < numberAtoms; ++riEl )
  {
    for (unsigned int rjHol = 0; rjHol < numberAtoms; ++rjHol)
    {     		    
      
      //Substracting vector elements
      for (signed int ii = 0; ii < gridMatrix[0].size(); ++ii)
      {
	relativeVector_tmp[ii] = gridMatrix[rjHol][ii] - gridMatrix[riEl][ii];
	
      }

      if ( std::abs(relativeVector_tmp[0] - relativeVector[0]) < 1e-4 && std::abs(relativeVector_tmp[1] - relativeVector[1]) < 1e-4 \
	&& std::abs(relativeVector_tmp[2] - relativeVector[2])<1e-4)
      { 
	
	for (unsigned int ii = 0; ii < gridMatrix[0].size(); ++ii)
	{
	  massVector_tmp[ii] = 0.5*(gridMatrix[rjHol][ii] + gridMatrix[riEl][ii]);
	}
	
	massVector.push_back(massVector_tmp);
	
      }
      
      
    }
  }
  
  int sizeMassVector = massVector.size();

  cout << sizeMassVector << " mass vectors found!" << endl; 
   
  
  //######################################################
  // Generating index matrix for electron and hole states
  // line corresponds to excitonic index, in column corersponding
  // electron [0] / hole [1] indices are stored
  //#####################################################
  
  cout << "Calculating index matrix" << endl; 
  
  // std::vector< std::vector< int > > indMat(numberCombination,std::vector<int>(2));
  std::vector < int > indMat_el(numberCombination); 
   std::vector < int > indMat_hol(numberCombination); 
  int countRow= 0;
  
  for (unsigned int iEl = 0; iEl < numberElecStates; ++iEl)
  {
    for (unsigned int jHol = 0; jHol < numberHoleStates; ++jHol)
    {
  //    indMat[countRow][0] = iEl;
	indMat_el[countRow] = iEl; //first column
	indMat_hol[countRow] = jHol; //second column
  //    indMat[countRow][1] = jHol;
  
  
      countRow = countRow+ 1 ;
    }
  }
  
  
  //######################################################
  // Generating input energy values
  //#####################################################
  
  double minEnergy = *std::min_element(excEigenvals.begin(), excEigenvals.end());
  double maxEnergy = *std::max_element(excEigenvals.begin(), excEigenvals.end());
  double energyStep = 0.01;
  
  std::vector<double> Energy;
  double tmp_value = 0;
  int loopCount = 0;
  
  cout << "Calculating Energy vector for Spectral function" << endl;
 while(tmp_value < (maxEnergy-energyStep))
 { 
    tmp_value = minEnergy + loopCount*energyStep;
    Energy.push_back(tmp_value);
    loopCount = loopCount + 1;

  }
  
  int sizeEnergy = Energy.size();


  
  
  //######################################################
  // Calculating the Spectral function
  //######################################################
  
  cout << "Start calculation of spectral function" << endl;
  
  int energyCount;
  
   std::vector<double> Spectral_func(sizeEnergy);
  #pragma omp parallel for
        //Loop through chosen energy values
      
  for(int iEnergy = 0; iEnergy < sizeEnergy; ++iEnergy)
  { 
    //Progress counter
  #pragma omp atomic
    energyCount += 1;
    
    if (energyCount % 4 == 0)
    {
        #pragma omp critical    
           cout << energyCount/double(sizeEnergy)*100 << "% of energies calculated" << endl; 
    }

    
    
        //Loop through all mass Vectors
  for(signed int iMass = 0; iMass < sizeMassVector; ++iMass)
  {  

     int ind_elec;
    int ind_hole;
  
    std::vector< double > difference_dummy_elec(3);
    std::vector< double > difference_dummy_hole(3);
  
    std::vector< double> rElec(3);
    std::vector< double> rHole(3);
  
    //Calculating place of electron and hole depending on massVector
    for (unsigned int component = 0; component < 3; ++component)
    {
      rElec[component] = massVector[iMass][component] - 0.5 * relativeVector[component];
      rHole[component] = massVector[iMass][component] + 0.5 * relativeVector[component];
    } //endfor place of electron/hole
    
    
    //Finding row index of electron/hole space vector to determine column of elec/hole state 
    for (unsigned int row = 0; row < numberAtoms; ++row)
    {
      for (unsigned int component = 0; component < 3; ++component)
      {
	difference_dummy_elec[component] = std::abs(gridMatrix[row][component] - rElec[component]);
	difference_dummy_hole[component] = std::abs(gridMatrix[row][component] - rHole[component]);     
      } //endfor comparing gridMatrix with electron place
      
      if (difference_dummy_elec[0] < 1e-6 && difference_dummy_elec[1] < 1e-6 && difference_dummy_elec[2] < 1e-6)
      {
	ind_elec = row;
      } //endif ind_elec
      
      if (difference_dummy_hole[0] < 1e-6 && difference_dummy_hole[1] < 1e-6 && difference_dummy_hole[2] < 1e-6)
      {
	ind_hole = row;
      } //endif ind_hole
      
    } //endfor row

   //Sum over all excitonic eigenvalues
  
    double Sum_iExc = 0;
    
      for (int iExc = 0; iExc < numberExcEnergies; ++iExc)
    {
    
    //Calculating multiplicaton of coefficient of eigenvectos
      double coeffMult = 0;
    for (int ii = 0; ii < numberCombination; ++ii)    
    {
      //coeffMult =  coeffMult + excStatesMatrix[ii][iExc] * electronStatesMatrix[indMat[ii][0]][ind_elec] * holeStatesMatrix[indMat[ii][1]][ind_hole];
      int ind_elecState = indMat_el[ii]; 
      int ind_holeState = indMat_hol[ii];
      coeffMult =  coeffMult + excStatesMatrix_transposed[iExc][ii] * electronStatesMatrix[ind_elecState][ind_elec] * holeStatesMatrix[ind_holeState][ind_hole];
    } //endfor multiplication of coefficients
    
    
    Sum_iExc = Sum_iExc + std::abs(coeffMult)*std::abs(coeffMult) * broadening/(std::pow(broadening,2.) + std::pow((Energy[iEnergy] - excEigenvals[iExc]),2.));
    
    } //endfor excitonic eigenvalues
    
    
    Spectral_func[iEnergy] = Spectral_func[iEnergy] + 2.*Pi*Sum_iExc;
    
  
  } //endfor iMass Loop 
  } //endfor iEnergy
  
  
    // Timer to measure the elapsed time
stopProcTime = clock();
stopWallTime = time(0);
double totalProcTime = (stopProcTime - startProcTime) \
                      / (double)CLOCKS_PER_SEC;
double totalWallTime = (stopWallTime - startWallTime);
//cout << "##############################################" << endl;
cout << " Elapsed WALL CLOCK TIME: " << totalWallTime << " seconds" << endl;
cout << " Elapsed PROCESSOR TIME:  " << totalProcTime << " seconds" << endl;
//cout << "##############################################" << endl;
cout << endl;

  //######################################################
  // Writing Energy and corresponding Spectral function in file
  //######################################################
  


// Open output file stream
std::ofstream spectralOfStream(spectralFuncFilename.c_str());

// Set the numerical precision
spectralOfStream.precision(12);

cout << "--------------------------------------------------" << endl;
cout << "Exporting spectral function to " << endl
     << spectralFuncFilename << endl << endl;
     
 for(unsigned int iiStream = 0; iiStream < sizeEnergy; ++iiStream)
 {
   spectralOfStream << Energy[iiStream] << " " << Spectral_func[iiStream]  << endl; 
 }					
  

  
//#################################################
// CLOSE MAIN
// #################################################
}