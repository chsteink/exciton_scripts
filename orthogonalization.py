# -*- coding: utf-8 -*-
"""
Created on Fri Jan  8 13:19:17 2016

@author: csteinke
"""
from __future__ import division
from __future__ import print_function


#Defining directory in which functions can be found
#rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/Realspace_HF_python'
import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

import numpy as np
from scipy import linalg

#Script to orthogonalize the eigenvectors of the hamilton matrix

#place of saved eigenvectors
#rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/Realspace_HF_python/'
#folder = 'Data/HexLadder/'
#system = 'Dielectric/10x10/'
#filename = 'supercell_10x10_5_15_periodicInE2_.npz'

#load_name = rootDir + folder + system + filename

#loading data
#Tmp = np.load(load_name)

#HFTBM_EV = Tmp['HFTBM_EV']
#HFTBM_EE = Tmp['HFTBM_EE']
#HFTBM = Tmp['HFTBM']

def orthogonalization(HFTBM_EE, HFTBM_EV, HFTBM):
    #counting number of eigenvalues
    numbers_Eigenval = HFTBM_EE.shape[0]
    
    #defining matrix of eigenvalue Differences: choosing ones to prevent counting zeros by accident
    
    Difference_Eigenvalues = np.ones((numbers_Eigenval,numbers_Eigenval))
   # ScalarProd_test = np.zeros((numbers_Eigenval, numbers_Eigenval), dtype = complex)    
    
    for ii in range(0, numbers_Eigenval):
        
        #calculating pairwise differences of energies (only upper triangular matrix, because E_ii - E_jj = - (E_jj - E_ii))
        for jj in range(ii, numbers_Eigenval):
            
            Difference_Eigenvalues[ii, jj] = HFTBM_EE[ii] - HFTBM_EE[jj]
           # ScalarProd_test[ii,jj] = np.dot(HFTBM_EV[:,ii], HFTBM_EV[:,jj])
            

    
        #finding indices of same energies
        Index_sameEnergy = np.where(np.abs(Difference_Eigenvalues[ii,:]) < 10e-13)[0]    
        number_sameEnergy = Index_sameEnergy.shape[0]
        
        #Orthogonalization process: calculate scalar product and check if it's "zero" (< 1e-13)
        Orthogonalisation = 0 #only orthogonalize, if scalar product isn't zero
        
        for ll in range(0,number_sameEnergy ):
            for kk in range(ll+1, number_sameEnergy): 
                ScalarProd = np.dot(HFTBM_EV[:,Index_sameEnergy[ll]], HFTBM_EV[:,Index_sameEnergy[kk]])
                #print(ScalarProd)
            
                if np.abs(ScalarProd) > 1e-14:
                 Orthogonalisation = 1 #only orthogonalize, if at least one of the scalar products isn't zero
                 
        if Orthogonalisation == 1:
            VecRoom = HFTBM_EV[:,Index_sameEnergy[0]:Index_sameEnergy[-1]+1]
            print('Starting orthogonalisation')
            
                #change old vectors with orthogonalized vectors:
            HFTBM_EV[:,Index_sameEnergy[0]:Index_sameEnergy[-1]+1] = linalg.orth(VecRoom)

    
    #testing program
    New_Eigenvalues = np.diag(np.dot(np.linalg.inv(HFTBM_EV),np.dot(HFTBM, HFTBM_EV)))   
    Testing_Val = np.abs(New_Eigenvalues - HFTBM_EE)
    
    for ii in range(0,Testing_Val.shape[0]):
        
        if Testing_Val[ii] > 1e-14:
            print('Something went wrong in line %i' % ii)
            print('Difference: %12.8e' % Testing_Val[ii])
            
    return HFTBM_EE, HFTBM_EV
