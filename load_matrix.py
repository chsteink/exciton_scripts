# -*- coding: utf-8 -*-
"""
Created on Mon Feb  1 08:58:00 2016

@author: csteinke
"""

from __future__ import division
from __future__ import print_function


#Defining directory in which functions can be found
import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib.pyplot as plt
import time

import scipy as sp
from scipy.sparse import linalg
from scipy.sparse import lil_matrix


#run excitons prog - loading data, give to H script

print('Loading Data                         ')
#Defining place of Data 
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'
subfolder = 'Hamiltonian/'
filename = 'sparse_exc_ham_7x7'
ending = '.dat'
whole_file = rootDir + folder + subfolder + filename 
input_file = whole_file + ending

start = time.clock()
EHM_Data = np.loadtxt(input_file)
end = time.clock()
print('%.6f seconds \n' % (end-start))

EHM = sp.sparse.coo_matrix((EHM_Data[:,2], (EHM_Data[:,0],EHM_Data[:,1])))
EHM = EHM.tocsr()

print('Diagonalizing EHM \n')
start = time.clock()
[EHM_EE, EHM_EV] = sp.sparse.linalg.eigsh(EHM, k = (EHM.shape[0]-1), which='SM')
end = time.clock()
print('It took %.6f seconds' %(end-start))

[DOS_EHM, binEdges] = np.histogram(EHM_EE, bins = 150)
bincenters = 0.5 * (binEdges[1:] + binEdges[:-1])

print('Saving DOS \n')
output_name = filename + '_DOS.dat'
np.savetxt(rootDir + folder + subfolder + output_name, np.array([bincenters, DOS_EHM]))

print('Loading density density Data \n')
#--------------------------------------
filename = 'sparse_exc_ham_7x7_densdens'
ending = '.dat'
whole_file = rootDir + folder + subfolder + filename 
input_file = whole_file + ending

EHM_Data_DD = np.loadtxt(input_file)
EHM_DD = sp.sparse.coo_matrix((EHM_Data[:,2], (EHM_Data[:,0],EHM_Data[:,1])))
EHM_DD = EHM.tocsr()

print('Diagonalizing density density matrix \n')
[EHM_EEd, EHM_EVd] = sp.sparse.linalg.eigsh(EHM_DD, k = (EHM.shape[0]-1),which='SM')

[DOS_EHMdd, binEdges] = np.histogram(EHM_EEd, bins = 150)
bincenters = 0.5 * (binEdges[1:] + binEdges[:-1])

print('Saving DOS of density density EHM \n')
output_name = filename + '_DOS.dat'
np.savetxt(rootDir + folder + subfolder + output_name, np.array([bincenters, DOS_EHMdd]))
