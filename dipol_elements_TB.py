# -*- coding: utf-8 -*-
"""
Created on Thu Feb 18 15:16:20 2016

@author: csteinke
"""

#Programm calculating dipole matrix elements
#Matrix elements of impulse operator maybe wrong sign!

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc

os.system(['clear','cls'][os.name == 'nt'])

latticeConstant = 3.18
nnCellsX = 9
nnCellsY = 6

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Python/HexLadder/Dielectric/'
subfolder = str(nnCellsX) + 'x' + str(nnCellsY) +'/'
filename = 'supercell_'+str(nnCellsX) + 'x' + str(nnCellsY) +'_5_15_periodicInE2_'
path = rootDir + folder + subfolder + filename 

TBM_EV = np.loadtxt(path + 'TBM_EV.dat')
TBM_EE = np.loadtxt(path + 'TBM_EE.dat')
TBM = np.loadtxt(path + 'TBM.dat')

grid = np.loadtxt(rootDir + 'Data/Python/HexLadder/supercell'+str(nnCellsX) + 'x' + str(nnCellsY) +'_cart_grid.dat')
grid = grid[:,0:-1]

#==============================================================================
# #Getting electron and hole states
#==============================================================================
numberOfStates = TBM_EE.shape[0]
nOccStates = int(numberOfStates/2)

#changing order of hole-energy, thus that h[0] = energy of valence band
holeEnerg = TBM_EE[0:nOccStates]
sortind = np.argsort(holeEnerg)[::-1]
holeEnerg = holeEnerg[sortind]

holeStates = TBM_EV[:,0:nOccStates]
holeStates = holeStates[:,sortind]

elecEnerg = TBM_EE[nOccStates:numberOfStates]
elecStates = TBM_EV[:, nOccStates:numberOfStates]

#==============================================================================
# Calculating different expectation values of operators
#==============================================================================

#natural units: charge [1] and length [1/eV]
e0 = 8.5424546e-2
me = 510.99906e3
lengthFactor = 1e-10/(1.9732705e-7) #multiply to length 
grid = lengthFactor*grid



numberHoles = holeStates.shape[1]
numberElec = elecStates.shape[1]
numberAtoms = grid.shape[0]


DipolMatSave = np.zeros((numberElec*numberHoles,5))
DipolMatrix = np.zeros((numberElec, numberHoles, 3))

ImpulsMatSave = np.zeros((numberElec*numberHoles, 5))
ImpulsMatrix = np.zeros((numberElec, numberHoles, 3))


Surface = 0
plt.ioff()

nn = 0
for iEl in range(0,numberElec):
    for jHol in range(0,numberHoles):
        
        DipolMat = np.zeros((1,3))
        ImpulsMat = np.zeros((1,3))        
        
        for R in range(0, numberAtoms):
            DipolMat= DipolMat + elecStates[R,iEl]*holeStates[R,jHol]*grid[R,:]

            for R1 in range(0,numberAtoms):
                ImpulsMat = ImpulsMat + elecStates[R,iEl]*holeStates[R1,jHol] * TBM[R,R1] *(-1)*(grid[R,:] - grid[R1,:])
                
        DipolMatSave[nn,0] = iEl + 1
        DipolMatSave[nn,1] = jHol + 1
        DipolMatSave[nn,2:] = np.abs(1j*me*(elecEnerg[iEl]-holeEnerg[jHol])*DipolMat)**2
        ImpulsMatSave[nn,0] = iEl + 1
        ImpulsMatSave[nn,1] = jHol + 1
        ImpulsMatSave[nn,2:] = np.abs(1j*me*ImpulsMat)**2
        
        nn = nn + 1 
        DipolMatrix[iEl,jHol,:] = np.abs(me*(elecEnerg[iEl]-holeEnerg[jHol])*DipolMat)**2
        ImpulsMatrix[iEl,jHol,:] = np.abs(1j*me*ImpulsMat)**2
        
    stri = str(iEl)
    print(stri)
    xaxis = range(1,numberHoles+1)
    plt.figure()
    plt.title('i='+stri)
    plt.plot(xaxis, DipolMatrix[iEl,:,0],label='dx', color='black')
    plt.plot(xaxis, DipolMatrix[iEl,:,1],label='dy', color='red')
    plt.plot(xaxis, DipolMatrix[iEl,:,2],label='dz', color='blue')
    plt.xlabel('j')
    plt.ylabel('dij')   
    plt.legend()
    plt.savefig(rootDir + folder + subfolder + 'Dipolmoments_TB/Ortsoperator/'+filename + 'Dipol_i'+stri+'.eps', format='eps')

    plt.close()
    
    plt.figure()
    plt.title('i='+stri)
    plt.plot(xaxis, ImpulsMatrix[iEl,:,0],label='dx', color='black')
    plt.plot(xaxis, ImpulsMatrix[iEl,:,1],label='dy', color='red')
    plt.plot(xaxis, ImpulsMatrix[iEl,:,2],label='dz', color='blue')
    plt.xlabel('j')
    plt.ylabel('dij')   
    plt.legend()
    plt.savefig(rootDir + folder + subfolder + 'Dipolmoments_TB/Impulsoperator/'+filename + 'Impuls_i'+stri+'.eps', format='eps')
    plt.close()
    
        
    plt.figure()
    plt.title('i='+stri)
    plt.plot(xaxis, ImpulsMatrix[iEl,:,1],label='p-dy', color='red')
    plt.plot(xaxis, DipolMatrix[iEl,:,1],label='r-dy', color='black')
    plt.xlabel('j')
    plt.ylabel('dij')   
    plt.legend()
    plt.savefig(rootDir + folder + subfolder + 'Dipolmoments_TB/'+filename + 'Compare_i'+stri+'.eps', format='eps')
    plt.close()
    
    
 

np.savetxt( rootDir + folder + subfolder  +'Dipolmoments_TB/Dipol_TB.dat', DipolMatSave, fmt=('%i','%i','%.6e','%.6e', '%.6e'))
np.savetxt( rootDir + folder + subfolder  +'Dipolmoments_TB/Impuls_TB.dat', ImpulsMatSave, fmt=('%i','%i','%.6e','%.6e', '%.6e'))

