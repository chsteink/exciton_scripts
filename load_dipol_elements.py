# -*- coding: utf-8 -*-
"""
Created on Tue Mar 15 09:33:30 2016

@author: csteinke
"""

import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#loading submodules
import numpy as np
import matplotlib 
#matplotlib.use('PS')
import matplotlib.pyplot as plt
from matplotlib import rc

plt.ioff()

os.system(['clear','cls'][os.name == 'nt'])

rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder'
subfolder = '/9x6/5_15/'
filename = 'supercell_9x6_5_15_periodicInE2_'
path = rootDir + folder + subfolder + filename 

DipolMatSave = np.loadtxt(path + 'Dipol.dat')
#ImpulsMatSave= np.loadtxt(path + 'Impuls.dat')

nnCellsX = 9
nnCellsY = 6

numberHoles = nnCellsX*nnCellsY*4/2
numberElec = nnCellsX*nnCellsY*4/2
numberAtoms = nnCellsX*nnCellsY*4


DipolMatrix = np.zeros((numberElec, numberHoles, 3))
#ImpulsMatrix = np.zeros((numberElec, numberHoles, 3))

#Generating Matrix from Input-files (1-based indexing in Data, 0-based in python)
Dipol_index = DipolMatSave[:,0:2].astype(int)-1
#Impuls_index = ImpulsMatSave[:,0:2].astype(int)-1

DipolMatrix[Dipol_index[:,0],Dipol_index[:,1],:] = DipolMatSave[:,2:5]
#ImpulsMatrix[Impuls_index[:,0],Impuls_index[:,1],:] = ImpulsMatSave[:,2:5]

#
#for i in range(0,numberElec):
##Norm of Data: for every i choose highest value and use this value as norm
#
#    Norm = np.max(np.max(DipolMatrix[i,:,:],axis = 1))
#    DipolMatrix_norm = DipolMatrix[i,:,:]/Norm
#    
#    Norm_impuls = np.max(np.max(ImpulsMatrix[i,:,:],axis = 1))
#    ImpulsMatrix_norm = ImpulsMatrix[i,:,:]/Norm_impuls
#    
#    stri = str(i)
#    print(stri)
#    xaxis = range(1,numberHoles+1)
#    plt.figure()
#    plt.title('i='+stri)
#    plt.plot(xaxis, DipolMatrix_norm[:,0],label='dx', color='black')
#    plt.plot(xaxis, DipolMatrix_norm[:,1],label='dy', color='red')
#    plt.plot(xaxis, DipolMatrix_norm[:,2],label='dz', color='blue')
#    plt.xlabel('j')
#    plt.ylabel('dij')   
#    
#    
#    plt.plot(xaxis, ImpulsMatrix_norm[:,0], '--',label='dx-Impuls', color='green',)
#    plt.plot(xaxis, ImpulsMatrix_norm[:,1], '--', label='dy-Impuls', color='magenta')
#    plt.plot(xaxis, ImpulsMatrix_norm[:,2], '--', label='dz-Impuls', color='cyan')
#    plt.xlabel('j')
#    plt.ylabel('dij')   
#
#    plt.legend(loc='upper left')
#    
#    
#    plt.savefig(rootDir + folder + subfolder + 'Comparison/'+filename + 'compare_i'+stri+'.png', format='png')
#    plt.close()