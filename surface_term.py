# -*- coding: utf-8 -*-
"""
Created on Mon Jan 18 15:33:04 2016

@author: csteinke
"""
#==============================================================================
# Program to evaluate the surface correction term for the d_y component of the
# dipole Matrix
#==============================================================================
from __future__ import division
from __future__ import print_function


#Defining directory in which functions can be found
import sys, os
path = os.path.abspath('')
dir_path = os.path.dirname(path)
sys.path.insert(0, dir_path)

#sys.path.append('/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/Excitons/')

#loading submodules
import numpy as np
import matplotlib.pyplot as plt

from scipy.interpolate import griddata


plt.ion()

#---Defining properties of supercell (Hexagonal Model system)
NNCellsX = 9
NNCellsY = 6
a = 3.18

e0 = 8.5424546e-2

#Lattice vectors of non-primitive unit cell of the supercell
LatticeY= np.sqrt(3)*a
LatticeX = a
LatticeZ = 2*a/4

#Number of atom points and Volume of supercell
N_atoms = NNCellsX * NNCellsY * 4
V_supercell = NNCellsX * LatticeX * NNCellsY * LatticeY * 2 * LatticeZ

#---Loading important Data (Eigenvalues and eigenvectors of electron and hole 
#--States from Hartree Fock calculations of heterogeneous system)
rootDir = '/home/csteinke/Promotion/01_HF_Inhomogeneous_HalfSpace/'
folder = 'Data/Excitons/HexLadder/'
subfolder = str(NNCellsX)+'x'+str(NNCellsY)+'/5_15/'
states_praefix = 'supercell_'+str(NNCellsX)+'x'+str(NNCellsY)+'_5_15_periodicInE2_'
path = rootDir + folder + subfolder

elecEnerg = np.loadtxt(path + 'elec_energies.dat')
holeEnerg = np.loadtxt(path + 'hole_energies.dat')
elecStates = np.loadtxt(path + states_praefix+'elecStates.dat')
holeStates = np.loadtxt(path + states_praefix+'holeStates.dat')

#--Loading grid of supercell
grid = np.loadtxt(rootDir + 'Data/Python/HexLadder/' + 'supercell'+str(NNCellsX)+'x'+str(NNCellsY)+'_cart_grid.dat')
gridCart = grid[:,0:3]



#==============================================================================
# Adding Data from Boundary conditions to fill interpolation gaps 
#Therefore add grid points at the borders of the supercell (at (Nx*LatticeX, y) 
#and (x, Ny*LatticeY)) and choose values according to boundary conditions
#0 in x-direction and the value of (0,y) in y-direction (periodic boundaries)
#==============================================================================

#Generating new "pseudo"-gridpoints in x and y direction
gridPseudo_X = np.zeros((2*NNCellsY+2,3))
gridPseudo_Y = np.zeros((2*NNCellsX,3))

#--Grid in x-direction
ii = 0
for ny in range(0,NNCellsY+1):
    gridPseudo_X[ii,:]   = np.array([NNCellsX*LatticeX,ny*LatticeY,0])
    gridPseudo_X[ii+1,:] = np.array([NNCellsX*LatticeX,ny*LatticeY,a/4])

    ii = ii + 2

#--Grid in y.direction
ii = 0
for nx in range(0,NNCellsX):
    gridPseudo_Y[ii,:]     = np.array([nx*LatticeX, NNCellsY * LatticeY, 0 ])
    gridPseudo_Y[ii+1,:]   = np.array([nx*LatticeX, NNCellsY * LatticeY, a/4])
    
    ii = ii +2

#--Defining and choosing transition from electron (iEl) to hole (jHol) state
numberElec = elecStates.shape[1]
numberHole = holeStates.shape[1]
SurfaceSave = np.zeros((numberElec*numberHole,3))

nn = 0
for iEl in range(0,numberElec):
    for jHol in range(0,numberHole):
                
        coefficient_elec = elecStates[:,iEl]
        coefficient_hole = holeStates[:,jHol]
        
        
        #--Generating coefficients for pseudo-grid points
        PseudoWF_elecX = np.zeros((gridPseudo_X.shape[0])) 
        PseudoWF_holeX = np.zeros((gridPseudo_X.shape[0]))  
        PseudoWF_elecY = np.zeros((gridPseudo_Y.shape[0]))
        PseudoWF_holeY = np.zeros((gridPseudo_Y.shape[0]))
        
        #Finding indices of gridCart, where y == 0 (border)
        ind_border = np.where(gridCart[:,1] == 0) 
        
        PseudoWF_elecY[0:gridPseudo_Y.shape[0]] = coefficient_elec[ind_border]
        PseudoWF_holeY[0:gridPseudo_Y.shape[0]] = coefficient_hole[ind_border]
            
        #Merging everything together
        gridCart_interp = np.concatenate((gridCart,gridPseudo_X,gridPseudo_Y),axis=0)    
        coefficient_elec_interp = np.concatenate((coefficient_elec,PseudoWF_elecX, PseudoWF_elecY),axis=0)
        coefficient_hole_interp = np.concatenate((coefficient_hole,PseudoWF_holeX, PseudoWF_holeY),axis=0)
        
        #Renormalize coefficients so that sum(ci**2) = 1
        coefficient_elec_interp = coefficient_elec_interp/(np.sum(coefficient_elec_interp**2))
        coefficient_hole_interp = coefficient_hole_interp/(np.sum(coefficient_hole_interp**2))
        
        #==============================================================================
        # Interpolating Data on Supercell with pseudo atom points at the border
        #==============================================================================
        nnGridPoints = 200
        nGridZ = 4
        method_str = 'linear'
        
        xi = np.linspace(min(gridCart_interp[:,0]), max(gridCart_interp[:,0]), nnGridPoints)
        yi = np.linspace(min(gridCart_interp[:,1]), max(gridCart_interp[:,1]), nnGridPoints)
        zi = np.linspace(min(gridCart_interp[:,2]), max(gridCart_interp[:,2]), nGridZ)
        X,Y,Z= np.meshgrid(xi,yi,zi)
        
        WF_elec = griddata((gridCart_interp[:,0],gridCart_interp[:,1],gridCart_interp[:,2]), coefficient_elec_interp[:],(X,Y,Z))
        WF_hole = griddata((gridCart_interp[:,0],gridCart_interp[:,1],gridCart_interp[:,2]), coefficient_hole_interp[:],(X,Y,Z))
        
        nLatticeSites = nnGridPoints**2 * 4 - np.nansum(np.isnan(WF_elec))
        V_gridPoint = V_supercell/nLatticeSites
        
        #Norm of interpolated wavefuntios
        WF_elec = WF_elec/np.sqrt(V_gridPoint*np.nansum(WF_elec**2))
        WF_hole = WF_hole/np.sqrt(V_gridPoint*np.nansum(WF_hole**2))
        
        #==============================================================================
        # Calculating surface term
        #==============================================================================
        
        #border area of supercell: 
        F = NNCellsX * a * 2 * a/4
        #border area per lattice site
        DeltaF_interp = F/nLatticeSites
        
        #difference of gridpoints in y-direction
        DeltaY = (np.max(gridCart_interp[:,1])-np.min(gridCart_interp[:,1]))/(nnGridPoints - 1)
        
        
        Surface = 0
        for k in range(0,nnGridPoints):
           for z in range(0,nGridZ):
                
                Surface = Surface + DeltaF_interp*(WF_elec[0,k,z]*(WF_hole[1,k,z]-WF_hole[0,k,z])/DeltaY - \
                                       (WF_elec[1,k,z] - WF_elec[0,k,z])/DeltaY * WF_hole[0,k,z])
                                       
        
        
        Surface = -1j/2*e0*Surface
        
        SurfaceSave[nn,0] = iEl + 1
        SurfaceSave[nn,1] = jHol + 1
        SurfaceSave[nn,2] = np.imag(Surface)

        nn = nn + 1
        print('Iteration number %i' %nn)

file_save = path + states_praefix + 'SurfaceTerm.dat'
np.savetxt(file_save, SurfaceSave, fmt=('%i','%i','%.6e'),header = 'iEl jHol Imag(C_ij)')
#
#C_ij_discrete = np.zeros((2*NNCellsX))
#
##
#
#ind_y0 = np.where(gridCart[:,1] == 0)[0]
#ind_y1 = np.where(gridCart[:,1] == 0 + LatticeY)[0]
#
#V_Cell = V_supercell/N_atoms
#
#coefficient_hole = coefficient_hole/np.sqrt(V_Cell)
#coefficient_elec = coefficient_elec/np.sqrt(V_Cell)
#
#DeltaF = F/(2*NNCellsX * LatticeX *a/4)#a/4 = distance between two layers
#
#Size_loop = ind_y0.shape[0]
#for k in range(0,Size_loop):
#    Diff_hole = (coefficient_hole[ind_y1[k]] - coefficient_hole[ind_y0[k]])/LatticeY
#    Diff_elec = (coefficient_elec[ind_y1[k]] - coefficient_elec[ind_y0[k]])/LatticeY
#    C_ij_discrete[k] = C_ij_discrete[k] + DeltaF*(coefficient_elec[ind_y0[k]]*Diff_hole \
#                                              - coefficient_hole[ind_y0[k]]*Diff_elec)
#                                              
#                                              
#    
#    
#C_ij_discrete = np.sum(C_ij_discrete)
#print(C_ij_discrete)
##
#

